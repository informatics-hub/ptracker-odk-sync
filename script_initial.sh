#!/bin/bash
# PTracker ODK Sync Initial Queries
#

date
echo "Executing initial query for OpenMRS..."
mysql -D"openmrs_test" -u"root" -p"root@ucsf" < "database/queries/query_initial_openmrs.sql"
echo "Complete, now please run script_initial.sh"

date
echo "Executing initial query for ODK..."
mysql -D"ptracker_odk" -u"root" -p"root@ucsf" < "database/queries/query_initial.sql"
echo "Complete, now ensure that cron jobs are set"
