#These scripts clears the synclog by voiding records for resubmission or by excluding record from synclog error count

#Exclude from synclog errors that have already been resubmitted with the status Error adding obs
UPDATE stag_ptracker_synclog a
INNER JOIN 
(
	SELECT  odk_uuid
	FROM stag_ptracker_synclog
	WHERE status = 'added obs' 
) b
ON a.odk_uuid = b.odk_uuid
SET error_message = null
WHERE status LIKE 'Error adding obs%'; 

#Exclude from synclog errors that have already been resubmitted with the status FAILED
UPDATE stag_ptracker_synclog a
INNER JOIN 
(
	SELECT  odk_uuid
	FROM stag_ptracker_synclog
	WHERE status = 'added obs' 
) b
ON a.odk_uuid = b.odk_uuid
SET error_message = null
WHERE a.status = 'FAILED';

#Remove records with status Error adding obs from synclog that need to be resubmitted, because they either do not have encounters or patients created on openmrs
DELETE
FROM stag_ptracker_synclog
WHERE status LIKE 'Error adding obs%' 
AND (openmrs_encounter_uuid is NULL OR openmrs_patient_uuid is null) 
AND error_message is not null;  

#Remove records with status FAILED from synclog that need to be resubmitted to openmrs
DELETE
FROM stag_ptracker_synclog
WHERE status = 'FAILED' 
AND error_message is not null;