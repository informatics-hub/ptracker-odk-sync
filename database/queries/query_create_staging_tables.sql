#Creates staging tables from encoutner views
#ANC
DROP TABLE IF EXISTS `stag_odk_anc`;
CREATE TABLE IF NOT EXISTS 
stag_odk_anc AS 
SELECT * 
FROM vw_odk_anc
ORDER BY visit_date_recorded_odk DESC;

#Delivery Infant
DROP TABLE IF EXISTS `stag_odk_delivery_infant`;
CREATE TABLE IF NOT EXISTS 
stag_odk_delivery_infant AS 
SELECT * 
FROM vw_odk_delivery_infant;

#Delivery
DROP TABLE IF EXISTS `stag_odk_delivery`;
CREATE TABLE IF NOT EXISTS 
stag_odk_delivery AS 
SELECT * 
FROM vw_odk_delivery
ORDER BY visit_date_recorded_odk DESC;

#PNC
DROP TABLE IF EXISTS `stag_odk_pnc`;
CREATE TABLE IF NOT EXISTS 
stag_odk_pnc AS 
SELECT * 
FROM vw_odk_pnc
ORDER BY visit_date_recorded_odk DESC;

#Infatn PNC
DROP TABLE IF EXISTS `stag_odk_pnc_infant`;
CREATE TABLE IF NOT EXISTS 
stag_odk_pnc_infant AS 
SELECT * 
FROM vw_odk_pnc_infant
ORDER BY visit_date_recorded_odk DESC;

#Creates staging tables for ODK csv for offline use from views
#facility list table
DROP TABLE IF EXISTS `stag_flist`;
CREATE TABLE IF NOT EXISTS 
stag_flist AS 
SELECT * 
FROM vw_flist;

#Creates staging table for odk uuids
#odk uuids table
DROP TABLE IF EXISTS `stag_ptracker_uuids`;
CREATE TABLE IF NOT EXISTS 
stag_ptracker_uuids AS 
SELECT * 
FROM vw_ptracker_uuids;

