# clears worklist queue #
DELETE FROM stag_ptracker_worklist
WHERE _URI 
	 IN 
		(
			SELECT odk_uuid AS _URI 
			FROM stag_ptracker_synclog
		)