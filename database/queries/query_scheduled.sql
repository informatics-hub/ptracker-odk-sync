# executes queries to be scheduled on a daily basis
# use ptracker_odk;
source /ptracker/ptracker-sync/database/queries/query_worklist_clear.sql
source /ptracker/ptracker-sync/database/queries/query_create_staging_tables.sql;
source /ptracker/ptracker-sync/database/queries/query_worklist_append.sql;