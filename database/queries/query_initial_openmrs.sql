# executes initial queries to setup ODK csv views and tables
source /ptracker/ptracker-sync/database/views/vw_plist.sql
source /ptracker/ptracker-sync/database/views/vw_ulist.sql
source /ptracker/ptracker-sync/database/queries/query_create_staging_tables_openmrs.sql