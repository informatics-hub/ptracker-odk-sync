# executes initial queries to setup ODK Sync views and tables
source /ptracker/ptracker-sync/database/views/vw_odk_anc.sql
source /ptracker/ptracker-sync/database/views/vw_odk_pnc.sql
source /ptracker/ptracker-sync/database/views/vw_odk_delivery.sql
source /ptracker/ptracker-sync/database/views/vw_odk_pnc_infant.sql
source /ptracker/ptracker-sync/database/views/vw_odk_delivery_infant.sql
source /ptracker/ptracker-sync/database/views/vw_ptracker_uuids.sql
source /ptracker/ptracker-sync/database/tables/facility_codes.sql
source /ptracker/ptracker-sync/database/views/vw_flist.sql
source /ptracker/ptracker-sync/database/tables/stag_ptracker_synclog.sql;
source /ptracker/ptracker-sync/database/tables/stag_ptracker_worklist.sql;
source /ptracker/ptracker-sync/database/queries/query_create_staging_tables.sql;
source /ptracker/ptracker-sync/database/queries/query_worklist_append.sql;
