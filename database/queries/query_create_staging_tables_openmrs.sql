#Creates staging tables for ODK csv for offline use
#patient list table
DROP TABLE IF EXISTS `stag_plist`;
CREATE TABLE IF NOT EXISTS 
stag_plist AS 
SELECT * 
FROM vw_plist
WHERE ptracker_id IS NOT NULL
AND (
		(date_of_last_visit = NULL) OR (encounter_datetime > DATE_SUB(now(), INTERVAL 18 MONTH))
	)
GROUP BY ptracker_id;

#user list table
DROP TABLE IF EXISTS `stag_ulist`;
CREATE TABLE IF NOT EXISTS 
stag_ulist AS 
SELECT * 
FROM vw_ulist;