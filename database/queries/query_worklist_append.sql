# populates worklist table with # 
INSERT INTO stag_ptracker_worklist	
SELECT
	NULL AS id, _URI, ptracker_id, visit_date, visit_type, ptracker_username
FROM stag_ptracker_uuids	
WHERE _URI 
	NOT IN 
		(
			SELECT odk_uuid AS _URI 
			FROM stag_ptracker_synclog
		)
	AND 
	_URI NOT IN	
		(
			SELECT _URI 
			FROM stag_ptracker_worklist
		)