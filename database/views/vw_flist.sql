DROP VIEW IF EXISTS vw_flist; 
CREATE VIEW vw_flist AS
    SELECT 
        id,
        mfl_code AS 'facility_code',
        NAME AS 'name',
        uuid
    FROM facility_codes 
    WHERE 
        mfl_code IS NOT null
    AND 
        id > '50'
    AND 
        uuid IS NOT null
    GROUP BY 
        mfl_code

