DROP VIEW IF EXISTS vw_odk_delivery_infant;
CREATE VIEW vw_odk_delivery_infant AS
        SELECT 
                ptracker_id AS  'ptracker_id',
                facility    AS  'facility_code',
                facility_name AS 'facility_name',
                null AS 'facility_uuid', 
                'Infant L&D' AS 'type',
                `PTK_201803_CORE`._uri AS 'odk_uuid',
                -- INFANT L&D PART --
                INFO_INFANT_PTRACKER_NUMBER AS  'infant_ptracker_id',
                infant_status AS 'infant_status',
                info_infant_sex  AS  'infant_sex',
                Date_format(info_infant_dob, "%Y-%m-%d %h:%i:%s") AS 'infant_dob',
                info_still_birth AS  'infant_stillbirth',
                info_infant_feeding     AS 'infant_feeding',
                Date_format(info_infant_dod, "%Y-%m-%d %h:%i:%s") AS 'infant_dod',
                info_infant_dod_missing AS 'infant_dod_missing',            
                info_infant_arv AS  'infant_arv_status',
                NULL AS  'infant_arv_reason_for_refusal',
                NULL AS  'infant_arv_reason_for_refusal_missing',
                -- MISC --
                mrs_openmrs_user_uuid AS 'provider_uuid',
                ptracker_username AS 'username',
                _SUBMISSION_DATE AS 'submission_date',
                `PTK_201803_CORE`._uri AS _URI,
                'PTK_201803' as version
        FROM `PTK_201803_CORE`
        JOIN `PTK_201803_CORE2` 
        ON `PTK_201803_CORE2`._PARENT_AURI = `PTK_201803_CORE`._URI
        JOIN `PTK_201803_LD_INFANT` 
        ON `PTK_201803_LD_INFANT`._PARENT_AURI = `PTK_201803_CORE`._URI
        WHERE  visit_type = '2'    
    UNION ALL
        SELECT 
                ptracker_id AS  'ptracker_id',
                facility    AS  'facility_code',
                facility_name AS 'facility_name',
                null AS 'facility_uuid', 
                'Infant L&D' AS 'type',
                `PTK_2018032_CORE`._uri AS 'odk_uuid',
                -- INFANT L&D PART --
                INFO_INFANT_PTRACKER_NUMBER AS  'infant_ptracker_id',
                infant_status AS 'infant_status',
                info_infant_sex  AS  'infant_sex',
                Date_format(info_infant_dob, "%Y-%m-%d %h:%i:%s") AS 'infant_dob',
                info_still_birth AS  'infant_stillbirth',
                info_infant_feeding     AS 'infant_feeding',
                Date_format(info_infant_dod, "%Y-%m-%d %h:%i:%s") AS 'infant_dod',
                info_infant_dod_missing AS 'infant_dod_missing',            
                info_infant_arv  AS   'infant_arv_status',
                NULL   AS   'infant_arv_reason_for_refusal',
                NULL       AS  'infant_arv_reason_for_refusal_missing',
                -- MISC --
                mrs_openmrs_user_uuid   AS 'provider_uuid',
                ptracker_username       AS 'username',
                _SUBMISSION_DATE AS 'submission_date',
                `PTK_2018032_CORE`._uri  AS _URI,
                'PTK_2018032' as version
        FROM `PTK_2018032_CORE`
        JOIN `PTK_2018032_CORE2` 
        ON `PTK_2018032_CORE2`._PARENT_AURI = `PTK_2018032_CORE`._URI
        JOIN `PTK_2018032_LD_INFANT` 
        ON `PTK_2018032_LD_INFANT`._PARENT_AURI = `PTK_2018032_CORE`._URI
        WHERE  visit_type = '2'   
   UNION ALL            
        SELECT 
                ptracker_id AS  'ptracker_id',
                facility    AS  'facility_code',
                facility_name AS 'facility_name',
                null AS 'facility_uuid', 
                'Infant L&D' AS 'type',
                `PTK_201810_CORE`._uri AS 'odk_uuid',
                -- INFANT L&D PART --
                info2_infant_ptracker_number AS  'infant_ptracker_id',
                infant_status AS 'infant_status',
                info_infant_sex  AS  'infant_sex',
                Date_format(info_infant_dob, "%Y-%m-%d %h:%i:%s") AS 'infant_dob',
                info_still_birth AS  'infant_stillbirth',
                info_infant_feeding     AS 'infant_feeding',
                Date_format(info_infant_dod, "%Y-%m-%d %h:%i:%s") AS 'infant_dod',
                info_infant_dod_missing AS 'infant_dod_missing',            
                info_infant_arv  AS   'infant_arv_status',
                info2_infant_arv_reason_for_refusal   AS   'infant_arv_reason_for_refusal',
                info2_infant_arv_reason_for_refusal_missing       AS  'infant_arv_reason_for_refusal_missing',
                -- MISC --
                mrs_openmrs_user_uuid   AS 'provider_uuid',
                ptracker_username       AS 'username',
                _SUBMISSION_DATE AS 'submission_date',
                `PTK_201810_CORE`._uri  AS _URI,
                'PTK_201810' as version
        FROM `PTK_201810_CORE`
        JOIN `PTK_201810_CORE2` 
        ON `PTK_201810_CORE2`._PARENT_AURI = `PTK_201810_CORE`._URI
        JOIN `PTK_201810_LD_INFANT` 
        ON `PTK_201810_LD_INFANT`._PARENT_AURI = `PTK_201810_CORE`._URI
        WHERE  visit_type = '2'
    UNION ALL
        SELECT 
                ptracker_id AS  'ptracker_id',
                facility    AS  'facility_code',
                facility_name AS 'facility_name',
                null AS 'facility_uuid', 
                'Infant L&D' AS 'type',
                `PTK_201902_CORE`._uri AS 'odk_uuid',
                -- INFANT L&D PART --
                info2_infant_ptracker_number   AS   'infant_ptracker_id',
                infant_status AS 'infant_status',
                info_infant_sex  AS  'infant_sex',
                Date_format(info_infant_dob, "%Y-%m-%d %h:%i:%s") AS 'infant_dob',
                info_still_birth AS  'infant_stillbirth',
                info_infant_feeding     AS 'infant_feeding',
                Date_format(info_infant_dod, "%Y-%m-%d %h:%i:%s") AS 'infant_dod',
                info_infant_dod_missing AS 'infant_dod_missing',            
                info_infant_arv  AS   'infant_arv_status',
                info2_infant_arv_reason_for_refusal   AS   'infant_arv_reason_for_refusal',
                info2_infant_arv_reason_for_refusal_missing       AS  'infant_arv_reason_for_refusal_missing',
                -- MISC --
                mrs_openmrs_user_uuid   AS 'provider_uuid',
                ptracker_username       AS 'username',
                _SUBMISSION_DATE AS 'submission_date',
                `PTK_201902_CORE`._uri  AS _URI,
                'PTK_201902' as version
        FROM `PTK_201902_CORE`
        JOIN `PTK_201902_CORE2` 
        ON `PTK_201902_CORE2`._PARENT_AURI = `PTK_201902_CORE`._URI
        JOIN `PTK_201902_LD_INFANT` 
        ON `PTK_201902_LD_INFANT`._PARENT_AURI = `PTK_201902_CORE`._URI
        WHERE  visit_type = '2'
   UNION ALL
        SELECT 
                ptracker_id AS  'ptracker_id',
                facility    AS  'facility_code',
                facility_name AS 'facility_name',
                null AS 'facility_uuid', 
                'Infant L&D' AS 'type',
                `PTK_2020_02_CORE`._uri AS 'odk_uuid',
                -- INFANT L&D PART --
                info2_infant_ptracker_number   AS   'infant_ptracker_id',
                infant_status AS 'infant_status',
                info_infant_sex  AS  'infant_sex',
                Date_format(info_infant_dob, "%Y-%m-%d %h:%i:%s") AS 'infant_dob',
                info_still_birth AS  'infant_stillbirth',
                info_infant_feeding     AS 'infant_feeding',
                Date_format(info_infant_dod, "%Y-%m-%d %h:%i:%s") AS 'infant_dod',
                info_infant_dod_missing AS 'infant_dod_missing',            
                info_infant_arv  AS   'infant_arv_status',
                info2_infant_arv_reason_for_refusal   AS   'infant_arv_reason_for_refusal',
                info2_infant_arv_reason_for_refusal_missing       AS  'infant_arv_reason_for_refusal_missing',
                -- MISC --
                mrs_openmrs_user_uuid   AS 'provider_uuid',
                ptracker_username       AS 'username',
                _SUBMISSION_DATE AS 'submission_date',
                `PTK_2020_02_CORE`._uri  AS _URI,
                'PTK_2020_02' as version
        FROM `PTK_2020_02_CORE` 
        JOIN `PTK_2020_02_CORE2` 
        ON `PTK_2020_02_CORE2`._PARENT_AURI = `PTK_2020_02_CORE`._URI
        JOIN `PTK_2020_02_LD_INFANT` 
        ON `PTK_2020_02_LD_INFANT`._PARENT_AURI = `PTK_2020_02_CORE`._URI
        WHERE  visit_type = '2'
   UNION ALL
        SELECT 
                ptracker_id AS  'ptracker_id',
                facility    AS  'facility_code',
                facility_name AS 'facility_name',
                null AS 'facility_uuid', 
                'Infant L&D' AS 'type',
                `PTK_2020_03_CORE`._uri AS 'odk_uuid',
                -- INFANT L&D PART --
                info2_infant_ptracker_number AS  'infant_ptracker_id',
                infant_status AS 'infant_status',
                info_infant_sex  AS  'infant_sex',
                Date_format(info_infant_dob, "%Y-%m-%d %h:%i:%s") AS 'infant_dob',
                info_still_birth AS  'infant_stillbirth',
                info_infant_feeding     AS 'infant_feeding',
                Date_format(info_infant_dod, "%Y-%m-%d %h:%i:%s") AS 'infant_dod',
                info_infant_dod_missing AS 'infant_dod_missing',            
                info_infant_arv  AS   'infant_arv_status',
                info2_infant_arv_reason_for_refusal   AS   'infant_arv_reason_for_refusal',
                info2_infant_arv_reason_for_refusal_missing       AS  'infant_arv_reason_for_refusal_missing',
                -- MISC --
                mrs_openmrs_user_uuid   AS 'provider_uuid',
                ptracker_username       AS 'username',
                _SUBMISSION_DATE AS 'submission_date',
                `PTK_2020_03_CORE`._uri  AS _URI,
                'PTK_2020_03' as version
        FROM `PTK_2020_03_CORE` 
        JOIN `PTK_2020_03_CORE2` 
        ON `PTK_2020_03_CORE2`._PARENT_AURI = `PTK_2020_03_CORE`._URI
        JOIN `PTK_2020_03_CORE3` 
        ON `PTK_2020_03_CORE3`._PARENT_AURI = `PTK_2020_03_CORE`._URI
        JOIN `PTK_2020_03_LD_INFANT` 
        ON `PTK_2020_03_LD_INFANT`._PARENT_AURI = `PTK_2020_03_CORE`._URI
        WHERE  visit_type = '2'
   UNION ALL
        SELECT 
                ptracker_id AS  'ptracker_id',
                facility    AS  'facility_code',
                facility_name AS 'facility_name',
                facility_uuid AS 'facility_uuid', 
                'Infant L&D' AS 'type',
                `PTK_2020_10_CORE`._uri AS 'odk_uuid',
                -- INFANT L&D PART --
                info2_infant_ptracker_number AS  'infant_ptracker_id',
                infant_status AS 'infant_status',
                info_infant_sex  AS  'infant_sex',
                Date_format(info_infant_dob, "%Y-%m-%d %h:%i:%s") AS 'infant_dob',
                info_still_birth AS  'infant_stillbirth',
                info_infant_feeding     AS 'infant_feeding',
                Date_format(info_infant_dod, "%Y-%m-%d %h:%i:%s") AS 'infant_dod',
                info_infant_dod_missing AS 'infant_dod_missing',            
                info_infant_arv  AS   'infant_arv_status',
                info2_infant_arv_reason_for_refusal   AS   'infant_arv_reason_for_refusal',
                info2_infant_arv_reason_for_refusal_missing       AS  'infant_arv_reason_for_refusal_missing',
                -- MISC --
                mrs_openmrs_user_uuid   AS 'provider_uuid',
                ptracker_username       AS 'username',
                _SUBMISSION_DATE AS 'submission_date',
                `PTK_2020_10_CORE`._uri  AS _URI,
                'PTK_2020_10_CORE' as version
        FROM `PTK_2020_10_CORE` 
        JOIN `PTK_2020_10_CORE2` 
        ON `PTK_2020_10_CORE2`._PARENT_AURI = `PTK_2020_10_CORE`._URI
        JOIN `PTK_2020_10_CORE3` 
        ON `PTK_2020_10_CORE3`._PARENT_AURI = `PTK_2020_10_CORE`._URI
        JOIN `PTK_2020_10_LD_INFANT` 
        ON `PTK_2020_10_LD_INFANT`._PARENT_AURI = `PTK_2020_10_CORE`._URI
        WHERE  visit_type = '2';        