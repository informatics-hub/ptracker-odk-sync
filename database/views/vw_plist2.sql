-- create view of all patients with encounters 		
DROP VIEW IF EXISTS vw_plist_1; 
CREATE VIEW vw_plist_1 AS
	SELECT
		pi.patient_id as '_key', -- change to p.person_id to main
		e.patient_id,
		(SELECT COUNT(*) FROM encounter WHERE patient_id = e.patient_id) AS total_visits,
		(SELECT COUNT(*) FROM encounter WHERE patient_id = e.patient_id AND encounter_type = 8) AS anc_visit,
		(SELECT COUNT(*) FROM encounter WHERE patient_id = e.patient_id AND (encounter_type = 10 OR encounter_type = 11))  AS pnc_visit,
		(SELECT COUNT(*) FROM encounter WHERE patient_id = e.patient_id AND encounter_type = 9)  AS l_and_d_visit,
		MAX(l.NAME) AS facility_of_last_visit,
		MAX(DATE_FORMAT( e.encounter_datetime, '%M %d, %Y' )) AS date_of_last_visit, 
		MAX(e.encounter_datetime) AS encounter_datetime, 
		MAX( IF ( pi.identifier_type = 4, pi.identifier, NULL ) ) AS art_number,
		MAX(
			IF
				( o.concept_id = 159427, DATE_FORMAT( e.encounter_datetime, '%M %d, %Y' ), NULL) 
			) AS date_of_last_ht,
		MAX(
			IF
				( o.concept_id = 164401, (CASE WHEN (o.value_coded = 164911) THEN 1 ELSE 0 END), NULL) 
			) AS kp,
		MAX(
			IF
				( o.concept_id = 159427, (CASE WHEN (o.value_coded = 138571) THEN 1 ELSE 0 END), NULL) 
			) AS np,
		COALESCE(MAX(
			IF
				( o.concept_id = 164401, (CASE WHEN (o.value_coded = 164911) THEN 1 ELSE NULL END), NULL) 
			) ,
		MAX(
			IF
				( o.concept_id = 159427, (CASE WHEN (o.value_coded = 138571) THEN 1 ELSE 0 END), 0) 
			) ) AS hstatus,
		MAX( IF ( pi.identifier_type = 3, pi.identifier, NULL ) ) AS openmrs_id,
		MAX( IF ( pi.identifier_type = 5, pi.identifier, NULL ) ) AS ptracker_id,
		MAX(l.NAME) AS facility,
		pn.given_name as gn,
		pn.middle_name as mn,
		pn.family_name as fn,
		pn.uuid as 'openmrs_person_uuid',
		pn.uuid as 'openmrs_patient_uuid',
		DATE_FORMAT( p.birthdate, '%M %d, %Y' ) AS birthdate,
		(SELECT IFNULL( (select rn.relationship_id from relationship rn where rn.person_b = e.patient_id and rn.relationship = 3 LIMIT 1) ,0)) as has_parent	
	FROM
		encounter e
			INNER JOIN obs o ON e.encounter_id = o.encounter_id -- AND o.voided = 0
			INNER JOIN person_name pn ON e.patient_id = pn.person_id 
			INNER JOIN person p ON e.patient_id = p.person_id
			INNER JOIN patient_identifier pi ON e.patient_id = pi.patient_id
			INNER JOIN location l ON e.location_id = l.location_id
			INNER JOIN encounter_type et ON et.encounter_type_id = e.encounter_type 
			AND et.uuid IN ( '2549af50-75c8-4aeb-87ca-4bb2cef6c69a', '269bcc7f-04f8-4ddc-883d-7a3a0d569aad', '2678423c-0523-4d76-b0da-18177b439eed' ) 
		WHERE pn.voided = 0 
		GROUP BY e.encounter_id;

-- create view of all patients without encounters 		
DROP VIEW IF EXISTS vw_plist_2; 
CREATE VIEW vw_plist_2 AS
	SELECT
		p.person_id AS '_key', -- change to p.person_id to main
		pi.patient_id,
		MAX( IF ( pi.identifier_type = 3, pi.identifier, NULL ) ) AS openmrs_id,
		MAX( IF ( pi.identifier_type = 5, pi.identifier, NULL ) ) AS ptracker_id,
		pn.given_name as gn,
		pn.middle_name as mn,
		pn.family_name as fn,
		p.uuid AS 'openmrs_person_uuid',
		p.uuid AS 'openmrs_patient_uuid',
		DATE_FORMAT( p.birthdate, '%M %d, %Y' ) AS birthdate,
		(SELECT IFNULL( (SELECT rn.relationship_id FROM relationship rn WHERE rn.person_b = p.person_id AND rn.relationship = 3 LIMIT 1) ,0)) AS has_parent	
	FROM
		person p
			INNER JOIN person_name pn ON p.person_id = pn.person_id 
			INNER JOIN patient_identifier pi ON p.person_id = pi.patient_id
	WHERE
		pi.voided = 0 
	AND 
		p.person_id NOT IN (SELECT patient_id FROM vw_plist_1)
	GROUP BY 
		p.person_id;

-- create view with all patients
DROP VIEW IF EXISTS vw_plist; 
CREATE VIEW vw_plist AS
	SELECT
		_key,
		patient_id,
		openmrs_id,
		ptracker_id,
		null as encounter_datetime,
		null as facility,
		null as hstatus,
		null as np,
		null as kp,
		null as date_of_last_ht,
		null as date_of_last_visit,
		null as facility_of_last_visit,
		null as l_and_d_visit,
		null as pnc_visit,
		null as anc_visit,
		null as total_visits,
		gn,
		mn,
		fn,
		openmrs_person_uuid,
		openmrs_patient_uuid,
		birthdate,
		has_parent
	FROM
		vw_plist_2
UNION ALL
    SELECT
		_key,
		patient_id,
		openmrs_id,
		ptracker_id,
		encounter_datetime,
		facility,
		hstatus,
		np,
		kp,
		date_of_last_ht,
		date_of_last_visit,
		facility_of_last_visit,
		l_and_d_visit,
		pnc_visit,
		anc_visit,
		total_visits,
		gn,
		mn,
		fn,
		openmrs_person_uuid,
		openmrs_patient_uuid,
		birthdate,
		has_parent
	FROM
		vw_plist_1;