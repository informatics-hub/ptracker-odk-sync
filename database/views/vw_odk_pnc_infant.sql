DROP VIEW IF EXISTS vw_odk_pnc_infant;  
CREATE VIEW vw_odk_pnc_infant AS
           SELECT
                PTRACKER_ID AS 'ptracker_id' ,
                FACILITY AS 'facility_code' , 
                FACILITY_NAME AS 'facility_name' ,
                'Infant PNC' AS 'type' ,
                DATE_FORMAT(visit_date, "%Y-%m-%d %h:%i:%s") AS 'visit_date' ,
                DATE_FORMAT(visit_date, "%Y-%m-%d %h:%i:%s") AS 'visit_date_recorded_odk' ,
                NULL AS 'openmrs_id' , -- opernmrs id not supplied through csv-- 
                MRS_PERSON_UUID AS 'openmrs_person_uuid' ,
                MRS_PATIENT_UUID AS 'openmrs_patient_uuid', 
                null AS 'openmrs_parent_uuid' ,
                null AS 'facility_uuid',
                PTK_201803_CORE._URI AS 'odk_uuid' ,
                CLIENT_INFO_NAME_GIVEN AS 'given' ,
                CLIENT_INFO_NAME_MIDDLE AS 'middle' ,
                CLIENT_INFO_NAME_FAMILY AS 'family' ,
                INFANTNAME_NEW_GIVEN AS 'given_new' ,
                INFANTNAME_NEW_FAMILY AS 'family_new' ,
                INFANTNAME_NEW_MIDDLE AS 'middle_new' ,
                CLIENT_INFO_NAME_SEX AS  'sex' ,
                DATE_FORMAT(CLIENT_INFO_INFANTDOB_DOB_INFANT, "%Y-%m-%d %h:%i:%s")   AS 'dob_infant',  
                CLIENT_INFO_CLIENT_CONTACT_COUNTRY AS  'country' ,
                CLIENT_INFO_CLIENT_CONTACT_COUNTRY_OTHER AS  'country_other' ,
                CLIENT_INFO_CLIENT_CONTACT_CLIENT_CONTACT_DISTRICT AS  'district' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPADDRESS_ADDRESS AS  'address' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPLOCATION_LOCATION AS  'location' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPPHONE_PHONE_NUMBER AS  'phone_number' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPKINNAME_KIN_NAME AS  'kin_name' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPKINCONTACT_KIN_CONTACT AS  'kin_contact' ,
                -- INFANT PNC PART -- 
                INFANTPNC_INFANT_TRANSFER_STATUS AS 'infant_transfer_status',
                NULL AS 'infant_breastfeeding_status',
                INFANTPNC_INFANT_TRANSFER_STATUS_TRANSFEREDTO AS 'infant_transfer_out',
                INFANTPNC_INFANT_TRANSFER_STATUS_TRANSFEREDTO_OTHER AS 'infant_transfer_out_other',
                DATE_FORMAT(INFANTPNC_GRPINFANTTRNSFOUTDATE_INFANT_TRANSFEROUT_DATE, "%Y-%m-%d %h:%i:%s") AS 'infant_transfer_out_date',
                INFANTPNC_GRPINFANTTRNSFOUTDATE_INFANT_TRANSFEROUT_DATE_MISSING AS 'infant_transfer_out_date_missing',
                INFANTPNC_INFANT_TRANSFERIN_FROM AS 'infant_transferin',
                INFANTPNC_INFANT_TRANSFERIN_FROM_OTHER AS  'infant_transferin_other',
                DATE_FORMAT(INFANTPNC_GRPINFANTTRNSFDATE_INFANT_TRANSFERIN_DATE, "%Y-%m-%d %h:%i:%s")   AS 'infant_transferin_date',
                INFANTPNC_GRPINFANTTRNSFDATE_INFANT_TRANSFERIN_DATE_MISSING AS 'infant_transferin_date_missing',
                DATE_FORMAT(INFANTPNC_GRPINFANTNXTVISITDATE_INFANT_NEXT_VISIT_DATE, "%Y-%m-%d %h:%i:%s")   AS 'infant_next_visit_date', 
                INFANTPNC_GRPINFANTNXTVISITDATE_INFANT_NEXT_VISIT_DATE_MISSING AS 'infant_next_visit_date_missing', 
                NULL AS 'infant_event_date',
                NULL AS 'infant_event_date_missing',
                NULL AS 'infant_transferto_artclinic_date',
                NULL AS 'infant_transferto_artclinic_date_missing',
                NULL AS 'infant_transferto_artclinic',
                NULL AS 'infant_transferto_artclinic_missing',
                INFANTPNC_HIV_EXPOSURE_STATUS AS 'hiv_exposure_status' ,
                INFANTPNC_ARV_PROPHYLAXIS_STATUS AS 'arv_prophylaxis_status',
                INFANTPNC_ARV_PROPHYLAXIS_ADHERENCE AS 'arv_prophylaxis_adherence' ,
                INFANTPNC_CTX_PROPHYLAXIS_STATUS AS 'ctx_prophylaxis_status' ,
                INFANTPNC_CTX_PROPHYLAXIS_ADHERENCE AS 'ctx_prophylaxis_adherence' ,
                INFANTPNC_INFANT_HIV_TESTED AS 'infant_hiv_tested' ,
                INFANTPNC_INFANT_HIV_TEST_USED AS 'infant_hiv_test_used' ,
                INFANTPNC_INFANT_HIV_TEST_RESULT AS 'infant_hiv_test_result' ,
                INFANTPNC_INFANT_HIV_TEST_RESULT_PCR AS 'infant_hiv_test_result_pcr' ,
                INFANTPNC_INFANT_HIV_TEST_CONF AS 'infant_hiv_test_conf' ,
                INFANTPNC_INFANT_HIV_TEST_CONF_RESULT AS 'infant_hiv_test_conf_result' ,
                INFANTPNC_INFANT_ART_LINKED AS 'infant_art_linked' ,  -- Was this infant linked to ART --
                INFANTPNC_GRPINFANTART_INFANT_ART_NUMBER AS  'art_number' ,
                INFANTPNC_GRPINFANTART_INFANT_ART_NUMBER_MISSING AS 'art_number_missing' ,
                DATE_FORMAT(INFANTPNC_GRPINFANTDEATHDATE_INFANT_DATE_DEATH, "%Y-%m-%d %h:%i:%s")   AS 'infant_date_death',  
                INFANTPNC_GRPINFANTDEATHDATE_INFANT_DATE_DEATH_MISSING AS 'infant_date_death_mising', 
                MRS_OPENMRS_USER_UUID AS  'provider_uuid', 
                PTRACKER_USERNAME AS 'username',
                _SUBMISSION_DATE AS 'submission_date',
                PTK_201803_CORE._URI AS _URI,
                INFANTPNC_ID_PARENT AS 'parent_ptracker_id',
                'PTK_201803' as version
            FROM PTK_201803_CORE
            JOIN PTK_201803_CORE2
            ON PTK_201803_CORE2._PARENT_AURI = PTK_201803_CORE._URI
            WHERE visit_type = '4'   
        UNION ALL
           SELECT
                PTRACKER_ID AS 'ptracker_id' ,
                FACILITY AS 'facility_code' , 
                FACILITY_NAME AS 'facility_name' ,
                'Infant PNC' AS 'type' ,
                DATE_FORMAT(visit_date, "%Y-%m-%d %h:%i:%s") AS 'visit_date' ,
                DATE_FORMAT(visit_date, "%Y-%m-%d %h:%i:%s") AS 'visit_date_recorded_odk' ,
                NULL AS 'openmrs_id' , -- opernmrs id not supplied through csv-- 
                MRS_PERSON_UUID AS 'openmrs_person_uuid' ,
                MRS_PATIENT_UUID AS 'openmrs_patient_uuid', 
                null AS 'openmrs_parent_uuid' ,
                null AS 'facility_uuid',
                PTK_2018032_CORE._URI AS 'odk_uuid' ,
                CLIENT_INFO_NAME_GIVEN AS 'given' ,
                CLIENT_INFO_NAME_MIDDLE AS 'middle' ,
                CLIENT_INFO_NAME_FAMILY AS 'family' ,
                INFANTNAME_NEW_GIVEN AS 'given_new' ,
                INFANTNAME_NEW_FAMILY AS 'family_new' ,
                INFANTNAME_NEW_MIDDLE AS 'middle_new' ,
                CLIENT_INFO_NAME_SEX AS  'sex' ,
                DATE_FORMAT(CLIENT_INFO_INFANTDOB_DOB_INFANT, "%Y-%m-%d %h:%i:%s")   AS 'dob_infant',  
                CLIENT_INFO_CLIENT_CONTACT_COUNTRY AS  'country' ,
                CLIENT_INFO_CLIENT_CONTACT_COUNTRY_OTHER AS  'country_other' ,
                CLIENT_INFO_CLIENT_CONTACT_CLIENT_CONTACT_DISTRICT AS  'district' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPADDRESS_ADDRESS AS  'address' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPLOCATION_LOCATION AS  'location' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPPHONE_PHONE_NUMBER AS  'phone_number' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPKINNAME_KIN_NAME AS  'kin_name' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPKINCONTACT_KIN_CONTACT AS  'kin_contact' ,
                -- INFANT PNC PART -- 
                INFANTPNC_INFANT_TRANSFER_STATUS AS 'infant_transfer_status',
                NULL AS 'infant_breastfeeding_status',
                INFANTPNC_INFANT_TRANSFER_STATUS_TRANSFEREDTO AS 'infant_transfer_out',
                INFANTPNC_INFANT_TRANSFER_STATUS_TRANSFEREDTO_OTHER AS 'infant_transfer_out_other',
                DATE_FORMAT(INFANTPNC_GRPINFANTTRNSFOUTDATE_INFANT_TRANSFEROUT_DATE, "%Y-%m-%d %h:%i:%s") AS 'infant_transfer_out_date',
                INFANTPNC_GRPINFANTTRNSFOUTDATE_INFANT_TRANSFEROUT_DATE_MISSING AS 'infant_transfer_out_date_missing',
                INFANTPNC_INFANT_TRANSFERIN_FROM AS 'infant_transferin',
                INFANTPNC_INFANT_TRANSFERIN_FROM_OTHER AS  'infant_transferin_other',
                DATE_FORMAT(INFANTPNC_GRPINFANTTRNSFDATE_INFANT_TRANSFERIN_DATE, "%Y-%m-%d %h:%i:%s")   AS 'infant_transferin_date',
                INFANTPNC_GRPINFANTTRNSFDATE_INFANT_TRANSFERIN_DATE_MISSING AS 'infant_transferin_date_missing',
                DATE_FORMAT(INFANTPNC_GRPINFANTNXTVISITDATE_INFANT_NEXT_VISIT_DATE, "%Y-%m-%d %h:%i:%s")   AS 'infant_next_visit_date', 
                INFANTPNC_GRPINFANTNXTVISITDATE_INFANT_NEXT_VISIT_DATE_MISSING AS 'infant_next_visit_date_missing', 
                NULL AS 'infant_event_date',
                NULL AS 'infant_event_date_missing',
                NULL AS 'infant_transferto_artclinic_date',
                NULL AS 'infant_transferto_artclinic_date_missing',
                NULL AS 'infant_transferto_artclinic',
                NULL AS 'infant_transferto_artclinic_missing',
                INFANTPNC_HIV_EXPOSURE_STATUS AS 'hiv_exposure_status' ,
                INFANTPNC_ARV_PROPHYLAXIS_STATUS AS 'arv_prophylaxis_status',
                INFANTPNC_ARV_PROPHYLAXIS_ADHERENCE AS 'arv_prophylaxis_adherence' ,
                INFANTPNC_CTX_PROPHYLAXIS_STATUS AS 'ctx_prophylaxis_status' ,
                INFANTPNC_CTX_PROPHYLAXIS_ADHERENCE AS 'ctx_prophylaxis_adherence' ,
                INFANTPNC_INFANT_HIV_TESTED AS 'infant_hiv_tested' ,
                INFANTPNC_INFANT_HIV_TEST_USED AS 'infant_hiv_test_used' ,
                INFANTPNC_INFANT_HIV_TEST_RESULT AS 'infant_hiv_test_result' ,
                INFANTPNC_INFANT_HIV_TEST_RESULT_PCR AS 'infant_hiv_test_result_pcr' ,
                INFANTPNC_INFANT_HIV_TEST_CONF AS 'infant_hiv_test_conf' ,
                INFANTPNC_INFANT_HIV_TEST_CONF_RESULT AS 'infant_hiv_test_conf_result' ,
                INFANTPNC_INFANT_ART_LINKED AS 'infant_art_linked' ,  -- Was this infant linked to ART --
                INFANTPNC_GRPINFANTART_INFANT_ART_NUMBER AS  'art_number' ,
                INFANTPNC_GRPINFANTART_INFANT_ART_NUMBER_MISSING AS 'art_number_missing' ,
                DATE_FORMAT(INFANTPNC_GRPINFANTDEATHDATE_INFANT_DATE_DEATH, "%Y-%m-%d %h:%i:%s")   AS 'infant_date_death',  
                INFANTPNC_GRPINFANTDEATHDATE_INFANT_DATE_DEATH_MISSING AS 'infant_date_death_mising', 
                MRS_OPENMRS_USER_UUID AS  'provider_uuid', 
                PTRACKER_USERNAME AS 'username',
                _SUBMISSION_DATE AS 'submission_date',
                PTK_2018032_CORE._URI AS _URI,
                INFANTPNC_ID_PARENT AS 'parent_ptracker_id',
                'PTK_2018032' as version
            FROM PTK_2018032_CORE
            JOIN PTK_2018032_CORE2
            ON PTK_2018032_CORE2._PARENT_AURI = PTK_2018032_CORE._URI
            WHERE visit_type = '4'            
        UNION ALL
           SELECT
                PTRACKER_ID AS 'ptracker_id' ,
                FACILITY AS 'facility_code' , 
                FACILITY_NAME AS 'facility_name' ,
                'Infant PNC' AS 'type' ,
                DATE_FORMAT(visit_date, "%Y-%m-%d %h:%i:%s") AS 'visit_date' ,
                DATE_FORMAT(visit_date, "%Y-%m-%d %h:%i:%s") AS 'visit_date_recorded_odk' ,
                NULL AS 'openmrs_id' , -- opernmrs id not supplied through csv-- 
                MRS_PERSON_UUID AS 'openmrs_person_uuid' ,
                MRS_PATIENT_UUID AS 'openmrs_patient_uuid', 
                null AS 'openmrs_parent_uuid' ,
                null AS 'facility_uuid',
                PTK_201810_CORE._URI AS 'odk_uuid' ,
                CLIENT_INFO_NAME_GIVEN AS 'given' ,
                CLIENT_INFO_NAME_MIDDLE AS 'middle' ,
                CLIENT_INFO_NAME_FAMILY AS 'family' ,
                INFANTNAME_NEW_GIVEN AS 'given_new' ,
                INFANTNAME_NEW_FAMILY AS 'family_new' ,
                INFANTNAME_NEW_MIDDLE AS 'middle_new' ,
                CLIENT_INFO_NAME_SEX AS  'sex' ,
                DATE_FORMAT(CLIENT_INFO_INFANTDOB_DOB_INFANT, "%Y-%m-%d %h:%i:%s")   AS 'dob_infant',  
                CLIENT_INFO_CLIENT_CONTACT_COUNTRY AS  'country' ,
                CLIENT_INFO_CLIENT_CONTACT_COUNTRY_OTHER AS  'country_other' ,
                CLIENT_INFO_CLIENT_CONTACT_CLIENT_CONTACT_DISTRICT AS  'district' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPADDRESS_ADDRESS AS  'address' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPLOCATION_LOCATION AS  'location' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPPHONE_PHONE_NUMBER AS  'phone_number' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPKINNAME_KIN_NAME AS  'kin_name' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPKINCONTACT_KIN_CONTACT AS  'kin_contact' ,
                -- INFANT PNC PART -- 
                INFANTPNC_INFANT_TRANSFER_STATUS AS 'infant_transfer_status',
                INFANTPNC_PNC_BREASTFEEDING_STATUS AS 'infant_breastfeeding_status',
                INFANTPNC_INFANT_TRANSFER_STATUS_TRANSFEREDTO AS 'infant_transfer_out',
                INFANTPNC_INFANT_TRANSFER_STATUS_TRANSFEREDTO_OTHER AS 'infant_transfer_out_other',
                DATE_FORMAT(INFANTPNC_GRPINFANTTRNSFOUTDATE_INFANT_TRANSFEROUT_DATE, "%Y-%m-%d %h:%i:%s") AS 'infant_transfer_out_date',
                INFANTPNC_GRPINFANTTRNSFOUTDATE_INFANT_TRANSFEROUT_DATE_MISSING AS 'infant_transfer_out_date_missing',
                INFANTPNC_INFANT_TRANSFERIN_FROM AS 'infant_transferin',
                INFANTPNC_INFANT_TRANSFERIN_FROM_OTHER AS  'infant_transferin_other',
                DATE_FORMAT(INFANTPNC_GRPINFANTTRNSFDATE_INFANT_TRANSFERIN_DATE, "%Y-%m-%d %h:%i:%s")   AS 'infant_transferin_date',
                INFANTPNC_GRPINFANTTRNSFDATE_INFANT_TRANSFERIN_DATE_MISSING AS 'infant_transferin_date_missing',
                DATE_FORMAT(INFANTPNC_GRPINFANTNXTVISITDATE_INFANT_NEXT_VISIT_DATE, "%Y-%m-%d %h:%i:%s")   AS 'infant_next_visit_date', 
                INFANTPNC_GRPINFANTNXTVISITDATE_INFANT_NEXT_VISIT_DATE_MISSING AS 'infant_next_visit_date_missing', 
                DATE_FORMAT(INFANTPNC_GRPINFANTEVENTDATE_INFANT_EVENT_DATE, "%Y-%m-%d %h:%i:%s")  AS 'infant_event_date',
                INFANTPNC_GRPINFANTEVENTDATE_INFANT_EVENT_DATE_MISSING AS 'infant_event_date_missing',
                DATE_FORMAT(INFANTPNC_16TCLINICDATE_INFANT_TRANSFERTO_ARTCLINIC_DATE, "%Y-%m-%d %h:%i:%s")  AS 'infant_transferto_artclinic_date',
                INFANTPNC_16TCLINICDATE_INFANT_TRANSFERTO_ARTCLINIC_DATE_MISSING AS 'infant_transferto_artclinic_date_missing',
                INFANTPNC_INFANT_TRANSFERTO_ARTCLINIC AS 'infant_transferto_artclinic',
                INFANTPNC_INFANT_TRANSFERTO_ARTCLINIC_OTHER AS 'infant_transferto_artclinic_missing',
                INFANTPNC_HIV_EXPOSURE_STATUS AS 'hiv_exposure_status' ,
                INFANTPNC_ARV_PROPHYLAXIS_STATUS AS 'arv_prophylaxis_status',
                INFANTPNC_ARV_PROPHYLAXIS_ADHERENCE AS 'arv_prophylaxis_adherence' ,
                INFANTPNC_CTX_PROPHYLAXIS_STATUS AS 'ctx_prophylaxis_status' ,
                INFANTPNC_CTX_PROPHYLAXIS_ADHERENCE AS 'ctx_prophylaxis_adherence' ,
                INFANTPNC_INFANT_HIV_TESTED AS 'infant_hiv_tested' ,
                INFANTPNC_INFANT_HIV_TEST_USED AS 'infant_hiv_test_used' ,
                INFANTPNC_INFANT_HIV_TEST_RESULT AS 'infant_hiv_test_result' ,
                INFANTPNC_INFANT_HIV_TEST_RESULT_PCR AS 'infant_hiv_test_result_pcr' ,
                INFANTPNC_INFANT_HIV_TEST_CONF AS 'infant_hiv_test_conf' ,
                INFANTPNC_INFANT_HIV_TEST_CONF_RESULT AS 'infant_hiv_test_conf_result' ,
                INFANTPNC_INFANT_ART_LINKED AS 'infant_art_linked' ,  -- Was this infant linked to ART --
                INFANTPNC_GRPINFANTART_INFANT_ART_NUMBER AS  'art_number' ,
                INFANTPNC_GRPINFANTART_INFANT_ART_NUMBER_MISSING AS 'art_number_missing' ,
                DATE_FORMAT(INFANTPNC_GRPINFANTDEATHDATE_INFANT_DATE_DEATH, "%Y-%m-%d %h:%i:%s")   AS 'infant_date_death',  
                INFANTPNC_GRPINFANTDEATHDATE_INFANT_DATE_DEATH_MISSING AS 'infant_date_death_mising', 
                MRS_OPENMRS_USER_UUID AS  'provider_uuid', 
                PTRACKER_USERNAME AS 'username',
                _SUBMISSION_DATE AS 'submission_date',
                PTK_201810_CORE._URI AS _URI,
                INFANTPNC_ID_PARENT AS 'parent_ptracker_id',
                'PTK_201810' as version
            FROM PTK_201810_CORE
            JOIN PTK_201810_CORE2
            ON PTK_201810_CORE2._PARENT_AURI = PTK_201810_CORE._URI
            WHERE visit_type = '4'
     UNION ALL
            SELECT
                PTRACKER_ID AS 'ptracker_id' ,
                FACILITY AS 'facility_code' , 
                FACILITY_NAME AS 'facility_name' ,
                'Infant PNC' AS 'type' ,
                DATE_FORMAT(visit_date, "%Y-%m-%d %h:%i:%s") AS 'visit_date' ,
                DATE_FORMAT(visit_date, "%Y-%m-%d %h:%i:%s") AS 'visit_date_recorded_odk' ,
                NULL AS 'openmrs_id' , -- opernmrs id not supplied through csv-- 
                MRS_PERSON_UUID AS 'openmrs_person_uuid' ,
                MRS_PATIENT_UUID AS 'openmrs_patient_uuid', 
                null AS 'openmrs_parent_uuid' ,
                null AS 'facility_uuid',
                PTK_201902_CORE._URI AS 'odk_uuid' ,
                CLIENT_INFO_NAME_GIVEN AS 'given' ,
                CLIENT_INFO_NAME_MIDDLE AS 'middle' ,
                CLIENT_INFO_NAME_FAMILY AS 'family' ,
                INFANTNAME_NEW_GIVEN AS 'given_new' ,
                INFANTNAME_NEW_FAMILY AS 'family_new' ,
                INFANTNAME_NEW_MIDDLE AS 'middle_new' ,
                CLIENT_INFO_NAME_SEX AS  'sex' ,
                DATE_FORMAT(CLIENT_INFO_INFANTDOB_DOB_INFANT, "%Y-%m-%d %h:%i:%s")   AS 'dob_infant',  
                CLIENT_INFO_CLIENT_CONTACT_COUNTRY AS  'country' ,
                CLIENT_INFO_CLIENT_CONTACT_COUNTRY_OTHER AS  'country_other' ,
                CLIENT_INFO_CLIENT_CONTACT_CLIENT_CONTACT_DISTRICT AS  'district' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPADDRESS_ADDRESS AS  'address' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPLOCATION_LOCATION AS  'location' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPPHONE_PHONE_NUMBER AS  'phone_number' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPKINNAME_KIN_NAME AS  'kin_name' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPKINCONTACT_KIN_CONTACT AS  'kin_contact' ,
                -- INFANT PNC PART -- 
                INFANTPNC_INFANT_TRANSFER_STATUS AS 'infant_transfer_status',
                INFANTPNC_PNC_BREASTFEEDING_STATUS AS 'infant_breastfeeding_status',
                INFANTPNC_INFANT_TRANSFER_STATUS_TRANSFEREDTO AS 'infant_transfer_out',
                INFANTPNC_INFANT_TRANSFER_STATUS_TRANSFEREDTO_OTHER AS 'infant_transfer_out_other',
                DATE_FORMAT(INFANTPNC_GRPINFANTTRNSFOUTDATE_INFANT_TRANSFEROUT_DATE, "%Y-%m-%d %h:%i:%s") AS 'infant_transfer_out_date',
                INFANTPNC_GRPINFANTTRNSFOUTDATE_INFANT_TRANSFEROUT_DATE_MISSING AS 'infant_transfer_out_date_missing',
                INFANTPNC_INFANT_TRANSFERIN_FROM AS 'infant_transferin',
                INFANTPNC_INFANT_TRANSFERIN_FROM_OTHER AS  'infant_transferin_other',
                DATE_FORMAT(INFANTPNC_GRPINFANTTRNSFDATE_INFANT_TRANSFERIN_DATE, "%Y-%m-%d %h:%i:%s")   AS 'infant_transferin_date',
                INFANTPNC_GRPINFANTTRNSFDATE_INFANT_TRANSFERIN_DATE_MISSING AS 'infant_transferin_date_missing',
                DATE_FORMAT(INFANTPNC_GRPINFANTNXTVISITDATE_INFANT_NEXT_VISIT_DATE, "%Y-%m-%d %h:%i:%s")   AS 'infant_next_visit_date', 
                INFANTPNC_GRPINFANTNXTVISITDATE_INFANT_NEXT_VISIT_DATE_MISSING AS 'infant_next_visit_date_missing', 
                DATE_FORMAT(INFANTPNC_GRPINFANTEVENTDATE_INFANT_EVENT_DATE, "%Y-%m-%d %h:%i:%s")  AS 'infant_event_date',
                INFANTPNC_GRPINFANTEVENTDATE_INFANT_EVENT_DATE_MISSING AS 'infant_event_date_missing',
                DATE_FORMAT(INFANTPNC_16TCLINICDATE_INFANT_TRANSFERTO_ARTCLINIC_DATE, "%Y-%m-%d %h:%i:%s")  AS 'infant_transferto_artclinic_date',
                INFANTPNC_16TCLINICDATE_INFANT_TRANSFERTO_ARTCLINIC_DATE_MISSING AS 'infant_transferto_artclinic_date_missing',
                INFANTPNC_INFANT_TRANSFERTO_ARTCLINIC AS 'infant_transferto_artclinic',
                INFANTPNC_INFANT_TRANSFERTO_ARTCLINIC_OTHER AS 'infant_transferto_artclinic_missing',
                INFANTPNC_HIV_EXPOSURE_STATUS AS 'hiv_exposure_status' ,
                INFANTPNC_ARV_PROPHYLAXIS_STATUS AS 'arv_prophylaxis_status',
                INFANTPNC_ARV_PROPHYLAXIS_ADHERENCE AS 'arv_prophylaxis_adherence' ,
                INFANTPNC_CTX_PROPHYLAXIS_STATUS AS 'ctx_prophylaxis_status' ,
                INFANTPNC_CTX_PROPHYLAXIS_ADHERENCE AS 'ctx_prophylaxis_adherence' ,
                INFANTPNC_INFANT_HIV_TESTED AS 'infant_hiv_tested' ,
                INFANTPNC_INFANT_HIV_TEST_USED AS 'infant_hiv_test_used' ,
                INFANTPNC_INFANT_HIV_TEST_RESULT AS 'infant_hiv_test_result' ,
                INFANTPNC_INFANT_HIV_TEST_RESULT_PCR AS 'infant_hiv_test_result_pcr' ,
                INFANTPNC_INFANT_HIV_TEST_CONF AS 'infant_hiv_test_conf' ,
                INFANTPNC_INFANT_HIV_TEST_CONF_RESULT AS 'infant_hiv_test_conf_result' ,
                INFANTPNC_INFANT_ART_LINKED AS 'infant_art_linked' ,  -- Was this infant linked to ART --
                INFANTPNC_GRPINFANTART_INFANT_ART_NUMBER AS  'art_number' ,
                INFANTPNC_GRPINFANTART_INFANT_ART_NUMBER_MISSING AS 'art_number_missing' ,
                DATE_FORMAT(INFANTPNC_GRPINFANTDEATHDATE_INFANT_DATE_DEATH, "%Y-%m-%d %h:%i:%s")   AS 'infant_date_death',  
                INFANTPNC_GRPINFANTDEATHDATE_INFANT_DATE_DEATH_MISSING AS 'infant_date_death_mising', 
                MRS_OPENMRS_USER_UUID AS  'provider_uuid', 
                PTRACKER_USERNAME AS 'username',
                _SUBMISSION_DATE AS 'submission_date',
                PTK_201902_CORE._URI AS _URI,
                INFANTPNC_ID_PARENT AS 'parent_ptracker_id',
                'PTK_201902' as version
            FROM PTK_201902_CORE
            JOIN PTK_201902_CORE2
            ON PTK_201902_CORE2._PARENT_AURI = PTK_201902_CORE._URI
            WHERE visit_type = '4'
        UNION ALL
            SELECT
                PTRACKER_ID AS 'ptracker_id' ,
                FACILITY AS 'facility_code' , 
                FACILITY_NAME AS 'facility_name' ,
                'Infant PNC' AS 'type' ,
                DATE_FORMAT(visit_date, "%Y-%m-%d %h:%i:%s") AS 'visit_date' ,
                DATE_FORMAT(visit_date, "%Y-%m-%d %h:%i:%s") AS 'visit_date_recorded_odk' ,
                NULL AS 'openmrs_id' , -- opernmrs id not supplied through csv-- 
                null AS 'openmrs_person_uuid' ,
                null AS 'openmrs_patient_uuid', 
                null AS 'openmrs_parent_uuid' ,
                null AS 'facility_uuid',
                PTK_2020_02_CORE._URI AS 'odk_uuid' ,
                CLIENT_INFO_NAME_GIVEN AS 'given' ,
                CLIENT_INFO_NAME_MIDDLE AS 'middle' ,
                CLIENT_INFO_NAME_FAMILY AS 'family' ,
                INFANTNAME_NEW_GIVEN AS 'given_new' ,
                INFANTNAME_NEW_FAMILY AS 'family_new' ,
                INFANTNAME_NEW_MIDDLE AS 'middle_new' ,
                CLIENT_INFO_NAME_SEX AS  'sex' ,
                DATE_FORMAT(CLIENT_INFO_INFANTDOB_DOB_INFANT, "%Y-%m-%d %h:%i:%s")   AS 'dob_infant',  
                CLIENT_INFO_CLIENT_CONTACT_COUNTRY AS  'country' ,
                CLIENT_INFO_CLIENT_CONTACT_COUNTRY_OTHER AS  'country_other' ,
                CLIENT_INFO_CLIENT_CONTACT_CLIENT_CONTACT_DISTRICT AS  'district' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPADDRESS_ADDRESS AS  'address' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPLOCATION_LOCATION AS  'location' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPPHONE_PHONE_NUMBER AS  'phone_number' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPKINNAME_KIN_NAME AS  'kin_name' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPKINCONTACT_KIN_CONTACT AS  'kin_contact' ,
                -- INFANT PNC PART -- 
                INFANTPNC_INFANT_TRANSFER_STATUS AS 'infant_transfer_status',
                INFANTPNC_PNC_BREASTFEEDING_STATUS AS 'infant_breastfeeding_status',
                INFANTPNC_INFANT_TRANSFER_STATUS_TRANSFEREDTO AS 'infant_transfer_out',
                INFANTPNC_INFANT_TRANSFER_STATUS_TRANSFEREDTO_OTHER AS 'infant_transfer_out_other',
                DATE_FORMAT(INFANTPNC_GRPINFANTTRNSFOUTDATE_INFANT_TRANSFEROUT_DATE, "%Y-%m-%d %h:%i:%s") AS 'infant_transfer_out_date',
                INFANTPNC_GRPINFANTTRNSFOUTDATE_INFANT_TRANSFEROUT_DATE_MISSING AS 'infant_transfer_out_date_missing',
                INFANTPNC_INFANT_TRANSFERIN_FROM AS 'infant_transferin',
                INFANTPNC_INFANT_TRANSFERIN_FROM_OTHER AS  'infant_transferin_other',
                DATE_FORMAT(INFANTPNC_GRPINFANTTRNSFDATE_INFANT_TRANSFERIN_DATE, "%Y-%m-%d %h:%i:%s")   AS 'infant_transferin_date',
                INFANTPNC_GRPINFANTTRNSFDATE_INFANT_TRANSFERIN_DATE_MISSING AS 'infant_transferin_date_missing',
                DATE_FORMAT(INFANTPNC_GRPINFANTNXTVISITDATE_INFANT_NEXT_VISIT_DATE, "%Y-%m-%d %h:%i:%s")   AS 'infant_next_visit_date', 
                INFANTPNC_GRPINFANTNXTVISITDATE_INFANT_NEXT_VISIT_DATE_MISSING AS 'infant_next_visit_date_missing', 
                DATE_FORMAT(INFANTPNC_GRPINFANTEVENTDATE_INFANT_EVENT_DATE, "%Y-%m-%d %h:%i:%s")  AS 'infant_event_date',
                INFANTPNC_GRPINFANTEVENTDATE_INFANT_EVENT_DATE_MISSING AS 'infant_event_date_missing',
                DATE_FORMAT(INFANTPNC_16TCLINICDATE_INFANT_TRANSFERTO_ARTCLINIC_DATE, "%Y-%m-%d %h:%i:%s")  AS 'infant_transferto_artclinic_date',
                INFANTPNC_16TCLINICDATE_INFANT_TRANSFERTO_ARTCLINIC_DATE_MISSING AS 'infant_transferto_artclinic_date_missing',
                INFANTPNC_INFANT_TRANSFERTO_ARTCLINIC AS 'infant_transferto_artclinic',
                INFANTPNC_INFANT_TRANSFERTO_ARTCLINIC_OTHER AS 'infant_transferto_artclinic_missing',
                INFANTPNC_HIV_EXPOSURE_STATUS AS 'hiv_exposure_status' ,
                INFANTPNC_ARV_PROPHYLAXIS_STATUS AS 'arv_prophylaxis_status',
                INFANTPNC_ARV_PROPHYLAXIS_ADHERENCE AS 'arv_prophylaxis_adherence' ,
                INFANTPNC_CTX_PROPHYLAXIS_STATUS AS 'ctx_prophylaxis_status' ,
                INFANTPNC_CTX_PROPHYLAXIS_ADHERENCE AS 'ctx_prophylaxis_adherence' ,
                INFANTPNC_INFANT_HIV_TESTED AS 'infant_hiv_tested' ,
                INFANTPNC_INFANT_HIV_TEST_USED AS 'infant_hiv_test_used' ,
                INFANTPNC_INFANT_HIV_TEST_RESULT AS 'infant_hiv_test_result' ,
                INFANTPNC_INFANT_HIV_TEST_RESULT_PCR AS 'infant_hiv_test_result_pcr' ,
                INFANTPNC_INFANT_HIV_TEST_CONF AS 'infant_hiv_test_conf' ,
                INFANTPNC_INFANT_HIV_TEST_CONF_RESULT AS 'infant_hiv_test_conf_result' ,
                INFANTPNC_INFANT_ART_LINKED AS 'infant_art_linked' ,  -- Was this infant linked to ART --
                INFANTPNC_GRPINFANTART_INFANT_ART_NUMBER AS  'art_number' ,
                INFANTPNC_GRPINFANTART_INFANT_ART_NUMBER_MISSING AS 'art_number_missing' ,
                DATE_FORMAT(INFANTPNC_GRPINFANTDEATHDATE_INFANT_DATE_DEATH, "%Y-%m-%d %h:%i:%s")   AS 'infant_date_death',  
                INFANTPNC_GRPINFANTDEATHDATE_INFANT_DATE_DEATH_MISSING AS 'infant_date_death_mising', 
                MRS_OPENMRS_USER_UUID AS  'provider_uuid', 
                PTRACKER_USERNAME AS 'username',
                _SUBMISSION_DATE AS 'submission_date',        
                PTK_2020_02_CORE._URI AS _URI,
                INFANTPNC_ID_PARENT AS 'parent_ptracker_id',
                'PTK_2020_02' as version
            FROM PTK_2020_02_CORE
            JOIN PTK_2020_02_CORE2
            ON PTK_2020_02_CORE2._PARENT_AURI = PTK_2020_02_CORE._URI
            WHERE visit_type = '4'  
        UNION ALL
            SELECT
                PTRACKER_ID AS 'ptracker_id' ,
                FACILITY AS 'facility_code' , 
                FACILITY_NAME AS 'facility_name' ,
                'Infant PNC' AS 'type' ,
                DATE_FORMAT(visit_date, "%Y-%m-%d %h:%i:%s") AS 'visit_date' ,
                DATE_FORMAT(visit_date, "%Y-%m-%d %h:%i:%s") AS 'visit_date_recorded_odk' ,
                NULL AS 'openmrs_id' , -- opernmrs id not supplied through csv-- 
                null AS 'openmrs_person_uuid' ,
                null AS 'openmrs_patient_uuid', 
                null AS 'openmrs_parent_uuid' ,
                null AS 'facility_uuid',
                PTK_2020_03_CORE._URI AS 'odk_uuid' ,
                CLIENT_INFO_NAME_GIVEN AS 'given' ,
                CLIENT_INFO_NAME_MIDDLE AS 'middle' ,
                CLIENT_INFO_NAME_FAMILY AS 'family' ,
                INFANTNAME_NEW_GIVEN AS 'given_new' ,
                INFANTNAME_NEW_FAMILY AS 'family_new' ,
                INFANTNAME_NEW_MIDDLE AS 'middle_new' ,
                CLIENT_INFO_NAME_SEX AS  'sex' ,
                DATE_FORMAT(CLIENT_INFO_INFANTDOB_DOB_INFANT, "%Y-%m-%d %h:%i:%s")   AS 'dob_infant',  
                CLIENT_INFO_CLIENT_CONTACT_COUNTRY AS  'country' ,
                CLIENT_INFO_CLIENT_CONTACT_COUNTRY_OTHER AS  'country_other' ,
                CLIENT_INFO_CLIENT_CONTACT_CLIENT_CONTACT_DISTRICT AS  'district' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPADDRESS_ADDRESS AS  'address' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPLOCATION_LOCATION AS  'location' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPPHONE_PHONE_NUMBER AS  'phone_number' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPKINNAME_KIN_NAME AS  'kin_name' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPKINCONTACT_KIN_CONTACT AS  'kin_contact' ,
                -- INFANT PNC PART -- 
                INFANTPNC_INFANT_TRANSFER_STATUS AS 'infant_transfer_status',
                INFANTPNC_PNC_BREASTFEEDING_STATUS AS 'infant_breastfeeding_status',
                INFANTPNC_INFANT_TRANSFER_STATUS_TRANSFEREDTO AS 'infant_transfer_out',
                INFANTPNC_INFANT_TRANSFER_STATUS_TRANSFEREDTO_OTHER AS 'infant_transfer_out_other',
                DATE_FORMAT(INFANTPNC_GRPINFANTTRNSFOUTDATE_INFANT_TRANSFEROUT_DATE, "%Y-%m-%d %h:%i:%s") AS 'infant_transfer_out_date',
                INFANTPNC_GRPINFANTTRNSFOUTDATE_INFANT_TRANSFEROUT_DATE_MISSING AS 'infant_transfer_out_date_missing',
                INFANTPNC_INFANT_TRANSFERIN_FROM AS 'infant_transferin',
                INFANTPNC_INFANT_TRANSFERIN_FROM_OTHER AS  'infant_transferin_other',
                DATE_FORMAT(INFANTPNC_GRPINFANTTRNSFDATE_INFANT_TRANSFERIN_DATE, "%Y-%m-%d %h:%i:%s")   AS 'infant_transferin_date',
                INFANTPNC_GRPINFANTTRNSFDATE_INFANT_TRANSFERIN_DATE_MISSING AS 'infant_transferin_date_missing',
                DATE_FORMAT(INFANTPNC_GRPINFANTNXTVISITDATE_INFANT_NEXT_VISIT_DATE, "%Y-%m-%d %h:%i:%s")   AS 'infant_next_visit_date', 
                INFANTPNC_GRPINFANTNXTVISITDATE_INFANT_NEXT_VISIT_DATE_MISSING AS 'infant_next_visit_date_missing', 
                DATE_FORMAT(INFANTPNC_GRPINFANTEVENTDATE_INFANT_EVENT_DATE, "%Y-%m-%d %h:%i:%s")  AS 'infant_event_date',
                INFANTPNC_GRPINFANTEVENTDATE_INFANT_EVENT_DATE_MISSING AS 'infant_event_date_missing',
                DATE_FORMAT(INFANTPNC_16TCLINICDATE_INFANT_TRANSFERTO_ARTCLINIC_DATE, "%Y-%m-%d %h:%i:%s")  AS 'infant_transferto_artclinic_date',
                INFANTPNC_16TCLINICDATE_INFANT_TRANSFERTO_ARTCLINIC_DATE_MISSING AS 'infant_transferto_artclinic_date_missing',
                INFANTPNC_INFANT_TRANSFERTO_ARTCLINIC AS 'infant_transferto_artclinic',
                INFANTPNC_INFANT_TRANSFERTO_ARTCLINIC_OTHER AS 'infant_transferto_artclinic_missing',
                INFANTPNC_HIV_EXPOSURE_STATUS AS 'hiv_exposure_status' ,
                INFANTPNC_ARV_PROPHYLAXIS_STATUS AS 'arv_prophylaxis_status',
                INFANTPNC_ARV_PROPHYLAXIS_ADHERENCE AS 'arv_prophylaxis_adherence' ,
                INFANTPNC_CTX_PROPHYLAXIS_STATUS AS 'ctx_prophylaxis_status' ,
                INFANTPNC_CTX_PROPHYLAXIS_ADHERENCE AS 'ctx_prophylaxis_adherence' ,
                INFANTPNC_INFANT_HIV_TESTED AS 'infant_hiv_tested' ,
                INFANTPNC_INFANT_HIV_TEST_USED AS 'infant_hiv_test_used' ,
                INFANTPNC_INFANT_HIV_TEST_RESULT AS 'infant_hiv_test_result' ,
                INFANTPNC_INFANT_HIV_TEST_RESULT_PCR AS 'infant_hiv_test_result_pcr' ,
                INFANTPNC_INFANT_HIV_TEST_CONF AS 'infant_hiv_test_conf' ,
                INFANTPNC_INFANT_HIV_TEST_CONF_RESULT AS 'infant_hiv_test_conf_result' ,
                INFANTPNC_INFANT_ART_LINKED AS 'infant_art_linked' ,  -- Was this infant linked to ART --
                INFANTPNC_GRPINFANTART_INFANT_ART_NUMBER AS  'art_number' ,
                INFANTPNC_GRPINFANTART_INFANT_ART_NUMBER_MISSING AS 'art_number_missing' ,
                DATE_FORMAT(INFANTPNC_GRPINFANTDEATHDATE_INFANT_DATE_DEATH, "%Y-%m-%d %h:%i:%s")   AS 'infant_date_death',  
                INFANTPNC_GRPINFANTDEATHDATE_INFANT_DATE_DEATH_MISSING AS 'infant_date_death_mising', 
                MRS_OPENMRS_USER_UUID AS  'provider_uuid', 
                PTRACKER_USERNAME AS 'username',
                _SUBMISSION_DATE AS 'submission_date',
                PTK_2020_03_CORE._URI AS '_URI',
                INFANTPNC_ID_PARENT AS 'parent_ptracker_id',
                'PTK_2020_03' as version
            FROM PTK_2020_03_CORE
            JOIN PTK_2020_03_CORE2
            ON PTK_2020_03_CORE2._PARENT_AURI = PTK_2020_03_CORE._URI
            JOIN PTK_2020_03_CORE3
            ON PTK_2020_03_CORE3._PARENT_AURI = PTK_2020_03_CORE._URI
            WHERE visit_type = '4'
        UNION ALL
            SELECT
                PTRACKER_ID AS 'ptracker_id' ,
                FACILITY AS 'facility_code' , 
                FACILITY_NAME AS 'facility_name' ,
                'Infant PNC' AS 'type' ,
                DATE_FORMAT(visit_date, "%Y-%m-%d %h:%i:%s") AS 'visit_date' ,
                DATE_FORMAT(visit_date, "%Y-%m-%d %h:%i:%s") AS 'visit_date_recorded_odk' ,
                NULL AS 'openmrs_id' , -- opernmrs id not supplied through csv-- 
                MRS_PERSON_UUID AS 'openmrs_person_uuid' ,
                MRS_PATIENT_UUID AS 'openmrs_patient_uuid', 
                INFANTPNC_MRS_PARENT_UUID AS 'openmrs_parent_uuid' ,
                facility_uuid AS 'facility_uuid', 
                PTK_2020_10_CORE._URI AS 'odk_uuid' ,
                CLIENT_INFO_NAME_GIVEN AS 'given' ,
                CLIENT_INFO_NAME_MIDDLE AS 'middle' ,
                CLIENT_INFO_NAME_FAMILY AS 'family' ,
                INFANTNAME_NEW_GIVEN AS 'given_new' ,
                INFANTNAME_NEW_FAMILY AS 'family_new' ,
                INFANTNAME_NEW_MIDDLE AS 'middle_new' ,
                CLIENT_INFO_NAME_SEX AS  'sex' ,
                DATE_FORMAT(CLIENT_INFO_INFANTDOB_DOB_INFANT, "%Y-%m-%d %h:%i:%s")   AS 'dob_infant',  
                CLIENT_INFO_CLIENT_CONTACT_COUNTRY AS  'country' ,
                CLIENT_INFO_CLIENT_CONTACT_COUNTRY_OTHER AS  'country_other' ,
                CLIENT_INFO_CLIENT_CONTACT_CLIENT_CONTACT_DISTRICT AS  'district' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPADDRESS_ADDRESS AS  'address' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPLOCATION_LOCATION AS  'location' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPPHONE_PHONE_NUMBER AS  'phone_number' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPKINNAME_KIN_NAME AS  'kin_name' ,
                CLIENT_INFO_CLIENT_CONTACT_GRPKINCONTACT_KIN_CONTACT AS  'kin_contact' ,
                -- INFANT PNC PART -- 
                INFANTPNC_INFANT_TRANSFER_STATUS AS 'infant_transfer_status',
                INFANTPNC_PNC_BREASTFEEDING_STATUS AS 'infant_breastfeeding_status',
                INFANTPNC_INFANT_TRANSFER_STATUS_TRANSFEREDTO AS 'infant_transfer_out',
                INFANTPNC_INFANT_TRANSFER_STATUS_TRANSFEREDTO_OTHER AS 'infant_transfer_out_other',
                DATE_FORMAT(INFANTPNC_GRPINFANTTRNSFOUTDATE_INFANT_TRANSFEROUT_DATE, "%Y-%m-%d %h:%i:%s") AS 'infant_transfer_out_date',
                INFANTPNC_GRPINFANTTRNSFOUTDATE_INFANT_TRANSFEROUT_DATE_MISSING AS 'infant_transfer_out_date_missing',
                INFANTPNC_INFANT_TRANSFERIN_FROM AS 'infant_transferin',
                INFANTPNC_INFANT_TRANSFERIN_FROM_OTHER AS  'infant_transferin_other',
                DATE_FORMAT(INFANTPNC_GRPINFANTTRNSFDATE_INFANT_TRANSFERIN_DATE, "%Y-%m-%d %h:%i:%s")   AS 'infant_transferin_date',
                INFANTPNC_GRPINFANTTRNSFDATE_INFANT_TRANSFERIN_DATE_MISSING AS 'infant_transferin_date_missing',
                DATE_FORMAT(INFANTPNC_GRPINFANTNXTVISITDATE_INFANT_NEXT_VISIT_DATE, "%Y-%m-%d %h:%i:%s")   AS 'infant_next_visit_date', 
                INFANTPNC_GRPINFANTNXTVISITDATE_INFANT_NEXT_VISIT_DATE_MISSING AS 'infant_next_visit_date_missing', 
                DATE_FORMAT(INFANTPNC_GRPINFANTEVENTDATE_INFANT_EVENT_DATE, "%Y-%m-%d %h:%i:%s")  AS 'infant_event_date',
                INFANTPNC_GRPINFANTEVENTDATE_INFANT_EVENT_DATE_MISSING AS 'infant_event_date_missing',
                DATE_FORMAT(INFANTPNC_16TCLINICDATE_INFANT_TRANSFERTO_ARTCLINIC_DATE, "%Y-%m-%d %h:%i:%s")  AS 'infant_transferto_artclinic_date',
                INFANTPNC_16TCLINICDATE_INFANT_TRANSFERTO_ARTCLINIC_DATE_MISSING AS 'infant_transferto_artclinic_date_missing',
                INFANTPNC_INFANT_TRANSFERTO_ARTCLINIC AS 'infant_transferto_artclinic',
                INFANTPNC_INFANT_TRANSFERTO_ARTCLINIC_OTHER AS 'infant_transferto_artclinic_missing',
                INFANTPNC_HIV_EXPOSURE_STATUS AS 'hiv_exposure_status' ,
                INFANTPNC_ARV_PROPHYLAXIS_STATUS AS 'arv_prophylaxis_status',
                INFANTPNC_ARV_PROPHYLAXIS_ADHERENCE AS 'arv_prophylaxis_adherence' ,
                INFANTPNC_CTX_PROPHYLAXIS_STATUS AS 'ctx_prophylaxis_status' ,
                INFANTPNC_CTX_PROPHYLAXIS_ADHERENCE AS 'ctx_prophylaxis_adherence' ,
                INFANTPNC_INFANT_HIV_TESTED AS 'infant_hiv_tested' ,
                INFANTPNC_INFANT_HIV_TEST_USED AS 'infant_hiv_test_used' ,
                INFANTPNC_INFANT_HIV_TEST_RESULT AS 'infant_hiv_test_result' ,
                INFANTPNC_INFANT_HIV_TEST_RESULT_PCR AS 'infant_hiv_test_result_pcr' ,
                INFANTPNC_INFANT_HIV_TEST_CONF AS 'infant_hiv_test_conf' ,
                INFANTPNC_INFANT_HIV_TEST_CONF_RESULT AS 'infant_hiv_test_conf_result' ,
                INFANTPNC_INFANT_ART_LINKED AS 'infant_art_linked' ,  -- Was this infant linked to ART --
                INFANTPNC_GRPINFANTART_INFANT_ART_NUMBER AS  'art_number' ,
                INFANTPNC_GRPINFANTART_INFANT_ART_NUMBER_MISSING AS 'art_number_missing' ,
                DATE_FORMAT(INFANTPNC_GRPINFANTDEATHDATE_INFANT_DATE_DEATH, "%Y-%m-%d %h:%i:%s")   AS 'infant_date_death',  
                INFANTPNC_GRPINFANTDEATHDATE_INFANT_DATE_DEATH_MISSING AS 'infant_date_death_mising', 
                MRS_OPENMRS_USER_UUID AS  'provider_uuid', 
                PTRACKER_USERNAME AS 'username',
                _SUBMISSION_DATE AS 'submission_date',
                PTK_2020_10_CORE._URI AS '_URI',
                INFANTPNC_ID_PARENT AS 'parent_ptracker_id',
                'PTK_2020_10' as version
            FROM PTK_2020_10_CORE
            JOIN PTK_2020_10_CORE2
            ON PTK_2020_10_CORE2._PARENT_AURI = PTK_2020_10_CORE._URI
            JOIN PTK_2020_10_CORE3
            ON PTK_2020_10_CORE3._PARENT_AURI = PTK_2020_10_CORE._URI
            WHERE visit_type = '4';                        