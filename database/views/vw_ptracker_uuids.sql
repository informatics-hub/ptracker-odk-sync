-- Creates a list of all uuids based on all forms and ODK record uuids--
DROP VIEW IF EXISTS vw_ptracker_uuids;  
CREATE VIEW vw_ptracker_uuids AS
	SELECT 
		_URI, ptracker_id, visit_date, visit_type, ptracker_username
	FROM PTK_201803_CORE
UNION ALL
	SELECT 
		_URI, ptracker_id, visit_date, visit_type, ptracker_username
	FROM PTK_2018032_CORE
UNION ALL
	SELECT 
		_URI, ptracker_id, visit_date, visit_type, ptracker_username
	FROM PTK_201810_CORE
UNION ALL
	SELECT 
		_URI, ptracker_id, visit_date, visit_type, ptracker_username
	FROM PTK_201902_CORE		
UNION ALL 
	SELECT 
		_URI, ptracker_id, visit_date, visit_type, ptracker_username
	FROM PTK_2020_02_CORE		
UNION ALL
	SELECT 
		_URI, ptracker_id, visit_date, visit_type, ptracker_username
	FROM PTK_2020_03_CORE
UNION ALL
	SELECT 
		_URI, ptracker_id, visit_date, visit_type, ptracker_username
	FROM PTK_2020_10_CORE;		
