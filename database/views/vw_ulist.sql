DROP VIEW IF EXISTS vw_ulist; 
CREATE VIEW vw_ulist AS
    SELECT
        p.person_id AS personid,
        pn.given_name,
        pn.family_name,
        p.gender,
        u.username,
        '1234' AS odk_pin,
        pn.uuid AS person_uuid
    FROM person p
        INNER JOIN person_name pn ON p.person_id = pn.person_id
        INNER JOIN users u ON p.person_id = u.person_id AND u.retired = 0
    WHERE u.username IS NOT NULL
        AND u.username != 'daemon'
        AND u.username != 'odksyncuser'
    GROUP BY p.person_id;