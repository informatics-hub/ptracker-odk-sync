DROP TABLE IF EXISTS `stag_ptracker_worklist`;
CREATE TABLE `stag_ptracker_worklist` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `_URI` text NOT NULL,
  `ptracker_id` text NOT NULL,
  `visit_date` text NOT NULL, 
  `visit_type` text NOT NULL,
  `ptracker_username` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
