#!/bin/bash
# PTracker ODK Sync Scheduled Queries
#

date
echo "Executing scheduled query..."
mysql -D"ptracker_odk" -u"root" -p"root@ucsf" < "database/queries/query_scheduled.sql"

date
echo "Executing scheduled query for OpenMRS..."
mysql -D"openmrs_test" -u"root" -p"root@ucsf" < "database/queries/query_scheduled_openmrs.sql"

date
echo "Done"