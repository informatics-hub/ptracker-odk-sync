#!/bin/bash
# PTracker ODK Sync Push 
clear
echo "PTracker ODK Sync - Push"

date
echo "Submitting ANC encounters to OpenMRS"
php web/push/anc.php -t 300
sleep 10s

date
echo "Submitting PNC encounters to OpenMRS"
php web/push/pnc.php -t 300
sleep 10s

date
echo "Submitting Infant PNC encounters to OpenMRS"
php web/push/infant_pnc.php -t 300
sleep 10s

date
echo "Submitting Maternity encounters to OpenMRS"
php web/push/labour.php -t 300
sleep 10s

echo "Done"
