<?php
session_start();

if(!isset($_SESSION["username"])) {
    $errorMsg = "You must login first!";
    header("Location: login.php?e=".$errorMsg."");  
}

error_reporting(E_ALL & ~E_NOTICE);

// specify date & timezone 
date_default_timezone_set('Africa/Windhoek');
$date 		 		= date('Y/m/d H:i:s');
$date_today 		= date('Y-m-d');
$date_1weekago		= date('Y-m-d', strtotime('-1 week'));
$date_1daysago		= date('Y-m-d', strtotime('-1 days'));
$date_2daysago		= date('Y-m-d', strtotime('-2 days'));
$date_3daysago		= date('Y-m-d', strtotime('-3 days'));
$date_4daysago		= date('Y-m-d', strtotime('-4 days'));
$date_5daysago		= date('Y-m-d', strtotime('-5 days'));
$date_6daysago		= date('Y-m-d', strtotime('-6 days'));

// connect to database & get openmrs logins
require_once('../push/database/config.php'); 

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta NAME="robots" CONTENT="none">
    <!-- UCSF Global Health Informatics Hub 2018/01 -->

    <title>Dashboard</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="submit" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../portal">PTracker SYNC</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <?php echo $_SESSION["username"]; ?> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="../"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                          <form method="post" action="search.php">
                            <div class="input-group custom-search-form">
                            	<input type="text" class="form-control" name="id" placeholder="Enter PTracker ID...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
						</form><!-- /input-group -->
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="worklist.php"><i class="fa fa-table fa-fw"></i> Worklist</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-table fa-fw"></i> Sync Log<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="synclog_all.php">All Entries</a>
                                </li>
                                <li>
                                    <a href="synclog_anc.php">ANC</a>
                                </li>
                                <li>
                                    <a href="synclog_delivery.php">Labour & Delivery</a>
                                </li>
                                <li>
                                    <a href="synclog_pnc.php">PNC</a>
                                </li>
                                <li>
                                    <a href="synclog_ipnc.php"> Infant PNC</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                       <li>
                            <a href="#"><i class="fa fa-table fa-fw"></i> ODK Records<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="odk_all.php">All Entries</a>
                                </li>
                                <li>
                                    <a href="odk_anc.php">ANC</a>
                                </li>
                                <li>
                                    <a href="odk_delivery.php">Labour & Delivery</a>
                                </li>
                                <li>
                                    <a href="odk_pnc.php">PNC</a>
                                </li>
                                <li>
                                    <a href="odk_ipnc.php"> Infant PNC</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Dashboard</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                
                       <?php if(isset($_GET['e'])) { ?>
            <div class="alert alert-info alert-dismissable">
              <button type="submit" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
               <?php echo $_GET['e']; ?>
            </div><?php } ?>
            </div>
            <div class="well">
                <h4>Welcome to the PTracker Sync Dashboard</h4>
                <p>This portal shows important and raw information based on the sync process between ODK Aggregate and OpenMRS.<br>
	                Contact the systems administrator for more information.
                </p>
     
            </div>
            
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Number of records synced daily in the last 7 days
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="morris-bar-chart"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
			</div>
			
            <div class="row">
               <div class="panel-heading">
                  Records synced from ODK to OpenMRS by type
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <!--div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div-->
                                <div class="col-xs-9 text-center">
                                    <div class="huge"><?php
                
                   $sql = "SELECT * FROM stag_ptracker_synclog WHERE type ='ANC'";
				   $result = $conn->query($sql);

                   if ($result->num_rows > 0) {
                       $rows_count=mysqli_num_rows($result);
                   } else { $rows_count = '0'; }
                   
                   echo number_format($rows_count);
                  ?></div>
                                    <div>ANC </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <!--div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div-->
                                <div class="col-xs-9 text-center">
                                    <div class="huge"><?php
                
                   $sql = "SELECT * FROM stag_ptracker_synclog WHERE type ='L&D' OR type = 'Infant L&D'";
				   $result = $conn->query($sql);

                   if ($result->num_rows > 0) {
                       $rows_count=mysqli_num_rows($result);
                   } else { $rows_count = '0'; }
                   
                   echo number_format($rows_count);
                  ?></div>
                                    <div>Labour & Delivery</div>
                                </div>
                            </div>
                        </div>

                   
                </div>
            </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <!--div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div-->
                                <div class="col-xs-9 text-center">
                                    <div class="huge"><?php
                
                   $sql = "SELECT * FROM stag_ptracker_synclog WHERE type ='PNC'";
				   $result = $conn->query($sql);

                   if ($result->num_rows > 0) {
                       $rows_count=mysqli_num_rows($result);
                   } else { $rows_count = '0'; }
                   
                   echo number_format($rows_count);
                  ?></div>
                                    <div>PNC </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <!--div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div-->
                                <div class="col-xs-9 text-center">
                                    <div class="huge"><?php
                
                   $sql = "SELECT * FROM stag_ptracker_synclog WHERE type ='Infant PNC'";
				   $result = $conn->query($sql);

                   if ($result->num_rows > 0) {
                       $rows_count=mysqli_num_rows($result);
                   } else { $rows_count = '0'; }
                   
                   echo number_format($rows_count);
                  ?></div>
                                    <div>Infant PNC </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
				<div class="row">


                <div class="col-lg-6 col-md-12">
                <?php 
                
                
				   $sql_error_count = "SELECT count(*) AS count FROM stag_ptracker_synclog WHERE error_message<>''";				   
                   $result_error_count = $conn->query($sql_error_count);
				   $row_error_count = mysqli_fetch_assoc($result_error_count);
				   
				   if ($row_error_count['count'] > 0) { $colorpanel = "panel-warning"; }
				   if ($row_error_count['count'] == 0) { $colorpanel = "panel-success"; }
				   if ($row_error_count['count'] > 10) { $colorpanel = "panel-alert"; }

				   ?>
                    <div class="panel <?php echo $colorpanel; ?>">
                        <div class="panel-heading">
                            <div class="row" style="text-align:center;">
                                <!--div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div-->
                                <div class="col-xs-9">
                                    <div class="huge">There are <a href="synclog_errors.php" target="_self"><?php

                   echo number_format($row_error_count['count']);
                  ?></a> syncs with errors.</div>
                                </div>
                          </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                <?php 
                
                
				   $sql_worklist_count = "SELECT count(*) AS count FROM stag_ptracker_worklist";				   
                   $result_worklist_count = $conn->query($sql_worklist_count);
				   $row_worklist_count = mysqli_fetch_assoc($result_worklist_count);
				   
				   if ($row_worklist_count['count'] < 10) { $colorpanel = "panel-warning"; }
				   if ($row_worklist_count['count'] > 10) { $colorpanel = "panel-alert"; }
				   if ($row_worklist_count['count'] == 0) { $colorpanel = "panel-success"; }

				   ?>
                    <div class="panel <?php echo $colorpanel; ?>">
                        <div class="panel-heading">
                            <div class="row" style="text-align:center;">
                                <!--div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div-->
                                <div class="col-xs-9">
                                    <div class="huge">There are <a href="worklist.php" target="_self"><?php

                   echo number_format($row_worklist_count['count']);
                  ?></a> records in worklist queue.</div>
                                </div>
                         </div>
                        </div>

                    </div>
                </div>
                
			</div>
			
			<div class="row">
			  <div class="panel-heading">
                  ODK and OpenMRS record totals
                </div>
				<div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           ODK Records by type
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="morris-donut-chart"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                
				<div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           OpenMRS Encounters by type
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="morris-donut-chart-openmrs"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
			</div>	
			
            <div class="row">

                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <!--div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div-->
                                <div class="col-xs-9 text-center">
                                    <div class="huge"><?php
                
                    $sql = "SELECT count(_URI) as counted FROM stag_ptracker_uuids";
				    $result = $conn->query($sql);
                    $rows_count = mysqli_fetch_assoc($result);

                   if ($rows_count["counted"] != null) {
                    echo number_format($rows_count["counted"]);
                } else { echo '0'; }
                  ?></div>
                                    <div>Records on ODK </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <!--div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div-->
                                <div class="col-xs-9 text-center">
                                    <div class="huge"><?php
                
                   $sql = "SELECT count(*) AS encounters FROM encounter WHERE voided = '0'";
				   $result = $conn3->query($sql);
				   $row = mysqli_fetch_assoc($result);
				   
                   if ($row["encounters"] != null) {
                       echo number_format($row["encounters"]);
                   } else { echo '0'; }
       
                  ?></div>
                                    <div>Encounters on OpenMRS</div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


               <div class="col-lg-6 col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <!--div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div-->
                                <div class="col-xs-9 text-center">
                                    <div class="huge"><?php
                
                   $sql = "SELECT count(*) AS encounters FROM stag_ptracker_synclog";
				   $result = $conn->query($sql);
				   $row = mysqli_fetch_assoc($result);
				   
                   if ($row["encounters"] != null) {
                       echo number_format($row["encounters"]);
                   } else { echo '0'; }
       
                  ?></div>
                                    <div>ODK records synced to OpenMRS</div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                
               <div class="col-lg-6 col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <!--div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div-->
                                <div class="col-xs-9 text-center">
                                    <div class="huge"><?php
                
                   $sql = "SELECT count(*) AS patients FROM patient WHERE voided = '0'";
				   $result = $conn3->query($sql);
				   $row = mysqli_fetch_assoc($result);
				   
                   if ($row["patients"] != null) {
                       echo number_format($row["patients"]);
                   } else { echo '0'; }
       
                  ?></div>
                                    <div>Patients on OpenMRS</div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                
            </div>
		
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>
    
    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <script src="vendor/raphael/raphael.min.js"></script>
    <script src="vendor/morrisjs/morris.min.js"></script>
    <script type="text/javascript">
	    $(function() {
	
	    Morris.Bar({
        element: 'morris-bar-chart',
			 data: [{
			    period: '<?php echo $date_1weekago; ?>',
	            anc: <?php
                
                   $sql_daily = "
                   SELECT 
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_1weekago."' AND TYPE = 'ANC') AS anc,
					(SELECT  count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_1weekago."' AND (TYPE = 'Infant L&D' OR TYPE = 'L&D')) AS delivery,
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_1weekago."' AND TYPE = 'PNC') AS pnc,
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_1weekago."' AND TYPE = 'Infant PNC') AS infant_pnc";
				   $result_daily = $conn->query($sql_daily);
				   $row_daily = mysqli_fetch_assoc($result_daily);
				   
                   if ($row_daily["anc"] != null) {
                       echo $row_daily["anc"];
                   } else { echo 'null'; }
       
                  ?>,
	            delivery: <?php
                   if ($row_daily["delivery"] != null) {
                       echo $row_daily["delivery"];
                   } else { echo 'null'; }
       
                  ?>,
	            pnc: <?php
                    if ($row_daily["pnc"] != null) {
                       echo $row_daily["pnc"];
                   } else { echo 'null'; }
       
                 ?>,
				infantpnc: <?php
                   if ($row_daily["infant_pnc"] != null) {
                       echo $row_daily["infant_pnc"];
                   } else { echo 'null'; }
       
                  ?>
	        }, {
	            period: '<?php echo $date_6daysago; ?>',
	            anc: <?php
                
                   $sql_daily = "
                   SELECT 
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_6daysago."' AND TYPE = 'ANC') AS anc,
					(SELECT  count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_6daysago."' AND (TYPE = 'Infant L&D' OR TYPE = 'L&D')) AS delivery,
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_6daysago."' AND TYPE = 'PNC') AS pnc,
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_6daysago."' AND TYPE = 'Infant PNC') AS infant_pnc";
				   $result_daily = $conn->query($sql_daily);
				   $row_daily = mysqli_fetch_assoc($result_daily);
				   
                   if ($row_daily["anc"] != null) {
                       echo $row_daily["anc"];
                   } else { echo 'null'; }
       
                  ?>,
	            delivery: <?php
                   if ($row_daily["delivery"] != null) {
                       echo $row_daily["delivery"];
                   } else { echo 'null'; }
       
                  ?>,
	            pnc: <?php
                    if ($row_daily["pnc"] != null) {
                       echo $row_daily["pnc"];
                   } else { echo 'null'; }
       
                 ?>,
				infantpnc: <?php
                   if ($row_daily["infant_pnc"] != null) {
                       echo $row_daily["infant_pnc"];
                   } else { echo 'null'; }
       
                  ?>
	        }, {
	            period: '<?php echo $date_5daysago; ?>',
	            anc: <?php
                
                   $sql_daily = "
                   SELECT 
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_5daysago."' AND TYPE = 'ANC') AS anc,
					(SELECT  count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_5daysago."' AND (TYPE = 'Infant L&D' OR TYPE = 'L&D')) AS delivery,
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_5daysago."' AND TYPE = 'PNC') AS pnc,
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_5daysago."' AND TYPE = 'Infant PNC') AS infant_pnc";
				   $result_daily = $conn->query($sql_daily);
				   $row_daily = mysqli_fetch_assoc($result_daily);
				   
                   if ($row_daily["anc"] != null) {
                       echo $row_daily["anc"];
                   } else { echo 'null'; }
       
                  ?>,
	            delivery: <?php
                   if ($row_daily["delivery"] != null) {
                       echo $row_daily["delivery"];
                   } else { echo 'null'; }
       
                  ?>,
	            pnc: <?php
                    if ($row_daily["pnc"] != null) {
                       echo $row_daily["pnc"];
                   } else { echo 'null'; }
       
                 ?>,
				infantpnc: <?php
                   if ($row_daily["infant_pnc"] != null) {
                       echo $row_daily["infant_pnc"];
                   } else { echo 'null'; }
       
                  ?>
	        }, {
	            period: '<?php echo $date_4daysago; ?>',
	            anc: <?php
                
                   $sql_daily = "
                   SELECT 
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_4daysago."' AND TYPE = 'ANC') AS anc,
					(SELECT  count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_4daysago."' AND (TYPE = 'Infant L&D' OR TYPE = 'L&D')) AS delivery,
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_4daysago."' AND TYPE = 'PNC') AS pnc,
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_4daysago."' AND TYPE = 'Infant PNC') AS infant_pnc";
				   $result_daily = $conn->query($sql_daily);
				   $row_daily = mysqli_fetch_assoc($result_daily);
				   
                   if ($row_daily["anc"] != null) {
                       echo $row_daily["anc"];
                   } else { echo 'null'; }
       
                  ?>,
	            delivery: <?php
                   if ($row_daily["delivery"] != null) {
                       echo $row_daily["delivery"];
                   } else { echo 'null'; }
       
                  ?>,
	            pnc: <?php
                    if ($row_daily["pnc"] != null) {
                       echo $row_daily["pnc"];
                   } else { echo 'null'; }
       
                 ?>,
				infantpnc: <?php
                   if ($row_daily["infant_pnc"] != null) {
                       echo $row_daily["infant_pnc"];
                   } else { echo 'null'; }
       
                  ?>
	        }, {
	            period: '<?php echo $date_3daysago; ?>',
	            anc: <?php
                
                   $sql_daily = "
                   SELECT 
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_3daysago."' AND TYPE = 'ANC') AS anc,
					(SELECT  count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_3daysago."' AND (TYPE = 'Infant L&D' OR TYPE = 'L&D')) AS delivery,
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_3daysago."' AND TYPE = 'PNC') AS pnc,
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_3daysago."' AND TYPE = 'Infant PNC') AS infant_pnc";
				   $result_daily = $conn->query($sql_daily);
				   $row_daily = mysqli_fetch_assoc($result_daily);
				   
                   if ($row_daily["anc"] != null) {
                       echo $row_daily["anc"];
                   } else { echo 'null'; }
       
                  ?>,
	            delivery: <?php
                   if ($row_daily["delivery"] != null) {
                       echo $row_daily["delivery"];
                   } else { echo 'null'; }
       
                  ?>,
	            pnc: <?php
                    if ($row_daily["pnc"] != null) {
                       echo $row_daily["pnc"];
                   } else { echo 'null'; }
       
                 ?>,
				infantpnc: <?php
                   if ($row_daily["infant_pnc"] != null) {
                       echo $row_daily["infant_pnc"];
                   } else { echo 'null'; }
       
                  ?>
	        }, {
	            period: '<?php echo $date_2daysago; ?>',
	            anc: <?php
                
                   $sql_daily = "
                   SELECT 
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_2daysago."' AND TYPE = 'ANC') AS anc,
					(SELECT  count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_2daysago."' AND (TYPE = 'Infant L&D' OR TYPE = 'L&D')) AS delivery,
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_2daysago."' AND TYPE = 'PNC') AS pnc,
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_2daysago."' AND TYPE = 'Infant PNC') AS infant_pnc";
				   $result_daily = $conn->query($sql_daily);
				   $row_daily = mysqli_fetch_assoc($result_daily);
				   
                   if ($row_daily["anc"] != null) {
                       echo $row_daily["anc"];
                   } else { echo 'null'; }
       
                  ?>,
	            delivery: <?php
                   if ($row_daily["delivery"] != null) {
                       echo $row_daily["delivery"];
                   } else { echo 'null'; }
       
                  ?>,
	            pnc: <?php
                    if ($row_daily["pnc"] != null) {
                       echo $row_daily["pnc"];
                   } else { echo 'null'; }
       
                 ?>,
				infantpnc: <?php
                   if ($row_daily["infant_pnc"] != null) {
                       echo $row_daily["infant_pnc"];
                   } else { echo 'null'; }
       
                  ?>
	        }, {
	            period: '<?php echo $date_1daysago; ?>',
	            anc: <?php
                
                   $sql_daily = "
                   SELECT 
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_1daysago."' AND TYPE = 'ANC') AS anc,
					(SELECT  count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_1daysago."' AND (TYPE = 'Infant L&D' OR TYPE = 'L&D')) AS delivery,
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_1daysago."' AND TYPE = 'PNC') AS pnc,
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_1daysago."' AND TYPE = 'Infant PNC') AS infant_pnc";
				   $result_daily = $conn->query($sql_daily);
				   $row_daily = mysqli_fetch_assoc($result_daily);
				   
                   if ($row_daily["anc"] != null) {
                       echo $row_daily["anc"];
                   } else { echo 'null'; }
       
                  ?>,
	            delivery: <?php
                   if ($row_daily["delivery"] != null) {
                       echo $row_daily["delivery"];
                   } else { echo 'null'; }
       
                  ?>,
	            pnc: <?php
                    if ($row_daily["pnc"] != null) {
                       echo $row_daily["pnc"];
                   } else { echo 'null'; }
       
                 ?>,
				infantpnc: <?php
                   if ($row_daily["infant_pnc"] != null) {
                       echo $row_daily["infant_pnc"];
                   } else { echo 'null'; }
       
                  ?>
	        }, {
	            period: '<?php echo $date_today; ?>',
	            anc: <?php
                
                   $sql_daily = "
                   SELECT 
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_today."' AND TYPE = 'ANC') AS anc,
					(SELECT  count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_today."' AND (TYPE = 'Infant L&D' OR TYPE = 'L&D')) AS delivery,
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_today."' AND TYPE = 'PNC') AS pnc,
					(SELECT count(*) AS count FROM stag_ptracker_synclog WHERE DATE(`date_created`)  = '".$date_today."' AND TYPE = 'Infant PNC') AS infant_pnc";
				   $result_daily = $conn->query($sql_daily);
				   $row_daily = mysqli_fetch_assoc($result_daily);
				   
                   if ($row_daily["anc"] != null) {
                       echo $row_daily["anc"];
                   } else { echo 'null'; }
       
                  ?>,
	            delivery: <?php
                   if ($row_daily["delivery"] != null) {
                       echo $row_daily["delivery"];
                   } else { echo 'null'; }
       
                  ?>,
	            pnc: <?php
                    if ($row_daily["pnc"] != null) {
                       echo $row_daily["pnc"];
                   } else { echo 'null'; }
       
                 ?>,
				infantpnc: <?php
                   if ($row_daily["infant_pnc"] != null) {
                       echo $row_daily["infant_pnc"];
                   } else { echo 'null'; }
       
                  ?>
	        }],
		    xkey: 'period',
	        ykeys: ['anc', 'delivery', 'pnc', 'infantpnc'],
	        labels: ['ANC', 'Labour & Delivery', 'PNC', 'Infant PNC'],
	        hideHover: 'auto',
	        resize: true

	    });
	
	    Morris.Donut({
	        element: 'morris-donut-chart',
	        data: [{
	            label: "ANC",
	            value: '<?php
                   $sql_worklist = "
                   SELECT 
					(SELECT count(*) AS count FROM stag_odk_anc ) AS anc,
					(SELECT  count(*) AS count FROM stag_odk_delivery ) +  (SELECT  count(*) AS count FROM stag_odk_delivery_infant ) AS delivery,
					(SELECT count(*) AS count FROM stag_odk_pnc  ) AS pnc,
					(SELECT count(*) AS count FROM stag_odk_pnc_infant) AS infant_pnc";
				   $result_worklist = $conn->query($sql_worklist);
				   $row_worklist = mysqli_fetch_assoc($result_worklist);
				   
                  
                       echo $row_worklist["anc"];
                  
                  ?>'
	        }, {
	            label: "Delivery",
	            value: '<?php
                       echo $row_worklist["delivery"];
             
       
                  ?>'
	        }, {
	            label: "PNC",
	            value: '<?php
                       echo $row_worklist["pnc"];
       
                  ?>'
	        }, {
	            label: "Infant PNC",
	            value: '<?php
                       echo $row_worklist["infant_pnc"];
       
                  ?>'
	        }],
	        resize: true
	    });
	    
	    
	    Morris.Donut({
	        element: 'morris-donut-chart-openmrs',
	        data: [{
	            label: "ANC",
	            value: '<?php
                   $sql_worklist = "
                   SELECT 
					(SELECT count(*) AS count FROM encounter WHERE encounter_type = '8' AND voided = '0') AS anc,
					(SELECT count(*) AS count FROM encounter WHERE encounter_type = '9' AND voided = '0') AS delivery,
					(SELECT count(*) AS count FROM encounter WHERE encounter_type = '10' AND voided = '0') AS pnc,
					(SELECT count(*) AS count FROM encounter WHERE encounter_type = '11' AND voided = '0') AS infant_pnc";
				   $result_worklist = $conn3->query($sql_worklist);
				   $row_worklist = mysqli_fetch_assoc($result_worklist);
				   
                  
                       echo $row_worklist["anc"];
                  
                  ?>'
	        }, {
	            label: "Delivery",
	            value: '<?php
                       echo $row_worklist["delivery"];
             
       
                  ?>'
	        }, {
	            label: "PNC",
	            value: '<?php
                       echo $row_worklist["pnc"];
       
                  ?>'
	        }, {
	            label: "Infant PNC",
	            value: '<?php
                       echo $row_worklist["infant_pnc"];
       
                  ?>'
	        }],
	        resize: true
	    });
	    
	    
	    
	    
	});

    </script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>


</body>
</html>
<?php $conn->close();  ?>