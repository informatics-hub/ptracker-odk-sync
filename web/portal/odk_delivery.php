<?php
session_start();
if (!isset($_SESSION["username"])) {
    $errorMsg = "You must login first!";
    header("Location: login.php?e=" . $errorMsg . "");
}

error_reporting(E_ALL & ~E_NOTICE);

// specify date & timezone 
date_default_timezone_set('Africa/Windhoek');
$date          = date('Y/m/d H:i:s');

// connect to database & get openmrs logins
require_once('../push/database/config.php');

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Ministry of Health & Social Services ">
    <meta NAME="robots" CONTENT="none">
    <!-- UCSF Global Health Informatics Hub 2018/01 -->

    <title>ODK Entries</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="submit" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="../portal">PTracker ODK/OpenMRS SYNC</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> <?php echo $_SESSION["username"]; ?> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="../"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li class="sidebar-search">
                                            <form method="post" action="search.php">
                    <div class="input-group custom-search-form">
                        <input type="text" class="form-control" name="id" placeholder="Enter PTracker ID...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </form><!-- /input-group -->
                    <!-- /input-group -->
                </li>
                <li>
                    <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>
                <li>
                    <a href="worklist.php"><i class="fa fa-table fa-fw"></i> Worklist</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-table fa-fw"></i> Sync Log<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="synclog_all.php">All Entries</a>
                        </li>
                        <li>
                            <a href="synclog_anc.php">ANC</a>
                        </li>
                        <li>
                            <a href="synclog_delivery.php">Labour & Delivery</a>
                        </li>
                        <li>
                            <a href="synclog_pnc.php">PNC</a>
                        </li>
                        <li>
                            <a href="synclog_ipnc.php"> Infant PNC</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
               <li>
                    <a href="#"><i class="fa fa-table fa-fw"></i> ODK Records<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="odk_all.php">All Entries</a>
                        </li>
                        <li>
                            <a href="odk_anc.php">ANC</a>
                        </li>
                        <li>
                            <a href="odk_delivery.php">Labour & Delivery</a>
                        </li>
                        <li>
                            <a href="odk_pnc.php">PNC</a>
                        </li>
                        <li>
                            <a href="odk_ipnc.php"> Infant PNC</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">ODK Entries</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        
               <?php if(isset($_GET['e'])) { ?>
    <div class="alert alert-info alert-dismissable">
      <button type="submit" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
       <?php echo $_GET['e']; ?>
    </div><?php } ?>
    </div>
    <div class="well">
        <h4>Labour & Delivery Submissions</h4>
        
        <?php
        
           $sql = "SELECT ptracker_id, facility_code, TYPE, visit_date, username, submission_date, _URI FROM stag_odk_delivery";
           $result = $conn->query($sql);

           if ($result->num_rows > 0) {
               $rows_count=mysqli_num_rows($result);
               if ($rows_count == null) { $rows_count = '0'; }
           }
          ?>
        
        <h2 class="btn btn-default btn-lg btn-block"><?php echo number_format($rows_count); ?>  records.</h2>
    </div>
    <div class="panel panel-default">
                <div class="panel-heading">
                  Sync Log
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>PTracker ID</th>
                                <th>Type</th>
                                <th>Facility</th>
                                <th>Visit Date</th>
                                <th>Username</th>
                                <th>Submission date</th>
                                <th>_URI</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                             if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) { ?>
                                
                            <tr class="odd gradeX">
                                <td><?php echo  $row["ptracker_id"]; ?></td>
                                <td><?php echo  $row["type"]; ?></td>
                                <td><?php echo  $row["facility_code"]; ?></td>
                                <td><?php echo  $row["visit_date"]; ?></td>
                                <td><?php echo  $row["username"]; ?></td>
                                <td><?php echo  $row["submission_date"]; ?></td>
                                <td><?php echo  $row["_URI"]; ?></td>
                            </tr>			    
                            <?php	}
                            } else {
                               
                        }
                        
                        ?>
                         </tbody>
                    </table>
                    <!-- /.table-responsive -->
                  </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->



    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });
    </script>

</body>

</html>
<?php $conn->close();  ?>