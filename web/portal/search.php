<?php
session_start();

if(!isset($_SESSION["username"])) {
    $errorMsg = "You must login first!";
    header("Location: login.php?e=".$errorMsg."");  
}

//error_reporting(E_ALL & ~E_NOTICE);

// specify date & timezone 
date_default_timezone_set('Africa/Windhoek');
$date 		 = date('Y/m/d H:i:s');

// connect to database & get openmrs logins
require_once('../push/database/config.php'); 

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Ministry of Health & Social Services ">
    <meta NAME="robots" CONTENT="none">
    <!-- UCSF Global Health Informatics Hub 2018/01 -->

    <title>Search</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="submit" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../portal">PTracker ODK/OpenMRS SYNC</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <?php echo $_SESSION["username"]; ?> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="../"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                                                    <form method="post" action="search.php">
                            <div class="input-group custom-search-form">
                            	<input type="text" class="form-control" name="id" placeholder="Enter PTracker ID...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
						</form><!-- /input-group -->
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="worklist.php"><i class="fa fa-table fa-fw"></i> Worklist</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-table fa-fw"></i> Sync Log<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="synclog_all.php">All Entries</a>
                                </li>
                                <li>
                                    <a href="synclog_anc.php">ANC</a>
                                </li>
                                <li>
                                    <a href="synclog_delivery.php">Labour & Delivery</a>
                                </li>
                                <li>
                                    <a href="synclog_pnc.php">PNC</a>
                                </li>
                                <li>
                                    <a href="synclog_ipnc.php"> Infant PNC</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                       <li>
                            <a href="#"><i class="fa fa-table fa-fw"></i> ODK Records<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="odk_all.php">All Entries</a>
                                </li>
                                <li>
                                    <a href="odk_anc.php">ANC</a>
                                </li>
                                <li>
                                    <a href="odk_delivery.php">Labour & Delivery</a>
                                </li>
                                <li>
                                    <a href="odk_pnc.php">PNC</a>
                                </li>
                                <li>
                                    <a href="odk_ipnc.php"> Infant PNC</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Search "<?php echo $_POST['id']; ?>"</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                
                       <?php if(isset($_GET['e'])) { ?>
            <div class="alert alert-info alert-dismissable">
              <button type="submit" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
               <?php echo $_GET['e']; ?>
            </div><?php } ?>
            </div>
            <div class="well">
                <p>Search for patient record on OpenMRS, ODK and the Sync Log using the PTracker ID</p>
                
                <?php
                
                
                   $sql_openmrs_q = "
                   SELECT encounter.encounter_datetime AS visit_date,
					encounter.encounter_type AS visit_type,
					patient_identifier.identifier AS ptracker_id,
					encounter.uuid,
					encounter.patient_id
					FROM encounter 
					JOIN  patient_identifier 
					ON patient_identifier.patient_id = encounter.patient_id
					WHERE encounter.voided = '0' AND patient_identifier.identifier = '".$_POST['id']."'";
					
				   $result4 = $conn3->query($sql_openmrs_q);
				   
				   if ($result4->num_rows > 0) {
                       $count_openmrs=mysqli_num_rows($result4);
                   } else  { 
                   
                   $count_openmrs = "0"; 
                   
                   echo mysqli_error($conn3);
                   }



                   $sql_worklist = "SELECT * FROM vw_ptracker_worklist where ptracker_id = '".$_POST['id']."' ";
				   $result1 = $conn->query($sql_worklist);

                   if ($result1->num_rows > 0) {
                       $count_worklist=mysqli_num_rows($result1);
                   } else  { $count_worklist = "0"; }

                   $sql_synclog = "SELECT * FROM stag_ptracker_synclog where ptracker_id = '".$_POST['id']."' ";
				   $result2 = $conn->query($sql_synclog);

                   if ($result2->num_rows > 0) {
                       $count_synclog=mysqli_num_rows($result2);
                   } else  { $count_synclog = "0"; }

                   $sql_odk = "
                  	SELECT ptracker_id, facility_code, TYPE as visit_type , visit_date, _URI FROM stag_odk_anc WHERE ptracker_id = '".$_POST['id']."' 
					UNION ALL
					SELECT ptracker_id, facility_code, TYPE as visit_type , visit_date, _URI FROM stag_odk_pnc WHERE ptracker_id = '".$_POST['id']."' 
					UNION ALL
					SELECT ptracker_id, facility_code, TYPE as visit_type , visit_date, _URI FROM stag_odk_pnc_infant WHERE ptracker_id = '".$_POST['id']."' 
					UNION ALL
					SELECT ptracker_id, facility_code, TYPE as visit_type , visit_date, _URI FROM stag_odk_delivery WHERE ptracker_id = '".$_POST['id']."' 
					";
				   $result3 = $conn->query($sql_odk);

                   if ($result3->num_rows > 0) {
                       $count_odk=mysqli_num_rows($result3);
                   } else  { $count_odk = "0"; }

                  ?>
                
                <h2 class="btn btn-default btn-lg"><?php echo number_format($count_worklist); ?><br> Worklist</h2>
                <h2 class="btn btn-default btn-lg"><?php echo number_format($count_synclog); ?><br> Sync Log</h2>
                <h2 class="btn btn-default btn-lg"><?php echo number_format($count_odk); ?><br> ODK Submissions</h2>
                <h2 class="btn btn-default btn-lg"><?php echo number_format($count_openmrs); ?><br> OpenMRS</h2>
            </div>
            <div class="panel panel-default">
                        <div class="panel-heading">
                            Search results
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Found In</th>
                                        <th>Visit Date</th>
                                        <th>Visit Type</th>
                                        <th>PTracker ID</th>
                                        <th>_URI / Encounter UUID</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                	if ($result1->num_rows > 0) {
                                		// output data of each row
									    while($row1 = $result1->fetch_assoc()) { ?>
									    
									<tr class="odd gradeX">
                                        <td>Worklist</td>
                                        <td><?php echo  $row1["visit_date"]; ?></td>
                                        <td><?php 
                                        if ($row1["visit_type"] == "1") { echo "ANC";  }
                                        if ($row1["visit_type"] == "2") { echo "L&D";  }
                                        if ($row1["visit_type"] == "3") { echo "PNC";  }
                                        if ($row1["visit_type"] == "4") { echo "Infant PNC";  }  ?></td>
                                        <td><?php echo  $row1["ptracker_id"]; ?></td>
                                        <td><?php echo  $row1["_URI"]; ?></td>
                                    </tr>			    
									<?php } } ?>
							       <?php 
                                	if ($result2->num_rows > 0) {
                                		// output data of each row
									    while($row2 = $result2->fetch_assoc()) { ?>
									    
									<tr class="odd gradeX">
                                        <td>Sync Log</td>
                                        <td><?php echo  $row2["visit_date"]; ?></td>
                                        <td><?php echo  $row2["type"]; ?></td>
                                        <td><?php echo  $row2["ptracker_id"]; ?></td>
                                        <td><?php echo  $row2["odk_uuid"]; ?></td>
                                    </tr>			    
									<?php } } ?>
							        <?php 
                                	if ($result3->num_rows > 0) {
                                		// output data of each row
									    while($row3 = $result3->fetch_assoc()) { ?>
									    
									<tr class="odd gradeX">
                                        <td>ODK</td>
                                        <td><?php echo  $row3["visit_date"]; ?></td>
                                        <td><?php echo  $row3["visit_type"]; ?></td>                                        
                                        <td><?php echo  $row3["ptracker_id"]; ?></td>
                                        <td><?php echo  $row3["_URI"]; ?></td>
                                    </tr>			    
									<?php } } ?>
						          <?php 
                                	if ($result4->num_rows > 0) {
                                		// output data of each row
									    while($row4 = $result4->fetch_assoc()) { ?>
									    
									<tr class="odd gradeX">
                                        <td><a href="<?php echo $openmrs_url; ?>/coreapps/clinicianfacing/patient.page?patientId=<?php echo $row4["patient_id"]; ?>" target="_blank">OpenMRS</a></td>
                                        <td><?php echo  $row4["visit_date"]; ?></td>
                                        <td><?php 
                                        if ($row4["visit_type"] == "8") { echo "ANC";  }
                                        if ($row4["visit_type"] == "9") { echo "L&D";  }
                                        if ($row4["visit_type"] == "10") { echo "PNC";  }
                                        if ($row4["visit_type"] == "11") { echo "Infant PNC";  }  ?></td>
                                        <td><?php echo  $row4["ptracker_id"]; ?></td>
                                        <td><?php echo  $row4["uuid"]; ?></td>
                                    </tr>			    
									<?php } } ?>
                                 </tbody>
                            </table>
                            <!-- /.table-responsive -->
                          </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->



            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>
    
    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

</body>
</html>
<?php $conn->close();  ?>
