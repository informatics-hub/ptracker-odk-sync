<?php
/*
version: 	1.0
author:		Asen Mwandemele
date:		November 2017

add observation Infant ARV Prophylaxis status
___________________________________
change log
___________________________________
date:		author:			comment:
29/01/2018	Asen			Corrected concept ids




*/ 			    
$obs_to_add = "Infant ARV Prophylaxis status";

switch ($odk_ipnc_arv_prophylaxis_status) {
    case "1":
    	//	1 Received ARV Prophylaxis (164922) = 	54431b42-dae4-4d19-9034-44ba7489a334
        $obs_coded_value = '857c5868-bdcd-4f5c-aadd-8b0fa7de8481';
        break;
    case "2":
    	//	2 Never received ARV prophylaxis (164923) = e2ae63f3-e4c9-4822-bf18-c894928556c8
        $obs_coded_value = "e2ae63f3-e4c9-4822-bf18-c894928556c8"; 
        break;
    case "3":
    	//3 Stopped ARV prophylaxis according to guideline (164924) = 	8eb8dee6-5342-4529-ad2f-4fc5a66becd9
        $obs_coded_value = "8eb8dee6-5342-4529-ad2f-4fc5a66becd9"; 
        break;
    case "66":
    	// 66 = Missing (164932) = 54b96458-6585-4c4c-a5b1-b3ca7f1be351
    	$obs_coded_value = "54b96458-6585-4c4c-a5b1-b3ca7f1be351"; 
  		break;
    default:
    	// code not recognized	
        $obs_coded_value = "54b96458-6585-4c4c-a5b1-b3ca7f1be351"; 
        echo "<br>".$date." Unknown obs code, setting ".$obs_to_add." to missing";
}

$obs_data = array(); 

$obs_data['person']			= $created_openmrs_person_uuid; // person uuid 
$obs_data['obsDatetime']    = $odk_visit_date_odk_recorded; //time of visit?
$obs_data['concept']        = "1148AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";  // concept uuid 
$obs_data['value']   		= $obs_coded_value; // value of the observation 
$obs_data['encounter']      = $created_openmrs_encounter_uuid; // encounter uuid
$obs_data['location']		= $odk_location_facility_encounter;

$obs_data = json_encode($obs_data);
  
$obs_curL = curl_init(''.$openmrs_url.'/ws/rest/v1/obs');
curl_setopt($obs_curL, CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
curl_setopt($obs_curL, CURLOPT_USERPWD,''.$openmrs_logins.'');
curl_setopt($obs_curL, CURLOPT_RETURNTRANSFER,true);
curl_setopt($obs_curL, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); 
curl_setopt($obs_curL, CURLOPT_POST,1);
curl_setopt($obs_curL, CURLOPT_POSTFIELDS,$obs_data);
$obs_output   = curl_exec($obs_curL);
$obs_status   = curl_getinfo($obs_curL,CURLINFO_HTTP_CODE);
curl_close($obs_curL);

// get json response
$obs_response = json_decode($obs_output);

//echo $obs_output;

// display error message or output uuid of person created
$obs_create_error = $obs_response->error->message;

if (isset($obs_response->error->message)) { 

	echo "<br>".$date." Error: ".$obs_response->error->message.""; 
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Error adding obs ".$obs_to_add."', http_status_code = '".$obs_status."', error_message = '".$obs_create_error."' WHERE id = '".$sync_log_id."'";
	echo "<br>".$date." Error adding obs <em>".$obs_to_add."</em>";
	
} else {
	$created_openmrs_obs_uuid = $obs_response->uuid;
	echo "<br>".$date." Added obs <em>".$obs_to_add."</em> - '".$created_openmrs_obs_uuid."'";
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Added obs', openmrs_obss_added = '".$obs_to_add."', http_status_code = '".$obs_status."', error_message = '".$obs_create_error."' WHERE id = '".$sync_log_id."'";

}
 
if ($conn->query($sql_update) === TRUE) {
    //echo "<br>".$date." Updated Sync Log Record ".$created_openmrs_obs_uuid."";
} else {
    echo "Error: " . $sql_update . "<br>" . $conn->error;
}




?>