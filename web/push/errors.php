<?php
/*
version: 	1.0
author:		Asen Mwandemele
date:		February 2017

Sends errors notifications to slack
___________________________________
change log
___________________________________
date:		author:			comment:

*/ 

// Report all errors except E_NOTICE
error_reporting(E_ALL & ~E_NOTICE);

// load boostrap styles
echo '
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
';

// specify date & timezone 
date_default_timezone_set('Africa/Windhoek');
$date 		 = date('Y/m/d H:i:s');

// connect to database & get openmrs logins
require_once('database/config.php'); 

//echo "<h4>PTracker ODK OpenMRS Sync</h4>";
echo "".$date." Start of <strong>errors</strong> run<br>";

// sql query to fetch data from odk view
$sql = "SELECT * FROM stag_ptracker_synclog WHERE error_message<>''";  // loads all records not listed in the sync_log
$result = $conn->query($sql);

// start to loop through each record returned in query
if ($result->num_rows > 0) {
        
	while($row = $result->fetch_assoc()) {
        
        $message = ">".$date." ".$row['type']." 😰 - An error occured when syncing ".$row['ptracker_id'].", SyncLog ID ".$row['id']." with this message:
        ".$row['error_message']."";
        include 'slack.php';
        echo $message."<br>";     
    }
} else {
    
echo "<br>".$date." <strong>No</strong> errors found.";
        
}

echo "<br>".$date." End of run.";
// close connection
$conn->close();
?>