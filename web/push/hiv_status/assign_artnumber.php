<?php
/*
version: 	1.0
author:		Asen Mwandemele
date:		December 2017

assign art number identifier to person
___________________________________
change log
___________________________________
date:		author:			comment:


*/

$identifier_to_add = "ART Number";

if ($odk_art_number != null) {
 			    

$identifier_data = array(); 

$identifier_data['identifier'] = $odk_art_number;
$identifier_data['identifierType'] = "9d6d1eec-2cd6-4637-a981-4a46b4b8b41f"; // ART Number    
     
$identifier_data = json_encode($identifier_data);
  
$identifier_curL = curl_init(''.$openmrs_url.'/ws/rest/v1/patient/'.$created_openmrs_person_uuid.'/identifier');
curl_setopt($identifier_curL, CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
curl_setopt($identifier_curL, CURLOPT_USERPWD,''.$openmrs_logins.'');
curl_setopt($identifier_curL, CURLOPT_RETURNTRANSFER,true);
curl_setopt($identifier_curL, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); 
curl_setopt($identifier_curL, CURLOPT_POST,1);
curl_setopt($identifier_curL, CURLOPT_POSTFIELDS,$identifier_data);
$identifier_output   = curl_exec($identifier_curL);
$identifier_status   = curl_getinfo($identifier_curL,CURLINFO_HTTP_CODE);
curl_close($identifier_curL);

// get json response
$identifier_response = json_decode($identifier_output);

//echo $identifier_output;

// display error message or output uuid of person created
$identifier_create_error = $identifier_response->error->message;

// remove inverted commas for MySQl insert
$identifier_create_error_comma = str_replace("'"," ",$identifier_create_error); 


if (isset($identifier_response->error->message)) { 

	echo "<br>".$date." Error: ".$identifier_response->error->message.""; 
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Error adding identifier ".$identifier_to_add."',  art_num_assigned = '" . $identifier_create_error_comma . "', http_status_code = '".$identifier_status."', error_message = '".$identifier_create_error_comma."' WHERE id = '".$sync_log_id."'";
	echo "<br>".$date." Error assigning identifier <em>".$identifier_to_add."</em>";
	
} else {
	$created_openmrs_identifier_uuid = $identifier_response->uuid;
	echo "<br>".$date." Assigned identifier <em>".$identifier_to_add."</em> - ".$created_openmrs_identifier_uuid."";
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Added identifier', art_num_assigned = 'TRUE', http_status_code = '".$identifier_status."', error_message = '".$identifier_create_error."' WHERE id = '".$sync_log_id."'";

}
 
if ($conn->query($sql_update) === TRUE) {
    //echo "<br>".$date." Updated Sync Log Record ".$created_openmrs_identifier_uuid."";
} else {
    echo "Error: " . $sql_update . "<br>" . $conn->error;
}


} else {	echo "<br>".$date." No value for identifier <em>".$identifier_to_add."</em>";  }
?>