<?php
/*
version: 	1.6
author:		Asen Mwandemele
date:		December 2017

infant pnc visit
___________________________________
change log
___________________________________
date:		author:			comment:
09/01/2018	Asen			Added an if statement that checks if encounter id has been created before creating observations etc.
10/01/2018	Asen			Added section to check openmrs if patient with ptracker id exists
							Added section to check if sync log record is created first
11/01/2018	Asen			Changes to country field and if statement for when fields are empty
12/01/2018	Asen			Added section to find provider id based on given username
16/01/2018	Asen			Changed SQL Query to reflect _URI rather than ptracker id and visit date
22/01/2018	Asen			Resolved issue where sync log is created even though openmrs server is down. 
26/01/2018	Asen			Added missing ART Number, added art linkage scrips and transfer status scripts
12/02/2018	Asen			Remove Age and DOB variables for infant and added corresponding variable from SQL View
20/02/2018	Asen			Fixed issue where infant PNC would not be recorded due to error is SQL query
20/04/2018	Asen			Added breastfeeding status questions (E001-05. Update the ODK sync to conform with the changes to the breastfeeding question)
15/08/2018	Asen			Fixes steming from Next visit date bug
16/08/2018	Asen			Fixes to infant transfered to ART Clinic
21/08/2018  Asen            Fixed to retreive location_id from openmrs database as facility id
20/01/2020	Asen			Added remove from worklist file to remove completed record from the worklist staging table
							Alter the MySQL query to reflect new worklist and encounter staging table
17/07/2020	Asen			Added new relationship script for mother child at pnc 

*/ 

// Report all errors except E_NOTICE
error_reporting(E_ALL & ~E_NOTICE);

// load boostrap styles
echo '
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
';

// specify date & timezone 
date_default_timezone_set('Africa/Windhoek');
$date 		 = date('Y/m/d H:i:s');

// connect to database & get openmrs logins
require_once('database/config.php'); 

//echo "<h4>PTracker ODK OpenMRS Sync</h4>";
echo "".$date." Start of <strong>Infant PNC</strong> run";

// sql query to fetch data from odk view
$sql = "SELECT * FROM stag_odk_pnc_infant WHERE _URI IN (SELECT _URI FROM stag_ptracker_worklist) LIMIT 20";  // loads all records not listed in the sync_log
$result = $conn->query($sql);

// start to loop through each record returned in query
if ($result->num_rows > 0) {
    $rows_count=mysqli_num_rows($result); // count number of rows
	echo "<br>".$date." Found <strong>".$rows_count."</strong> records.";
	
	while($row = $result->fetch_assoc()) {
        echo "<br>".$date." Processing ID <strong>" . $row["ptracker_id"]. "</strong>";

        		$odk_ptrackerid = $row["ptracker_id"];
        		$odk_visit_date = $row["visit_date"]; // Format should YYYY-MM-DD HH:MM:SS to avoid mixed updates and errors
        		$odk_visit_date_odk_recorded = $row["visit_date_recorded_odk"]; // Required specifically for visit date/time for openmrs
        		$odk_openmrs_id= $row["openmrs_id"];
        		$generated_openmrs_id = "";
        		$odk_openmrs_person_uuid = $row["openmrs_person_uuid"];
        		$odk_openmrs_patient_uuid = $row["openmrs_patient_uuid"];
        		$odk_odk_uuid = $row["odk_uuid"];
        		$odk_given = $row["given"];
        		$odk_middle = $row["middle"];
        		$odk_family = $row["family"];
        		$odk_given_new = $row["given_new"];
        		$odk_middle_new = $row["middle_new"];
        		$odk_family_new = $row["family_new"];        		
        		$odk_sex = $row["sex"];
        		$odk_dob = $row["dob_infant"];
				$odk_age = null;
				$odk_phone_number = $row["phone_number"];
				$odk_kin_name = $row["kin_name"];
				$odk_kin_contact = $row["kin_contact"];
				//infant pnc
				$parent_ptracker_id = $row["parent_ptracker_id"];
				$parent_openmrs_uuid = $row["openmrs_parent_uuid"];
				$odk_ipnc_hiv_exposure_status = $row["hiv_exposure_status"];
				$odk_ipnc_arv_prophylaxis_status = $row["arv_prophylaxis_status"];
				$odk_ipnc_ctx_prophylaxis_status = $row["ctx_prophylaxis_status"];
				$odk_ipnc_infant_hiv_tested = $row["infant_hiv_tested"];
				$odk_ipnc_arv_prophylaxis_adherence = $row["arv_prophylaxis_adherence"];
				$odk_ipnc_ctx_prophylaxis_adherence = $row["ctx_prophylaxis_adherence"];
				$odk_ipnc_infant_hiv_test_used = $row["infant_hiv_test_used"];
				$odk_ipnc_infant_hiv_test_result_pcr = $row["infant_hiv_test_result_pcr"];
				$odk_ipnc_infant_hiv_test_result = $row["infant_hiv_test_result"];
				$odk_ipnc_infant_hiv_confirmatory_test = $row["infant_hiv_test_conf"];
				$odk_ipnc_infant_hiv_confirmatory_test_result = $row["infant_hiv_test_conf_result"];
                $odk_pnc_breastfeeding_status = $row["infant_breastfeeding_status"];
                
                $odk_infant_event_date = $row["infant_event_date"];
                $odk_infant_event_date_missing = $row["infant_event_date_missing"];
                $odk_infant_transferto_artclinic_date = $row["infant_transferto_artclinic_date"];
                $odk_infant_transferto_artclinic_date_missing = $row["infant_transferto_artclinic_date_missing"];
                $odk_infant_transferto_artclinic = $row["infant_transferto_artclinic"];
                $odk_infant_transferto_artclinic_missing = $row["infant_transferto_artclinic_missing"];
                    
                $odk_infant_transfer_status = $row["infant_transfer_status"];
				$odk_infant_transferin = $row["infant_transferin"];
				$odk_infant_transferin_other = $row["infant_transferin_other"];
				$odk_infant_transferin_date = $row["infant_transferin_date"];
				$odk_infant_transferin_date_missing = $row["infant_transferin_date_missing"];
				$odk_infant_transfer_out = $row["infant_transfer_out"];
				$odk_infant_transfer_out_other = $row["infant_transfer_out_other"];
				$odk_infant_transfer_out_date = $row["infant_transfer_out_date"];
				$odk_infant_transfer_out_date_missing = $row["infant_transfer_out_date_missing"];
				
				$odk_infant_next_visit_date = $row["infant_next_visit_date"];
				$odk_infant_next_visit_date_missing = $row["infant_next_visit_date_missing"];
				
				$odk_infant_art_linked = $row["infant_art_linked"];
				
				$odk_art_number = $row["art_number"];
				$odk_odk_art_number_missing = $row["art_number_missing"];
				$odk_infant_art_linked = $row["infant_art_linked"];
				
				$infant_date_death = $row["infant_date_death"];
				$infant_date_death_mising = $row["infant_date_death_mising"];
				//misc
				$odk_provider_person_uuid = $row["provider_uuid"];
				$odk_provider_username = $row["username"];
				$odk_address = $row["address"];
				$odk_address_locaiton = $row["location"];
				$odk_country_district = '';
				$facility_uuid = $row['facility_uuid'];
				if ($row['facility_code'] != null) { $odk_facility = $row['facility_code']; } else { $odk_facility = $row['facility_code_other']; } // used to record facility code in staging table only

				//gets provider ID from openmrs database
				include 'general/provider_id.php';	

				// match locations with that on openmrs
				// facilities
				include 'general/facilities_encounter.php';				

				// countries
				include 'general/countries.php';				

				// districts
				include 'general/districts.php';				

				//creates record in log table
				include 'general/new_record.php';	
				
				if ($new_record_created != 1) {
				
				echo "<br>".$date." Sync record not created."; 
				
				} else  {
				 
					    // check openmrs if patient with ptracker id exists
					    include 'general/check_ptrackerid.php';
					    
					    
					    // if patient with ptracker id exists, it should create
					    if ($found_openmrs_patient_identifier_uuid == null) { 
					    
						   	 	// create new person
						   	 	include 'general/new_person.php';
			
						   	 	if ($created_openmrs_person_uuid != null) { 
								   	 	
								   	 	//generate openmrs id
								   	 	include 'general/idgen.php';
								   	 	
								   	 	// create patient
										include 'general/new_patient.php';
										
										// assign PTracker identifier to new person
										include 'general/assign_ptrackerid.php';
										
										//  add attributes to person
										// 	phone number
										if ($odk_phone_number != null) { include 'general/add_attribute_phone.php'; }
							
										// 	next of kin
										if ($odk_kin_name != null) { include 'general/add_attribute_kin.php'; }
											
										// 	next of kin phone number
										if ($odk_kin_contact != null) { include 'general/add_attribute_kin_phone.php'; }
											
										// 	addresses
										include 'general/add_addresses.php'; 
								}
					    
					    } else {
					    		// Set the patient uuid found in openmrs to be used for encounter
						    	$created_openmrs_patient_uuid = $found_openmrs_patient_identifier_uuid;	
						    	
						    	// Set the person uuid found in openmrs to be used for encounter
						    	$created_openmrs_person_uuid = $found_openmrs_person_identifier_uuid;
						    	
						    	// Update infant's name if the name field is not blank
								if ($odk_given != null) { 
								
									$odk_given_new = $odk_given;
									$odk_family_new = $odk_family;
									$odk_middle_new = $odk_middle;
									
									include 'general/update_person.php'; 
									
									} else if ($odk_given_new != null) {
																			
									include 'general/update_person.php'; 
									
									}
																	
								//  add attributes to person
								// 	phone number
								if ($odk_phone_number != null) { include 'general/add_attribute_phone.php'; }
							
								// 	next of kin
								if ($odk_kin_name != null) { include 'general/add_attribute_kin.php'; }
											
								// 	next of kin phone number
								if ($odk_kin_contact != null) { include 'general/add_attribute_kin_phone.php'; }
						    	
						    	echo "<br>".$date." <strong>Jump to</strong> Add Encounter";
					    }
					    
					    if ($created_openmrs_patient_uuid != null) { 
					    		
					    		// Update infant's name if the name is given
								if ($odk_given != '') { 
								
								    $odk_given_new = $odk_given;
								    $odk_family_new = $odk_family;
								    $odk_middle_new = $odk_middle;
									
								    include 'general/update_person.php'; 
                                }
									
					    		// create encounter
								include 'infant_pnc/new_encounter.php'; 
								
								// checks if encounter was created first.
								if ($created_openmrs_encounter_uuid != null) { 
								
									                // Record observation - pnc_breastfeeding_status 
                                                    if ($odk_pnc_breastfeeding_status != null) { include 'infant_pnc/new_obs_bf_status.php'; }

                                                    // Record observation - hiv infant exposure status
													if ($odk_ipnc_hiv_exposure_status != null) { include 'infant_pnc/new_obs_hiv_exposure_status.php'; }
													
													// Record observation - ARV Prophylaxis Status 
													if ($odk_ipnc_arv_prophylaxis_status != null) { include 'infant_pnc/new_obs_arv_prophylaxis_status.php'; }
									
													// Record observation - new_obs_ctx_prophylaxis_status
													if ($odk_ipnc_ctx_prophylaxis_status != null) { include 'infant_pnc/new_obs_ctx_prophylaxis_status.php'; }
													
													// Record observation - ipnc_infant_hiv_tested
													if ($odk_ipnc_infant_hiv_tested != null) { include 'infant_pnc/new_obs_infant_hiv_tested.php'; }
													
													// Record observation - ipnc_arv_prophylaxis_adherence
													if ($odk_ipnc_arv_prophylaxis_adherence != null) { include 'infant_pnc/new_obs_arv_prophylaxis_adherence.php'; }
													
													// Record observation - ipnc_ctx_prophylaxis_adherence
													if ($odk_ipnc_ctx_prophylaxis_adherence != null) { include 'infant_pnc/new_obs_ctx_prophylaxis_adherence.php'; }
													
													// Record observation - What kind of test was used?
													if ($odk_ipnc_infant_hiv_test_used != null) { include 'infant_pnc/new_obs_infant_hiv_test_used.php'; }
																	
													// Record observation - What was the DNA PCR test result?
													if ($odk_ipnc_infant_hiv_test_result_pcr != null) { include 'infant_pnc/new_obs_infant_hiv_test_result_pcr.php'; }
									
													// Record observation - What was the Rapid test result?
													if ($odk_ipnc_infant_hiv_test_result != null) { include 'infant_pnc/new_obs_infant_hiv_test_result.php'; }
									
													// Record observation - Was a confirmatory test performed during this visit?
													if ($odk_ipnc_infant_hiv_confirmatory_test != null) { include 'infant_pnc/new_obs_infant_hiv_confirmatory_test.php'; }
									
													//Record observation - confirmatory test result?
													if ($odk_ipnc_infant_hiv_confirmatory_test_result != null) { include 'infant_pnc/new_obs_infant_hiv_confirmatory_test_result.php'; }
									
													// Assign identifier - hiv - art number
													if ($odk_odk_art_number_missing != null) { include 'hiv_status/new_obs_art_number_missing.php'; }	else { include 'hiv_status/assign_artnumber.php'; }	
													
													// Record observation - Infant PNC -  Transfer status 
													if ($odk_infant_transfer_status != null) { include 'infant_pnc/new_obs_infant_transfer_status.php'; }

                                                    // Record observation - Infant PNC -  Event Date
													if ($odk_infant_event_date != null) { include 'infant_pnc/new_obs_infant_event_date.php'; }
                                                    
                                                    // Record observation - Infant PNC -  Event Date Missing
													if ($odk_infant_event_date_missing != null) { include 'infant_pnc/new_obs_infant_event_date_missing.php'; }
                                    
                                                    // Record observation - Infant PNC -  Transfer to ART Clinic Date
													if ($odk_infant_transferto_artclinic_date != null) { include 'infant_pnc/new_obs_infant_transferto_artclinic_date.php'; }
                                    
                                                    // Record observation - Infant PNC -  Transfer to ART Clinic Date
													if ($odk_infant_transferto_artclinic_date_missing != null) { include 'infant_pnc/new_obs_infant_transferto_artclinic_date_missing.php'; }
                                    
                                                    // Record observation - Infant PNC - Transfer to ART Clinic Missing
													if ($odk_infant_transferto_artclinic_missing != null) { include 'infant_pnc/new_obs_infant_transferto_artclinic_missing.php'; }
                                    
                                                    // Record observation - Infant PNC - Transfer to ART Clinic 
													if ($odk_infant_transferto_artclinic != null) { include 'infant_pnc/new_obs_infant_transferto_artclinic.php'; }                                    
													
                                                    // Record observation - Infant PNC -  Transfer In 
													if ($odk_infant_transferin != null) { include 'infant_pnc/new_obs_infant_pnc_transfer_in.php'; }

													// Record observation - Infant PNC -  Transfer In Date
													if ($odk_infant_transferin_date_missing != null) { include 'infant_pnc/new_obs_transferin_date_missing.php'; } else { include 'infant_pnc/new_obs_transferin_date.php'; }

													// Record observation - Infant PNC -  Transfer Out 
													if ($odk_infant_transfer_out != null) { include 'infant_pnc/new_obs_infant_pnc_transfer_out.php'; }

													// Record observation - Infant PNC -  Transfer Out Date
													if ($odk_infant_transfer_out_date_missing != null) { include 'infant_pnc/new_obs_transferout_date_missing.php'; } else { include 'infant_pnc/new_obs_transferout_date.php'; }

													// Record observation - Infant PNC - next visit date
													if ($odk_infant_next_visit_date_missing != null) { include 'infant_pnc/new_obs_next_visit_date_missing.php'; }
                                    
                                                    if ($odk_infant_next_visit_date != null) { include 'infant_pnc/new_obs_next_visit_date.php'; }

													// Record observation - Infant PNC - date of death
													if ($infant_date_death_mising != null) { include 'infant_pnc/new_obs_date_of_death_missing.php'; } else { include 'infant_pnc/new_obs_date_of_death.php'; }

													// Record observation - Infant PNC -  ART Linkage
													if ($odk_infant_art_linked != null) { include 'infant_pnc/new_obs_infant_art_linkage.php'; }

													// Create parent child relationship
													// First check if parent uuid is present
													if ($parent_openmrs_uuid != null) { 														
														//assign parent uuid to current parent uuid variable 
														$current_openmrs_parent_uuid = $parent_openmrs_uuid;
													} else {
														// check openmrs if parent with ptracker id exists														
														if ($parent_ptracker_id != null) { 
															include 'general/check_mother_ptrackerid.php'; 															
															if ($found_openmrs_m_patient_identifier_uuid == null) { 
																// no parent with ptracker id exists
																echo "<br>".$date." <strong>No</strong> parent with Ptracker ID exists in openmrs database.";
															} else {
																//assign found uuid to current parent uuid variable 
																$current_openmrs_parent_uuid = $found_openmrs_m_patient_identifier_uuid;
															}
														}														
													}
													// create the relationship
													if ($current_openmrs_parent_uuid != null) {
														$current_openmrs_child_uuid = $created_openmrs_patient_uuid;
														include 'general/new_relationship_pnc.php'; 
													} else {
														echo "<br>".$date." <strong>No</strong> parent relationship possible.";
													}										
								
								} else { echo "<br>".$date." Encounter not created.";
								
										//deletes record in log table
										include 'general/delete_record.php';
										
								}
		
						//deletes record in worklist table
						include 'general/delete_from_worklist.php';	

						} else { 
								//deletes record in log table
								include 'general/delete_record.php';

								die('<br>'.$date.' Stopping sync process until next attempt.') ; 
						 }
				}
    }
} else {
    
    echo "<br>".$date." <strong>No</strong> new records to process.";
}

echo "<br>".$date." End of run.";
// close connection
$conn->close();
?>