<?php
/*
version: 	1.5
author:		Asen Mwandemele
date:		December 2017

labour visit
___________________________________
change log
___________________________________
date:		author:			comment:
09/01/2018	Asen			Added an if statement that checks if encounter id has been created before creating observations etc.
10/01/2018	Asen			Added section to check openmrs if patient with ptracker id exists
							Added section to check if sync log record is created first
11/01/2018	Asen			Changes to country field and if statement for when fields are empty
12/01/2018	Asen			Added missing l&d fields before final review and merge
16/01/2018	Asen			Changed SQL Query to reflect _URI rather than ptracker id and visit date
16/01/2018	Asen			Added section that creates child group to record infant observations in L&D
02/02/2018	Asen			Fixed issue where infants born to existing ptracker clients would fail due to missing client name and address
12/02/2018	Asen			Added HIV Re-Test at 36 Weeks variable and corresponding script 
02/03/2018  Asen            Limit the result to 5 records per run
21/08/2018  Asen            Fixed to retreive location_id from openmrs database as facility id
20/01/2020	Asen			Added remove from worklist file to remove completed record from the worklist staging table
							Alter the MySQL query to reflect new worklist and encounter staging table
02/2020		Herobiam 		Added ANC First HIV Test Status

*/ 

// Report all errors except E_NOTICE
error_reporting(E_ALL & ~E_NOTICE);

// load boostrap styles
echo '
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
';

// specify date & timezone 
date_default_timezone_set('Africa/Windhoek');
$date 		 = date('Y/m/d H:i:s');

// connect to database & get openmrs logins
require_once('database/config.php'); 

//echo "<h4>PTracker ODK OpenMRS Sync</h4>";
echo ''.$date.' Start of <strong>Labour & Delivery</strong> run';

// sql query to fetch data from odk view
$sql = "SELECT * FROM stag_odk_delivery WHERE _URI IN (SELECT _URI FROM stag_ptracker_worklist) LIMIT 20";  // loads all records not present in the work list view
$result = $conn->query($sql);

// start to loop through each record returned in query
if ($result->num_rows > 0) {
    $rows_count=mysqli_num_rows($result); // count number of rows
	echo '<br>'.$date.' Found <strong>'.$rows_count.'</strong> records.';
	
	while($row = $result->fetch_assoc()) {
        echo '<br>'.$date.' Processing ID <strong>' . $row['ptracker_id']. '</strong>';

        		$odk_ptrackerid = $row['ptracker_id'];
        		$odk_visit_date = $row['visit_date']; // Format should YYYY-MM-DD HH:MM:SS to avoid mixed updates and errors
        		$odk_visit_date_odk_recorded = $row['visit_date_recorded_odk']; // Required specifically for visit date/time for openmrs
        		$odk_openmrs_id= $row['openmrs_id'];
        		$generated_openmrs_id = '';
        		$odk_openmrs_person_uuid = $row['openmrs_person_uuid'];
        		$odk_openmrs_patient_uuid = $row['openmrs_patient_uuid'];
        		$odk_odk_uuid = $row['_URI'];
        		$odk_given = $row['given'];
        		$odk_middle = $row['middle'];
        		$odk_family = $row['family'];
        		$odk_sex = $row['sex'];
        		$odk_dob = $row['dob'];
				$odk_age = $row['age'];
				$odk_phone_number = $row['phone_number'];
				$odk_kin_name = $row['kin_name'];
				$odk_kin_contact = $row['kin_contact'];
				//L&D
				$odk_ld_has_pinkbook = $row['ld_has_pinkbook'];
				$odk_ld_motherstatus= $row['ld_motherstatus'];
				$odk_ld_discharge_date = $row['ld_discharge_date'];
				$odk_ld_discharge_date_missing = $row['ld_discharge_date_missing'];
				$odk_ld_numberof_infants = $row['ld_numberof_infants'];
				$odk_ld_motherstatus_transfered = $row['ld_motherstatus_transfered'];
				$odk_ld_motherstatus_transfered_other = $row['ld_motherstatus_transfered_other'];
				//Hiv status
				$odk_anc_first_hiv_test_status = $row['anc_first_hiv_test_status']; //change
				$odk_hiv_test_status = $row['hiv_test_status'];
				$odk_hiv_retest_status = $row['hiv_retest_status'];
				$odk_hiv_test_result = $row['hiv_test_result'];
				$odk_art_int_status = $row['art_int_status'];
				$odk_anc_art_initation = $row['anc_art_initation'];
				$odk_art_int_refused_reason = $row['art_int_status_refused_reason'];
				$odk_art_int_refused_reason_missing = $row['art_int_status_refused_reason_missing'];
				$odk_art_number = $row['art_number'];
				$odk_odk_art_number_missing = $row["art_number_missing"];
				$odk_art_start_date = $row['art_start_date'];
				$odk_vl_test_done = $row['vl_test_done'];
				$odk_vl_test_date = $row['vl_test_date'];
				$odk_vl_test_result = $row['vl_test_result'];
				$odk_vl_test_result_value = $row['vl_test_result_value'];
				//misc
				$odk_provider_person_uuid = $row['provider_uuid'];
				$odk_provider_username = $row['username'];
				$odk_address = $row['address'];
				$odk_address_locaiton = $row['location'];
				$odk_country_district = '';
				$facility_uuid = $row['facility_uuid'];
				if ($row['facility_code'] != null) { $odk_facility = $row['facility_code']; } else { $odk_facility = $row['facility_code_other']; } // used to record facility code in staging table only

				//gets provider ID from openmrs database
				include 'general/provider_id.php';	

				// match locations with that on openmrs
				// facilities
				include 'general/facilities_encounter.php';				

				// countries
				include 'general/countries.php';				

				// districts
				include 'general/districts.php';
				
				// check to see if this ptracker_id has an l&d record in the sync log
				// if an l&d record exists, do not create an encounter or sync log record THEN
				// skip to adding THIS infant's observations to existing encounter then proceed to create the infant only
				include 'labour/check_stag_encounter.php';
								
				//creates record in log table
				if ($found_stag_labour_encounter_uuid == 0) { include 'general/new_record.php';	}
				
				if ($new_record_created != 1) {
				
						echo '<br>'.$date.' Sync record not created.'; 
				
				} else  {
				 		
					    // check openmrs if patient with ptracker id exists
					    include 'general/check_ptrackerid.php';

					    // if patient with ptracker id exists, it should create
					    if ($found_openmrs_patient_identifier_uuid == null) { 
					    
						   	 	// create new person
						   	 	include 'general/new_person.php';
			
						   	 	if ($created_openmrs_person_uuid != null) {
						   	 	
									   	 	//generate openmrs id
									   	 	include 'general/idgen.php';
									   	 	
									   	 	// create patient
											include 'general/new_patient.php';
											
											// assign PTracker identifier to new person
											include 'general/assign_ptrackerid.php';
											
											//  add attributes to person
											// 	phone number
											if ($odk_phone_number != null) { include 'general/add_attribute_phone.php'; }
								
											// 	next of kin
											if ($odk_kin_name != null) { include 'general/add_attribute_kin.php'; }
												
											// 	next of kin phone number
											if ($odk_kin_contact != null) { include 'general/add_attribute_kin_phone.php'; }
												
											// 	addresses
											include 'general/add_addresses.php';
								}   
					    } else {
					    		// Set the patient uuid found in openmrs to be used for encounter
						    	$created_openmrs_patient_uuid = $found_openmrs_patient_identifier_uuid;	
						    	
						    	// Set the person uuid found in openmrs to be used for encounter
						    	$created_openmrs_person_uuid = $found_openmrs_person_identifier_uuid;
						    	
						    	echo '<br>'.$date.' <strong>Jump to</strong> Add Encounter';
					    } 
						if ($created_openmrs_patient_uuid != null) { 

									if ($found_stag_labour_encounter_uuid == 0) {  
									
													// create encounter
													include 'labour/new_encounter.php'; 
                                        
                                                    // Record observation - PTracker ID
											        if ($odk_ptrackerid != null) { include 'general/add_ptracker_id.php'; }
																		
													// Record observation - labour - pink book
													if (isset($odk_ld_has_pinkbook)) { include 'labour/new_obs_pinkbook.php'; }
													
													// Record observation - labour - mother status
													if (isset($odk_ld_motherstatus)) { include 'labour/new_obs_motherstatus.php'; }
													
													// Record observation - labour - discharge date 
													if (isset($odk_ld_discharge_date)) { include 'labour/new_obs_discharge_date.php'; }
									
													// Record observation - labour - number of infants
													if (isset($odk_ld_numberof_infants)) { include 'labour/new_obs_numberof_infants.php'; }

													// Record observation - hiv - test status $odk_anc_first_hiv_test_status
													if ($odk_anc_first_hiv_test_status != null) { include 'labour/new_obs_anc_first_hiv_test_status.php'; }
													
													// Record observation - hiv - test status $odk_hiv_retest_status
													if ($odk_hiv_test_status != null) { include 'hiv_status/new_obs_hiv_test_status.php'; }

													// Record observation - hiv - retest status at 36 weeks 
													if ($odk_hiv_retest_status != null) { include 'hiv_status/new_obs_hiv_retest_status.php'; }
													
													// Record observation - hiv - test result
													if ($odk_hiv_test_result != null) { include 'hiv_status/new_obs_hiv_test_result.php'; }
													
													// Record observation - hiv - art initiation status
													if ($odk_art_int_status != null) { include 'hiv_status/new_obs_art_int_status.php'; }

													// Record observation - hiv - anc art initiation status
													if ($odk_anc_art_initation != null) { include 'hiv_status/new_obs_anc_art_initation.php'; }
													
													// Record observation - hiv - art refusal reason
													if ($odk_art_int_refused_reason != null) { include 'hiv_status/new_obs_art_int_refused_reason.php'; }
                                        
                                                    // Record observation - hiv - missnig art refusal reason 
                                                    if ($odk_art_int_refused_reason_missing != null) { include 'hiv_status/new_obs_art_int_refused_reason_missing.php'; }	

                                                    // Assign identifier - hiv - art number 
													if ($odk_odk_art_number_missing != null) { include 'hiv_status/new_obs_art_number_missing.php'; }	else { include 'hiv_status/assign_artnumber.php'; }	

													// Record observation - hiv - art start date 
													if ($odk_art_start_date != null) { include 'hiv_status/new_obs_art_start_date.php'; }
													
													// Record observation - hiv - vl test done
													if ($odk_vl_test_done != null)  { include 'hiv_status/new_obs_vl_test_done.php'; }				
													
													// Record observation - hiv - vl test date
													if ($odk_vl_test_date != null)  { include 'hiv_status/new_obs_vl_test_date.php'; }
													
													// Record observation - hiv - vl test result
													if ($odk_vl_test_result != null) { include 'hiv_status/new_obs_vl_test_result.php'; }
													
													// Record observation - hiv - vl copies
													if ($odk_vl_test_result_value != null) { include 'hiv_status/new_obs_vl_copies.php'; }
									
											} else { echo '<br>'.$date.' Encounter not created.'; }
											
									// add infant l&d obervsations and create person (infant) in openmrs 
									
									// sql query to fetch data from infants view
									$sql_infant_ld = "SELECT * FROM vw_odk_delivery_infant WHERE _URI = '".$odk_odk_uuid."' LIMIT 9";  // loads all infant records for this ODK submission limited to 9 based on openmrs child numbers
									$result_infant_ld = $conn->query($sql_infant_ld);
									
									// start to loop through each record returned in query
									if ($result_infant_ld->num_rows > 0) {
									    		$rows_count_infant_ld=mysqli_num_rows($result_infant_ld); // count number of rows
												echo '<br>'.$date.' Found <strong>'.$rows_count_infant_ld.'</strong> infant records.';
												
												//number if infants and instance of this infant
												$infant_child_instance = $rows_count_infant_ld;
												
												while($row_infant_ld = $result_infant_ld->fetch_assoc()) {
		
															if ($odk_infant_ptracker_id != null) {
																		echo '<br>'.$date.' Processing infant number <strong>'.$infant_child_instance.'</strong> with ID - <strong>'.$odk_infant_ptracker_id.'</strong>'; 
															} else { 
																		echo '<br>'.$date.' Processing infant number <strong>'.$infant_child_instance.'</strong> '; 
															}
															
															// creating infant (person)
															$odk_infant_given = 'TBD';
															$odk_infant_middle = 'TBD';
															if ($odk_family == null) { $odk_family = 'TBD';}
															$odk_infant_family = $odk_family;
															$parent_person_uuid = $created_openmrs_person_uuid;
															//Infant L&D
															$odk_infant_ptracker_id = $row_infant_ld['infant_ptracker_id'];
															$odk_infant_status = $row_infant_ld['infant_status'];
															$odk_infant_arv_status = $row_infant_ld['infant_arv_status'];
															$odk_infant_arv_reason_for_refusal = $row_infant_ld['infant_arv_reason_for_refusal'];
															$odk_infant_arv_reason_for_refusal_missing = $row_infant_ld['infant_arv_reason_for_refusal_missing'];
															$odk_infant_dod = $row_infant_ld['infant_dod'];
															$odk_infant_dod_missing = $row_infant_ld['infant_dod_missing'];
															$odk_infant_feeding = $row_infant_ld['infant_feeding'];
															$odk_infant_stillbirth = $row_infant_ld['infant_stillbirth'];
															$odk_infant_dob = $row_infant_ld['infant_dob'];
															$odk_infant_sex = $row_infant_ld['infant_sex'];
															
															// Record observation - creates child group to record infant observations in L&D 
															include 'labour/new_child_group.php';
															
															//subtracts 1 from the number of infants
															$infant_child_instance = $infant_child_instance - 1;
																										
															//creates record in log table
															include 'general/new_record_infant.php';
															
															if ($odk_infant_status == '1') {
																	// create new person
															   	 	include 'general/new_person_infant.php';
															   	 	
															   	 	if ($created_openmrs_infant_uuid != null) {
															   	 	
																		   	 	//create new relationship
																		   	 	include 'general/new_relationship.php';
																		   	 	
																		   	 	//generate openmrs id
																		   	 	include 'general/idgen.php';
																		   	 	
																			 	// create patient
																				include 'general/new_patient_infant.php';
																				
																				// assign PTracker identifier to new person
																				if ($odk_infant_ptracker_id != null) { include 'general/assign_ptrackerid_infant.php'; }
																				
																	}
															}
												}
										}
					
						//deletes record in worklist table
						include 'general/delete_from_worklist.php';	
	
				} else { 
						//deletes record in log table labour_infant.php
						include 'general/delete_record.php';								
						
						die('<br>'.$date.' Stopping sync process until next attempt.') ; 
 }
		   }
    }
} else {
    echo '<br>'.$date.' <strong>No</strong> new records to process.';
}
echo '<br>'.$date.' End of run.';
// close connection
$conn->close();
?>