<?php
/*
version: 	1.0
author:		Carlos Sibalatani
date:		June 2021


___________________________________
change log
___________________________________
date:		author:			comment:
*/

// Report all errors except E_NOTICE
error_reporting(E_ALL & ~E_NOTICE);

// load boostrap styles
echo '
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
';

// specify date & timezone 
date_default_timezone_set('Africa/Windhoek');
$date          = date('Y/m/d H:i:s');

// connect to database & get openmrs logins
require_once('database/config.php');

//echo '<h4>PTracker ODK OpenMRS Sync</h4>';
echo '' . $date . ' Start of <strong>ANC</strong> run';

// sql query to fetch data from odk view
$sql = "select * from stag_ptracker_synclog a
where status LIKE 'Error adding obs%'
and openmrs_encounter_uuid is not NULL
and openmrs_patient_uuid is not null
and error_message is not null
and (ptracker_id, visit_date) not in (SELECT ptracker_id, visit_date from stag_ptracker_synclog b where status = 'added obs')";
$synclog = $conn->query($sql);

// start to loop through each record returned in query
if ($synclog->num_rows > 0) {
    $rows_count = mysqli_num_rows($synclog); // count number of rows

    echo '<br>' . $date . ' Found <strong>' . $rows_count . '</strong> records.';

    while ($row = $synclog->fetch_assoc()) {
        echo '<br>' . $date . ' Processing ID <strong>' . $row['ptracker_id'] . '</strong>';

        $status = $row['status'];
        $type = $row['type'];
        $odk_uuid = $row['odk_uuid'];
        $created_openmrs_person_uuid = $row['openmrs_person_uuid'];
        $created_openmrs_encounter_uuid = $row['openmrs_encounter_uuid'];
        $odk_visit_date_odk_recorded = $row['visit_date'];

        switch ($type) {
            case 'ANC':
                $odk_record = $conn->query("select * from stag_odk_anc where odk_uuid = '".$odk_uuid."' ");
                $odk_row = $odk_record->fetch_assoc();
                break;
            case 'Infant PNC':
                $odk_record = $conn->query("select * from stag_odk_pnc_infant where odk_uuid = '".$odk_uuid."' ");
                $odk_row = $odk_record->fetch_assoc();
                break;
            case 'L&D':
                $odk_record = $conn->query("select * from stag_odk_delivery where odk_uuid = '".$odk_uuid."' ");
                $odk_row = $odk_record->fetch_assoc();
                break;
            case 'Infant L&D':
                $odk_record = $conn->query("select * from stag_odk_delivery_infant where odk_uuid = '".$odk_uuid."' ");
                $odk_row = $odk_record->fetch_assoc();
                break;
            case 'PNC':
                $odk_record = $conn->query("select * from stag_odk_pnc where odk_uuid = '".$odk_uuid."' ");
                $odk_row = $odk_record->fetch_assoc();
                break;
            default:
                NULL;
                break;
        }

        $facility_uuid = $odk_row['facility_uuid'];
        if ($odk_row['facility_code'] != null) { $odk_facility = $odk_row['facility_code']; } else { $odk_facility = $odk_row['facility_code_other']; }
        include 'general/facilities_encounter.php';

        switch ($status) {
            case 'Error adding obs ART NUMBER - MISSING':
                include 'hiv_status/new_obs_art_number_missing.php';
                break;
            case 'Error adding obs ANC Next Visit Date - MISSING':
                include 'anc/new_obs_next_visit_date_missing.php';
                break;
            case 'Error adding obs Facility of next appointment':
                $odk_next_facility_to_visit = $odk_row['next_facility_to_visit'];
                include 'anc/new_obs_facility_of_next_appointment.php';
                break;
            case 'Error adding obs Infant PNC Event Date':
                $odk_infant_event_date = $odk_row["infant_event_date"];
                include 'infant_pnc/new_obs_infant_event_date.php';
                break;
            case 'Error adding obs Infant PNC Event Date - MISSING':
                include 'infant_pnc/new_obs_infant_event_date_missing.php';
                break;
            case 'Error adding obs Infant PNC Next Visit Date':
                $odk_infant_next_visit_date = $odk_row["infant_next_visit_date"];
                include 'infant_pnc/new_obs_next_visit_date.php';
                break;
            case 'Error adding obs Infant PNC Next Visit Date - MISSING':
                include 'infant_pnc/new_obs_next_visit_date_missing.php';
                break;
            case 'Error adding obs Next Facility Transferred':
                $odk_next_facility_to_visit_transfered = $odk_row['next_facility_to_visit_transfered'];
                include 'pnc/new_obs_facility_transfer.php';
                break;
            case 'Error adding obs Transfer To':
                $odk_next_facility_to_visit_transfered = $odk_row['next_facility_to_visit_transfered'];
                include 'anc/new_obs_facility_of_next_appointment_transfer.php';
                break;
            default:
                NULL;
                break;
        }
        $sql_create= "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', error_message = NULL WHERE odk_uuid = '".$odk_uuid."'";

        if ($conn->query($sql_create) === TRUE) {
            echo "<br>".$date." Sync log record <strong>".$sync_log_id."</strong> has been resubmitted observation";
            $new_record_created = -1;
        } else {
            echo "<br>".$date." Error removing sync log record: " . $sql_create . "<br>" . $conn->error;
            die('<br><span style="color:red"><strong>A critical error occurred.</strong></span>');

        }
    }
} else {

    echo '<br>' . $date . ' <strong>No</strong> new records to process.';
}

echo '<br>' . $date . ' End of run.';
// close connection
$conn->close();