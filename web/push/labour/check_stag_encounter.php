<?php
/*
version: 	1.0
author:		Asen Mwandemele
date:		January 2018

FIND PATIENT ENCOUNTER UUID FOR LABOUR & DELIVERY USING PTRACKER ID
___________________________________
change log
___________________________________
date:		author:			comment:
16/01/2018	Asen			Changed the logic from ptracker id and visit type to ODK URI

*/

$identifier_to_check = "L&D Encounters";
echo "<br>".$date." Checking <em>".$identifier_to_check."</em> for _URI <em>".$odk_odk_uuid."</em>";

$sql_labour_encounter = "SELECT * FROM stag_ptracker_synclog WHERE odk_uuid = '".$odk_odk_uuid."'";
$result_sql_labour_encounter = $conn->query($sql_labour_encounter);
$row_labour_encounter = $result_sql_labour_encounter->fetch_assoc();

if (isset($row_labour_encounter["openmrs_encounter_uuid"])) { 
	
		$found_stag_labour_encounter_uuid = $row_labour_encounter["openmrs_encounter_uuid"];
		echo "<br>".$date." Found encounter - <em>".$found_stag_labour_encounter_uuid."</em>";
		$new_record_created = 1;

} else { 

		$found_stag_labour_encounter_uuid = 0;

}



?>