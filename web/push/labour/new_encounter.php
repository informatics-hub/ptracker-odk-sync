<?php
/*
version: 	1.2
author:		Asen Mwandemele
date:		December 2017

labour & delivery encounter
___________________________________
change log
___________________________________
date:		author:			comment:
13/12/2017	Asen			Added "form" type to encoutner payload
09/01/2018	Asen			Added fix for encounter provider informations
16/08/2018  Asen            Removed white spaces

*/ 
    
$encounter_data      	= array();
$encounterProviders  	= array();
$Provider			  	= array();

$encounterProviders['provider']      = $odk_provider_id;
$encounterProviders['encounterRole'] = "a0b03050-c99b-11e0-9572-0800200c9a66";

array_push($Provider, $encounterProviders);

$encounter_data['patient']	            = $created_openmrs_patient_uuid;   
$encounter_data['encounterDatetime']	= $odk_visit_date; 
$encounter_data['encounterType']	    = "2678423c-0523-4d76-b0da-18177b439eed";  // Labour & Delivery
$encounter_data['location']	            = $odk_location_facility_encounter;
$encounter_data['form']	                = "1e5614d6-5306-11e6-beb8-9e71128cae77"; // L&D Form
$encounter_data['encounterProviders']	= $Provider;

$encounter_data = json_encode($encounter_data);
  
$encounter_curL = curl_init(''.$openmrs_url.'/ws/rest/v1/encounter');
curl_setopt($encounter_curL, CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
curl_setopt($encounter_curL, CURLOPT_USERPWD,''.$openmrs_logins.'');
curl_setopt($encounter_curL, CURLOPT_RETURNTRANSFER,true);
curl_setopt($encounter_curL, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); 
curl_setopt($encounter_curL, CURLOPT_POST,1);
curl_setopt($encounter_curL, CURLOPT_POSTFIELDS,$encounter_data);
$encounter_output   = curl_exec($encounter_curL);
$encounter_status   = curl_getinfo($encounter_curL,CURLINFO_HTTP_CODE);
curl_close($encounter_curL);

// get json response
$encounter_response = json_decode($encounter_output);

//echo $encounter_output;

// display error message or output uuid of person created
$encounter_create_error = $encounter_response->error->message;

if (isset($encounter_response->error->message)) { 

	echo "<br>".$date." Error: ".$encounter_response->error->message.""; 
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Error Creating Encounter', type = 'L&D', http_status_code = '".$encounter_status."', error_message = '".$encounter_create_error."' WHERE id = '".$sync_log_id."'";
	echo "<br>".$date." Error Creating Encounter";
	
} else {
	$created_openmrs_encounter_uuid = $encounter_response->uuid;
	echo "<br>".$date." Created Encounter with UUID: ".$created_openmrs_encounter_uuid."";
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', openmrs_encounter_uuid = '".$created_openmrs_encounter_uuid."', type = 'L&D', status = 'Created Encounter with UUID: ".$created_openmrs_encounter_uuid."', http_status_code = '".$encounter_status."', error_message = '".$encounter_create_error."' WHERE id = '".$sync_log_id."'";

}
 
if ($conn->query($sql_update) === TRUE) {
   // echo "<br>".$date." Updated Sync Log Record ".$created_openmrs_encounter_uuid."";
} else {
    echo "Error: " . $sql_update . "<br>" . $conn->error;
}

		
?>