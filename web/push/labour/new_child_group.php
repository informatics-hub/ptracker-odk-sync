<?php
/*
version: 	1.1
author:		Asen Mwandemele
date:		January 2018

add observation new child group
___________________________________
change log
___________________________________
date:		author:			comment:
24/01/2018	Asen			changed still birth value as recorded in openmrs source form
							changed ifnant arv values as recorded in openmrs source form
16/01/2018	Asen			Added reason for refusal or ARV
10/12/2018  Asen            Added "Infant received NVP + AZT Prophylaxis up to 6 weeks (164956)"

*/ 		
$obs_to_add = "New Child Group";

$obs_data		                      = array(); 		
$infantObservations  			      = array();
$Provider			  		          = array();
$infant_data 	                      = array(); 
$infant_status_data                   = array();
$infant_stillbirth_data			      = array(); 
$infant_dob_data                      = array(); 
$infant_dod_data                      = array(); 
$infant_sex_data                      = array(); 
$infant_feeding_data			      = array(); 
$infant_arv_status_data			      = array(); 
$infant_arv_refusal_data_missing      = array(); 
$infant_arv_refusal_data			  = array(); 
$infant_ptrackerid_data			      = array(); 
$infant_mrn_data			          = array();

// infant status
if($odk_infant_status != null){
switch ($odk_infant_status) {
    case "1":
    	// 1 Alive (151849) = 151849AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
        $infant_coded_value = "151849AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
        break;
    case "2":
    	// 2 Infant died (154223) = 154223AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
        $infant_coded_value = "154223AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; 
        break;
	case "3":
    	// 3 still birth (125872) =  125872AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA  
    	$infant_coded_value = "125872AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";  
        break;
    case "66":
    	// 66 = Missing (164932) = 54b96458-6585-4c4c-a5b1-b3ca7f1be351
    	$infant_coded_value = "54b96458-6585-4c4c-a5b1-b3ca7f1be351"; 
  		break;
    default:
    	// code not recognized	
        $infant_coded_value = "54b96458-6585-4c4c-a5b1-b3ca7f1be351"; 
        echo "<br>".$date." Unknown obs code, setting ".$infant_to_add." to missing";
}

$infant_status_data["person"]		  = $created_openmrs_person_uuid; // person uuid 
$infant_status_data["encounter"]      = $created_openmrs_encounter_uuid; // encounter uuid
$infant_status_data["obsDatetime"]    = $odk_visit_date_odk_recorded; //time of visit?
$infant_status_data["concept"]        = "159917AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";  // concept uuid
$infant_status_data["value"]   		  = $infant_coded_value; // value of the observation 
$infant_status_data['location']		  = $odk_location_facility_encounter;	

}

array_push($infantObservations, $infant_status_data);


// still birth
if($odk_infant_stillbirth != null){
switch ($odk_infant_stillbirth) {
    case "1":
    	// 1 fresh (159916) 
        $infant_stillbirth_value = "159916AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; 
        break;
    case "2":
    	// 2 macerated  (135436) 
        $infant_stillbirth_value = "135436AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
        break;
    case "66":
    	// 66 = Missing (164932) = 54b96458-6585-4c4c-a5b1-b3ca7f1be351
    	$infant_stillbirth_value = "54b96458-6585-4c4c-a5b1-b3ca7f1be351"; 
  		break;
    default:
    	// code not recognized	
        $infant_stillbirth_value = "54b96458-6585-4c4c-a5b1-b3ca7f1be351"; 
        echo "<br>".$date." Unknown obs code, setting still birth to missing";
}

$infant_stillbirth_data["person"]		  = $created_openmrs_person_uuid; // person uuid 
$infant_stillbirth_data["encounter"]      = $created_openmrs_encounter_uuid; // encounter uuid
$infant_stillbirth_data["obsDatetime"]    = $odk_visit_date_odk_recorded; //time of visit?
$infant_stillbirth_data["concept"]        = "125872AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";  // concept uuid
$infant_stillbirth_data["value"]   		  = $infant_stillbirth_value; // value of the observation 
$infant_stillbirth_data['location']		  = $odk_location_facility;	

array_push($infantObservations, $infant_stillbirth_data);

}

// infant reason for refusing ARV
if($odk_infant_arv_reason_for_refusal != null){
$infant_arv_refusal_data["person"]			= $created_openmrs_person_uuid; // person uuid 
$infant_arv_refusal_data["encounter"]      	= $created_openmrs_encounter_uuid; // encounter uuid
$infant_arv_refusal_data["obsDatetime"]    	= $odk_visit_date_odk_recorded; //time of visit?
$infant_arv_refusal_data["concept"]        	= "163322AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";  // concept uuid
$infant_arv_refusal_data["value"]   	   	= $odk_infant_arv_reason_for_refusal; // value of the observation 
$infant_arv_refusal_data['location']		= $odk_location_facility;	

array_push($infantObservations, $infant_arv_refusal_data);

}

// infant reason for refusing ARV - MISSNIG
if($odk_infant_arv_reason_for_refusal_missing != null){
$infant_arv_refusal_data_missing["person"]			= $created_openmrs_person_uuid; // person uuid 
$infant_arv_refusal_data_missing["encounter"]      = $created_openmrs_encounter_uuid; // encounter uuid
$infant_arv_refusal_data_missing["obsDatetime"]    = $odk_visit_date_odk_recorded; //time of visit?
$infant_arv_refusal_data_missing["concept"]        = "fd85eb5c-a8fc-4fa5-bd2f-5b4dab355978";  // concept uuid
$infant_arv_refusal_data_missing["value"]   	   = 'true'; // value of the observation  MISSING
$infant_arv_refusal_data_missing['location']		= $odk_location_facility;	

array_push($infantObservations, $infant_arv_refusal_data_missing);

}

// infant medical record number (Ptracker ID)
if($odk_infant_ptracker_id != null){
$infant_mrn_data["person"]			= $created_openmrs_person_uuid; // person uuid 
$infant_mrn_data["encounter"]      	= $created_openmrs_encounter_uuid; // encounter uuid
$infant_mrn_data["obsDatetime"]    	= $odk_visit_date_odk_recorded; //time of visit?
$infant_mrn_data["concept"]        	= "164803AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";  // concept uuid
$infant_mrn_data["value"]   	   	= $odk_infant_ptracker_id; // value of the observation 
$infant_mrn_data['location']		= $odk_location_facility;	

array_push($infantObservations, $infant_mrn_data);

}

// infant date of birth
if($odk_infant_dob != null){
$infant_dob_data["person"]			= $created_openmrs_person_uuid; // person uuid 
$infant_dob_data["encounter"]      = $created_openmrs_encounter_uuid; // encounter uuid
$infant_dob_data["obsDatetime"]    = $odk_visit_date_odk_recorded; //time of visit?
$infant_dob_data["concept"]        = "164802AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";  // concept uuid
$infant_dob_data["value"]   	   = $odk_infant_dob; // value of the observation 
$infant_dob_data['location']			= $odk_location_facility;	

array_push($infantObservations, $infant_dob_data);

} 

// infant date of death
if($odk_infant_dod != null){
$infant_dod_data["person"]			= $created_openmrs_person_uuid; // person uuid 
$infant_dod_data["encounter"]      = $created_openmrs_encounter_uuid; // encounter uuid
$infant_dod_data["obsDatetime"]    = $odk_visit_date_odk_recorded; //time of visit?
$infant_dod_data["concept"]        = "1543AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";  // concept uuid
$infant_dod_data["value"]   	   = $odk_infant_dod; // value of the observation 
$infant_dod_data['location']			= $odk_location_facility;	

array_push($infantObservations, $infant_dod_data);

} 

// infant date of death missing
if ($odk_infant_dod_missing != null){ 	
$infant_dod_data["person"]			= $created_openmrs_person_uuid; // person uuid 
$infant_dod_data["encounter"]      = $created_openmrs_encounter_uuid; // encounter uuid
$infant_dod_data["obsDatetime"]    = $odk_visit_date_odk_recorded; //time of visit?
$infant_dod_data["concept"]        = "38b6933e-b6ec-4213-8784-987920f5e49c";  // concept uuid
$infant_dod_data["value"]   	   = 'true'; // value of the observation  MISSING
$infant_dod_data['location']		= $odk_location_facility;	


array_push($infantObservations, $infant_dod_data);

}


// infant sex 
if($odk_infant_sex != null){
switch ($odk_infant_sex) {
    case "M":
    	// M  (1534) 
        $infant_sex_value = "1534AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; 
        break;
    case "F":
    	// F  (1535) 
        $infant_sex_value = "1535AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
        break;
    case "66":
    	// 66 = Missing (164932) = 54b96458-6585-4c4c-a5b1-b3ca7f1be351
    	$infant_sex_value = "54b96458-6585-4c4c-a5b1-b3ca7f1be351"; 
  		break;
    default:
    	// code not recognized	
        $infant_sex_value = "54b96458-6585-4c4c-a5b1-b3ca7f1be351"; 
        echo "<br>".$date." Unknown obs code, setting Infant Sex to missing";
}

$infant_sex_data["person"]			= $created_openmrs_person_uuid; // person uuid 
$infant_sex_data["encounter"]       = $created_openmrs_encounter_uuid; // encounter uuid
$infant_sex_data["obsDatetime"]    	= $odk_visit_date_odk_recorded; //time of visit?
$infant_sex_data["concept"]        	= "1587AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";  // concept uuid
$infant_sex_data["value"]   		= $infant_sex_value; // value of the observation 
$infant_sex_data['location']		= $odk_location_facility;	

array_push($infantObservations, $infant_sex_data);
}

// infant feeding
if($odk_infant_feeding != null){
switch ($odk_infant_feeding) {
    case "1":
    	// EBF  (5526) 
        $infant_feeding_value = "5526AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; 
        break;
    case "2":
    	// RF  (1595) 
        $infant_feeding_value = "1595AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
        break;
    case "66":
    	// 66 = Missing (164932) = 54b96458-6585-4c4c-a5b1-b3ca7f1be351
    	$infant_feeding_value = "54b96458-6585-4c4c-a5b1-b3ca7f1be351"; 
  		break;
    default:
    	// code not recognized	
        $infant_feeding_value = "54b96458-6585-4c4c-a5b1-b3ca7f1be351"; 
        echo "<br>".$date." Unknown obs code, setting Infant Sex to missing";
}
$infant_feeding_data["person"]			= $created_openmrs_person_uuid; // person uuid 
$infant_feeding_data["encounter"]      = $created_openmrs_encounter_uuid; // encounter uuid
$infant_feeding_data["obsDatetime"]    	= $odk_visit_date_odk_recorded; //time of visit?
$infant_feeding_data["concept"]        	= "1151AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";  // concept uuid
$infant_feeding_data["value"]   		= $infant_feeding_value; // value of the observation 
$infant_feeding_data['location']			= $odk_location_facility;	


array_push($infantObservations, $infant_feeding_data);

}

// Infant Received ARV
if($odk_infant_arv_status != null){
switch ($odk_infant_arv_status) {
    case "1":
    	// ARV Prophylaxis daily up to 6 weeks  (164918) 
        $infant_arv_status_value = "83a60fef-31c6-4937-907b-42ced15474e3"; 
        break;
    case "2":
    	// Refused ARV Prophylaxis  (127750) 
        $infant_arv_status_value = "127750AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
        break;
    case "3":
    	// Stock-out of ARV Prophylaxis  (1754) 
        $infant_arv_status_value = "1754AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
        break;
    case "4":
    	// Infant received NVP + AZT Prophylaxis up to 6 weeks (164956) 
        $infant_arv_status_value = "99b29c50-fc67-11e8-8eb2-f2801f1b9fd1";
        break;
    case "66":
    	// 66 = Missing (164932) = 54b96458-6585-4c4c-a5b1-b3ca7f1be351
    	$infant_arv_status_value = "54b96458-6585-4c4c-a5b1-b3ca7f1be351"; 
  		break;
    default:
    	// code not recognized	
        $infant_arv_status_value = "54b96458-6585-4c4c-a5b1-b3ca7f1be351"; 
        echo "<br>".$date." Unknown obs code, setting Infant Sex to missing";
}
$infant_arv_status_data["person"]			= $created_openmrs_person_uuid; // person uuid 
$infant_arv_status_data["encounter"]      	= $created_openmrs_encounter_uuid; // encounter uuid
$infant_arv_status_data["obsDatetime"]    	= $odk_visit_date_odk_recorded; //time of visit?
$infant_arv_status_data["concept"]        	= "1148AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";  // concept uuid
$infant_arv_status_data['location']			= $odk_location_facility;	
$infant_arv_status_data["value"]   			= $infant_arv_status_value; // value of the observation 

array_push($infantObservations, $infant_arv_status_data);

}

switch ($infant_child_instance) {
    case "1":
		$infantInstance = "8fe7ad7a-494d-4799-bf72-9f58fbdae221"; // child 1
        break;
    case "2":
		$infantInstance = "8fe7ad7a-494d-4799-bf72-9f58fbdae222"; // child 2
        break;
    case "3":
		$infantInstance = "8fe7ad7a-494d-4799-bf72-9f58fbdae223"; // child 3
        break;
    case "4":
		$infantInstance = "8fe7ad7a-494d-4799-bf72-9f58fbdae224"; // child 4
        break;
    case "5":
		$infantInstance = "8fe7ad7a-494d-4799-bf72-9f58fbdae225"; // child 5
        break;
    case "6":
		$infantInstance = "8fe7ad7a-494d-4799-bf72-9f58fbdae226"; // child 6
        break;
    case "7":
		$infantInstance = "8fe7ad7a-494d-4799-bf72-9f58fbdae227"; // child 7
        break;
    case "8":
		$infantInstance = "8fe7ad7a-494d-4799-bf72-9f58fbdae228"; // child 8
        break;
    case "9":
		$infantInstance = "8fe7ad7a-494d-4799-bf72-9f58fbdae229"; // child 9
        break;
    default:
    	// code not recognized	
        die("Unknown number of this child.");
}

$obs_data['person']			= $created_openmrs_person_uuid; // person uuid 
$obs_data['obsDatetime']    = $odk_visit_date_odk_recorded; //time of visit?
$obs_data['encounter']      = $created_openmrs_encounter_uuid; // encounter uuid
$obs_data['concept']	   	= $infantInstance;  // concept uuid
$obs_data['location']		= $odk_location_facility;
$obs_data['groupMembers']  	= $infantObservations;  // concept uuid

$obs_data = json_encode($obs_data);

//echo  $obs_data;
  
$obs_curL = curl_init(''.$openmrs_url.'/ws/rest/v1/obs');
curl_setopt($obs_curL, CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
curl_setopt($obs_curL, CURLOPT_USERPWD,''.$openmrs_logins.'');
curl_setopt($obs_curL, CURLOPT_RETURNTRANSFER,true);
curl_setopt($obs_curL, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); 
curl_setopt($obs_curL, CURLOPT_POST,1);
curl_setopt($obs_curL, CURLOPT_POSTFIELDS,$obs_data);
$obs_output   = curl_exec($obs_curL);
$obs_status   = curl_getinfo($obs_curL,CURLINFO_HTTP_CODE);
curl_close($obs_curL);

// get json response
$obs_response = json_decode($obs_output);

//echo $obs_output;

// display error message or output uuid of person created
$obs_create_error_first = $obs_response->error->message;

// remove inverted commas for MySQl insert
$obs_create_error = str_replace("'"," ",$obs_create_error_first); 

if (isset($obs_response->error->message)) { 

	echo "<br>".$date." Error: ".$obs_response->error->message.""; 
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Error adding obs ".$obs_to_add."', http_status_code = '".$obs_status."', error_message = '".$obs_create_error."' WHERE id = '".$sync_log_id."'";
	echo "<br>".$date." Error adding obs <em>".$obs_to_add."</em>";
	
} else {
	$created_openmrs_obs_uuid = $obs_response->uuid;
	echo "<br>".$date." Added obs <em>".$obs_to_add."</em> - '".$created_openmrs_obs_uuid."'";
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Added obs', openmrs_obss_added = '".$obs_to_add."', http_status_code = '".$obs_status."', error_message = '".$obs_create_error."' WHERE id = '".$sync_log_id."'";

}
 
if ($conn->query($sql_update) === TRUE) {
    //echo "<br>".$date." Updated Sync Log Record ".$created_openmrs_obs_uuid."";
} else {
    echo "Error: " . $sql_update . "<br>" . $conn->error;
}


?>