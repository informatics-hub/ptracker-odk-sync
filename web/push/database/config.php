<?php
/*
version: 	1.2
author:		Asen Mwandemele
date:		November 2017

database connection and openmrs global variables
___________________________________
change log
___________________________________
date:		author:			comment:
10/01/2018	Asen			Removed single quotes
*/ 

// database connection
$servername = 'localhost';
$username = 'root';
$password = 'root@ucsf';
$dbname = 'ptracker_odk';

//needed to connect to openmrs database
$username2 = 'root';
$password2 = 'root@ucsf';
$dbname2 = 'openmrs_test';


// create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// create connection
$conn3 = new mysqli($servername, $username2, $password2, $dbname2);

// check connection
if ($conn->connect_error) {
    die('Connection failed: ' . $conn->connect_error);
}


if ($conn3->connect_error) {
    die('Connection failed: ' . $con3->connect_error);
}

	
//openmrs login details
//$openmrs_logins = 'asenm:Asenm123';

$openmrs_logins = 'odkuat:Testing2018';

$openmrs_username = 'odkuat';

$openmrs_password = 'Testing2018';

$openmrs_url= 'https://ptracker-test.globalhealthapp.net/ptrackertest';
            
?>