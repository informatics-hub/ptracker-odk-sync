<?php
// source: https://gist.github.com/alexstone/9319715
// (string) $message - message to be passed to Slack
// (string) $room - room in which to write the message, too
// (string) $icon - You can set up custom emoji icons to use with each message
        $room = "ptracker-uptimes";
        $icon = ":longbox:"; 
        $room = ($room) ? $room : "ptracker-uptimes";
        $data = "payload=" . json_encode(array(
                "channel"       =>  "#{$room}",
                "text"          =>  $message,
                "icon_emoji"    =>  $icon
            ));
	
	// You can get your webhook endpoint from your Slack settings
        $ch = curl_init("https://hooks.slack.com/services/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");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
	
        // Laravel-specific log writing method
    // Log::info("Sent to Slack: " . $message, array('context' => 'Notifications'));

    // return $result;

?>