<?php
/*
version: 	1.0
author:		Asen Mwandemele
date:		January 2018

new patient
___________________________________
change log
___________________________________
date:		author:			comment:

*/ 			    

$identifiers_infant           = array();
$patient_infant_data          = array();
$person_identification_infant = array();

$identifiers_infant['identifier']     = $generated_openmrs_id; // Generated OpenMRS ID
$identifiers_infant['identifierType'] = "05a29f94-c0ed-11e2-94be-8c13b969e334"; // uuid of OpenMRS ID
$identifiers_infant['location']       = $odk_location_facility_encounter; 
 
// combine 
array_push($person_identification_infant,$identifiers_infant);

$patient_infant_data['person']      = $created_openmrs_infant_uuid; // Newly created person's uuid  
$patient_infant_data['identifiers'] = $person_identification_infant;

$patient_infant_data = json_encode($patient_infant_data);
  
$patient_infant_curL = curl_init(''.$openmrs_url.'/ws/rest/v1/patient');
curl_setopt($patient_infant_curL, CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
curl_setopt($patient_infant_curL, CURLOPT_USERPWD,''.$openmrs_logins.'');
curl_setopt($patient_infant_curL, CURLOPT_RETURNTRANSFER,true);
curl_setopt($patient_infant_curL, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); 
curl_setopt($patient_infant_curL, CURLOPT_POST,1);
curl_setopt($patient_infant_curL, CURLOPT_POSTFIELDS,$patient_infant_data);
$patient_infant_output   = curl_exec($patient_infant_curL);
$patient_infant_status   = curl_getinfo($patient_infant_curL,CURLINFO_HTTP_CODE);
curl_close($patient_infant_curL);

// get json response
$patient_infant_response = json_decode($patient_infant_output);

// display error message or output uuid of person created
$patient_infant_create_error = $patient_infant_response->error->message;

if (isset($patient_infant_response->error->message)) { 

	echo "<br>".$date." Error: ".$patient_infant_response->error->message."";  
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Error Creating  Infant Patient ', http_status_code = '".$patient_infant_status."', error_message = '".$patient_infant_create_error."' WHERE id = '".$sync_log_id."'";
	echo "<br>".$date." Error Creating  Infant Patient ";
	
} else {
	$created_openmrs_patient_infant_uuid = $patient_infant_response->uuid;
	echo "<br>".$date." Created  Infant Patient  with UUID: ".$created_openmrs_patient_uuid."";
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', openmrs_patient_uuid = '".$created_openmrs_patient_infant_uuid."', status = 'Created Infant Patient with UUID: ".$created_openmrs_patient_infant_uuid."', http_status_code = '".$patient_infant_status."', error_message = '".$patient_infant_create_error."' WHERE id = '".$sync_log_id."'";

}
 
if ($conn->query($sql_update) === TRUE) {
    //echo "<br>".$date." Updated Sync Log Record '".$created_openmrs_patient_uuid."'.";
} else {
    echo "Error: " . $sql_update . "<br>" . $conn->error;
}

?>