<?php
/*
version: 	1.0
author:		Asen Mwandemele
date:		November 2017

add person/patient attributes
___________________________________
change log
___________________________________
date:		author:			comment:

*/ 			    
$attribute_to_add = "next of kin phone number";

$attribute_data = array(); 

$attribute_data['value'] = $odk_kin_contact;
$attribute_data['attributeType'] = "d135bf7c-6ddd-4636-be22-fbd334a8f134"; // next of kin phone number"  
     
$attribute_data = json_encode($attribute_data);
  
$attribute_curL = curl_init(''.$openmrs_url.'/ws/rest/v1/person/'.$created_openmrs_person_uuid.'/attribute');
curl_setopt($attribute_curL, CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
curl_setopt($attribute_curL, CURLOPT_USERPWD,''.$openmrs_logins.'');
curl_setopt($attribute_curL, CURLOPT_RETURNTRANSFER,true);
curl_setopt($attribute_curL, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); 
curl_setopt($attribute_curL, CURLOPT_POST,1);
curl_setopt($attribute_curL, CURLOPT_POSTFIELDS,$attribute_data);
$attribute_output   = curl_exec($attribute_curL);
$attribute_status   = curl_getinfo($attribute_curL,CURLINFO_HTTP_CODE);
curl_close($attribute_curL);

// get json response
$attribute_response = json_decode($attribute_output);

//echo $attribute_output;

// display error message or output uuid of person created
$attribute_create_error = $attribute_response->error->message;

if (isset($attribute_response->error->message)) { 

	echo "<br>".$date." Error: ".$attribute_response->error->message.""; 
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Error adding attribute ".$attribute_to_add."', http_status_code = '".$attribute_status."', error_message = '".$attribute_create_error."' WHERE id = '".$sync_log_id."'";
	echo "<br>".$date." Error adding attribute <em>".$attribute_to_add."</em>";
	
} else {
	$created_openmrs_attribute_uuid = $attribute_response->uuid;
	echo "<br>".$date." Added attribute <em>".$attribute_to_add."</em> - '".$created_openmrs_attribute_uuid."'";
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Added attribute', openmrs_attributes_added = '".$attribute_to_add."', http_status_code = '".$attribute_status."', error_message = '".$attribute_create_error."' WHERE id = '".$sync_log_id."'";

}
 
if ($conn->query($sql_update) === TRUE) {
    //echo "<br>".$date." Updated Sync Log Record ".$created_openmrs_attribute_uuid."";
} else {
    echo "Error: " . $sql_update . "<br>" . $conn->error;
}




?>