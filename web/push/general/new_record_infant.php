<?php
/*
version: 	1.0
author:		Asen Mwandemele
date:		January 2018

create new record in sync log table
___________________________________
change log
___________________________________
date:		author:			comment:

*/      		
			   
$sql_create= "INSERT INTO stag_ptracker_synclog (ptracker_id, parent_ptracker_id, parent_openmrs_person_uuid, date_created, facility, status, odk_uuid, visit_date, type) VALUES ('".$odk_infant_ptracker_id."', '".$odk_ptrackerid."', '".$created_openmrs_person_uuid."', '".$date."', '".$odk_facility."', 'New sync record', '".$odk_odk_uuid."', '".$odk_visit_date."', 'Infant L&D')";


if ($conn->query($sql_create) === TRUE) {
	$sync_log_id = $conn->insert_id;
    echo "<br>".$date." Created sync log record.";
    $new_record_created = 1;
} else {
    echo "<br>".$date." Error creating sync log record: " . $sql_create . "<br>" . $conn->error;
    die('<br><span style="color:red"><strong>A critical error occurred.</strong></span>');

}

	

?>