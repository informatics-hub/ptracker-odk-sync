<?php
/*
version: 	1.1
author:		Asen Mwandemele
date:		November 2017

add person/patient addresss
___________________________________
change log
___________________________________
date:		author:			comment:
11/01/2018	Asen			Changes to country field and if statement for when fields are empty

*/ 			    
$address_to_add = " info";
$address_data = array(); 

if ($odk_address != '') { $address_data['address1']    = $odk_address; }   // address
if ($odk_address_locaiton != '') { $address_data['address2']    = $odk_address_locaiton; }   // location
if ($odk_country != null) { $address_data['country'] = $odk_country; }
if ($odk_country_district != null) { $address_data['countyDistrict'] = $odk_country_district; }
     
$address_data = json_encode($address_data);
  
$address_curL = curl_init(''.$openmrs_url.'/ws/rest/v1/person/'.$created_openmrs_person_uuid.'/address');
curl_setopt($address_curL, CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
curl_setopt($address_curL, CURLOPT_USERPWD,''.$openmrs_logins.'');
curl_setopt($address_curL, CURLOPT_RETURNTRANSFER,true);
curl_setopt($address_curL, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); 
curl_setopt($address_curL, CURLOPT_POST,1);
curl_setopt($address_curL, CURLOPT_POSTFIELDS,$address_data);
$address_output   = curl_exec($address_curL);
$address_status   = curl_getinfo($address_curL,CURLINFO_HTTP_CODE);
curl_close($address_curL);

// get json response
$address_response = json_decode($address_output);

//echo $address_output;

// display error message or output uuid of person created
$address_create_error = $address_response->error->message;

if (isset($address_response->error->message)) { 

	echo "<br>".$date." Error: ".$address_response->error->message.""; 
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Error adding address ".$address_to_add."', openmrs_address_added = 'FALSE', http_status_code = '".$address_status."', error_message = '".$address_create_error."' WHERE id = '".$sync_log_id."'";
	echo "<br>".$date." Error adding address <em>".$address_to_add."</em>";
	
} else {
	$created_openmrs_address_uuid = $address_response->uuid;
	echo "<br>".$date." Added address <em>".$address_to_add."</em> - '".$created_openmrs_address_uuid."'";
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Added address', openmrs_address_added = 'TRUE', http_status_code = '".$address_status."', error_message = '".$address_create_error."' WHERE id = '".$sync_log_id."'";

}
 
if ($conn->query($sql_update) === TRUE) {
    //echo "<br>".$date." Updated Sync Log Record ".$created_openmrs_address_uuid."";
} else {
    echo "Error: " . $sql_update . "<br>" . $conn->error;
}




?>