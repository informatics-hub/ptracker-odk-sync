<?php
/*
version: 	1.4
author:		Asen Mwandemele
date:		November 2017

new person
___________________________________
change log
___________________________________
date:		author:			comment:
12/12/2017	Asen			removed type from insert statement
10/01/2018	Asen			removed indenting spaces
10/01/2018	Asen			removed sync log creation query and replaced with update quey 
11/01/2018	Asen			removed dob and added age which is calculated for all patients in odk
11/01/2018	Asen			Set age as secondary to dob

*/      		
			   
$name        = array();
$names       = array();
$person_data = array(); 

$name['givenName']  = $odk_given;
if(isset($odk_middle)) { $name['middleName'] = $odk_middle;}
$name['familyName'] = $odk_family;

array_push($names,$name); 

if($odk_dob != null ) { $person_data['birthdate'] = $odk_dob; } else { $person_data['age'] = $odk_age; }
$person_data['gender']    = $odk_sex;   
$person_data['names'] = $names;
     
$person_data = json_encode($person_data);

$person_curL = curl_init(''.$openmrs_url.'/ws/rest/v1/person');
curl_setopt($person_curL, CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
curl_setopt($person_curL, CURLOPT_USERPWD,''.$openmrs_logins.'');
curl_setopt($person_curL, CURLOPT_RETURNTRANSFER,true);
curl_setopt($person_curL, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); 
curl_setopt($person_curL, CURLOPT_POST,1);
curl_setopt($person_curL, CURLOPT_POSTFIELDS,$person_data);
$person_output   = curl_exec($person_curL);
$person_status   = curl_getinfo($person_curL,CURLINFO_HTTP_CODE);
curl_close($person_curL);

// get json response
$person_response = json_decode($person_output);

echo $person_data;

echo $person_output;

// display error message or output uuid of person created
$person_create_error = $person_response->error->message;

if (isset($person_response->error->message)) { 

	$created_openmrs_person_uuid = null;
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Error creating person', http_status_code = '".$person_status."', error_message = '".$person_create_error."' WHERE id = '".$sync_log_id."'";
	echo "<br>".$date." Error Creating Person: <br>".$person_create_error."<br> ";
	
} else {
	$created_openmrs_person_uuid = $person_response->uuid;
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Person created successfully', http_status_code = '".$person_status."', openmrs_person_uuid = '".$created_openmrs_person_uuid."', error_message = '".$person_create_error."' WHERE id = '".$sync_log_id."'";
	echo "<br>".$date." Created person with UUID: ".$created_openmrs_person_uuid."";
	
} 

if ($conn->query($sql_update) === TRUE) {

} else {
    echo "Error: " . $sql_create . "<br>" . $conn->error;
}

?>