<?php
/*
version: 	1.4
author:		Asen Mwandemele
date:		January 2018

FIND PATIENT USING PTRACKER ID
___________________________________
change log
___________________________________
date:		author:			comment:
11/01/2018	Asen			Set age as secondary to dob
23/02/2018	Asen			Fixed issue where sync picked first ptracker ID in OpenMRS response.
27/02/2018	Asen			Changed emoji and modified slack notification
10/07/2018	Asen			Changed details and steps to find client on openmrs
15/08/2018	Asen			Switched from using API to MySQL query for finding UUI for patient with PTracker ID
17/07/2020	Asen			Checks for provided uuid from view before searching for PTracker ID

*/

if ($odk_openmrs_patient_uuid != null) {

    $found_openmrs_patient_identifier_uuid = $odk_openmrs_patient_uuid;
    $found_openmrs_person_identifier_uuid = $odk_openmrs_patient_uuid;
    echo "<br>".$date." Using identifier <em>OpenMRS Patient UUID</em> (".$odk_openmrs_patient_uuid.") from view";

    
} else {

    $identifier_to_check = "PTracker ID";
    echo "<br>".$date." Checking identifier <em>".$identifier_to_check."</em> (".$odk_ptrackerid.")";

    $sql_find_patient_uuid = "SELECT * FROM person WHERE person_id IN (SELECT patient_id FROM patient_identifier WHERE identifier = '".$odk_ptrackerid."') ";
    $result_find_patient_uuid = $conn3->query($sql_find_patient_uuid);
    $row_find_patient_uuid = $result_find_patient_uuid->fetch_assoc();

    if ($row_find_patient_uuid['uuid'] != '') {
        
        echo "<br>".$date." Patient Found : <em>".$row_find_patient_uuid['uuid']."</em>";
        
        $found_openmrs_patient_identifier_uuid = $row_find_patient_uuid['uuid'];
        $found_openmrs_person_identifier_uuid = $row_find_patient_uuid['uuid'];
            
        $sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Found identifier ".$identifier_to_check."', openmrs_patient_uuid = '".$found_openmrs_patient_identifier_uuid."', ptracker_id_found = 'TRUE', http_status_code = '".$identifier_status."', error_message = '".$identifier_ptracker_create_error."' WHERE id = '".$sync_log_id."'";
        
    } else {
        
        $found_openmrs_patient_identifier_uuid = null;
        echo "<br>".$date." Patient not found";
        $sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'No patient with identifier ".$identifier_to_check." found', ptracker_id_found = 'FALSE', http_status_code = '".$identifier_status."', error_message = '".$identifier_ptracker_create_error."' WHERE id = '".$sync_log_id."'";
        $found_openmrs_patient_identifier_uuid = null;
        $found_openmrs_person_identifier_uuid = null;

    }
    
    if ($conn->query($sql_update) === TRUE) {


    } else {
        
        echo "Error: " . $sql_update . "<br>" . $conn->error;
    }
}    

?>