<?php
/*
version: 	1.0
author:		Asen Mwandemele
date:		February 2018

update person's name based on new name from odk
___________________________________
change log
___________________________________
date:		author:			comment: 
20/02/2018	Asen			Fixed issue where infant PNC would not be recorded due to error is SQL query
21/02/2018	Asen			Fixed issue where name update would fail due to invalid payload.
 
*/

if ($created_openmrs_person_uuid != null) {
    
    // create connection
    $conn4 = new mysqli($servername2, $username2, $password2, $dbname2);

	//get system id by using username
	$sql_user_person = "SELECT * FROM person WHERE uuid = '".$created_openmrs_person_uuid."' ";
	$result_sql_user_person = $conn4->query($sql_user_person);
	$row_user_person = $result_sql_user_person->fetch_assoc();
		
	$sql_person_name_get = "SELECT * FROM person_name WHERE person_id = '".$row_user_person["person_id"]."' ";
	$result_sql_person_name_get = $conn4->query($sql_person_name_get);
	$row_person_name = $result_sql_person_name_get->fetch_assoc();
	
}
if ($row_person_name["uuid"] != null) {
	
$openmrs_person_name_uuid = $row_person_name["uuid"];
	    	    			   
$name        = array();
$names       = array();
$person_data = array(); 

$name['givenName']      = $odk_given_new;
if(isset($odk_middle_new)) { $name['middleName'] = $odk_middle_new;}
$name['familyName']     = $odk_family_new;
$name['preferred']      = "true"; 

//array_push($names,$name); 

//$person_data['names'] = $names;
     
$person_data = json_encode($name);
 
//echo $person_data;
    
$person_curL = curl_init(''.$openmrs_url.'/ws/rest/v1/person/'.$created_openmrs_person_uuid.'/name/'.$openmrs_person_name_uuid.'');
curl_setopt($person_curL, CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
curl_setopt($person_curL, CURLOPT_USERPWD,''.$openmrs_logins.'');
curl_setopt($person_curL, CURLOPT_RETURNTRANSFER,true);
curl_setopt($person_curL, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); 
curl_setopt($person_curL, CURLOPT_POST,1);
curl_setopt($person_curL, CURLOPT_POSTFIELDS,$person_data);
$person_output   = curl_exec($person_curL);
$person_status   = curl_getinfo($person_curL,CURLINFO_HTTP_CODE);
curl_close($person_curL);

// get json response
$person_response = json_decode($person_output);

//echo $person_output;

// display error message or output uuid of person created
$person_create_error = $person_response->error->message;

if (isset($person_response->error->message)) { 
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Error updating person', http_status_code = '".$person_status."', error_message = '".$person_create_error."' WHERE id = '".$sync_log_id."'";
	echo "<br>".$date." Error Updating Person: <br>".$person_create_error."<br> ";
	
} else {
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Person updated successfully', http_status_code = '".$person_status."', openmrs_person_uuid = '".$created_openmrs_person_uuid."', error_message = '".$person_create_error."' WHERE id = '".$sync_log_id."'";
	echo "<br>".$date." Updated person with UUID: ".$created_openmrs_person_uuid."";
	
} 

if ($conn->query($sql_update) === TRUE) {

} else {
    echo "Error: " . $sql_create . "<br>" . $conn->error;
}
}
?>