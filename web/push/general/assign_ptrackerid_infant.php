<?php
/*
version: 	1.0
author:		Asen Mwandemele
date:		January 2018

assign ptracker identifier to new person
___________________________________
change log
___________________________________
date:		author:			comment:
24/01/2018	Asen			made correction to api url
*/ 			    
$identifier_infant_to_add = "Infant PTracker ID";

$identifier_infant_data = array(); 

$identifier_infant_data['identifier'] = $odk_infant_ptracker_id;
$identifier_infant_data['identifierType'] = "4da0a3fe-e546-463f-81fa-084f098ff06c"; // PTracker ID    
     
$identifier_infant_data = json_encode($identifier_infant_data);
  
$identifier_infant_curL = curl_init(''.$openmrs_url.'/ws/rest/v1/patient/'.$created_openmrs_patient_infant_uuid.'/identifier');
curl_setopt($identifier_infant_curL, CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
curl_setopt($identifier_infant_curL, CURLOPT_USERPWD,''.$openmrs_logins.'');
curl_setopt($identifier_infant_curL, CURLOPT_RETURNTRANSFER,true);
curl_setopt($identifier_infant_curL, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); 
curl_setopt($identifier_infant_curL, CURLOPT_POST,1);
curl_setopt($identifier_infant_curL, CURLOPT_POSTFIELDS,$identifier_infant_data);
$identifier_infant_output   = curl_exec($identifier_infant_curL);
$identifier_infant_status   = curl_getinfo($identifier_infant_curL,CURLINFO_HTTP_CODE);
curl_close($identifier_infant_curL);

// get json response
$identifier_infant_response = json_decode($identifier_infant_output);

//echo $identifier_infant_output;

// display error message or output uuid of person created
$identifier_infant_create_error = $identifier_infant_response->error->message;

// remove inverted commas for MySQl insert
$identifier_infant_create_error_comma = str_replace("'"," ",$identifier_infant_create_error); 


if (isset($identifier_infant_response->error->message)) { 

	echo "<br>".$date." Error: ".$identifier_infant_response->error->message.""; 
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Error adding identifier ".$identifier_infant_to_add."',  ptracker_id_assigned = '" . $identifier_infant_create_error_comma . "', http_status_code = '".$identifier_infant_status."', error_message = '".$identifier_infant_create_error_comma."' WHERE id = '".$sync_log_id."'";
	echo "<br>".$date." Error assigning identifier <em>".$identifier_infant_to_add."</em>";
	
} else {
	$created_openmrs_identifier_uuid = $identifier_infant_response->uuid;
	echo "<br>".$date." Assigned identifier <em>".$identifier_infant_to_add."</em> - ".$created_openmrs_identifier_uuid."";
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Added identifier', ptracker_id_assigned = 'TRUE', http_status_code = '".$identifier_infant_status."', error_message = '".$identifier_infant_create_error."' WHERE id = '".$sync_log_id."'";

}
 
if ($conn->query($sql_update) === TRUE) {
    //echo "<br>".$date." Updated Sync Log Record ".$created_openmrs_identifier_uuid."";
} else {
    echo "Error: " . $sql_update . "<br>" . $conn->error;
}



?>