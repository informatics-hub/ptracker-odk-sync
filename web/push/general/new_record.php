<?php
/*
version: 	1.0
author:		Asen Mwandemele
date:		January 2018

create new record in sync log table
___________________________________
change log
___________________________________
date:		author:			comment:

*/      		
			   
$sql_create= "INSERT INTO stag_ptracker_synclog (ptracker_id, date_created, facility, status, odk_uuid, openmrs_person_uuid, openmrs_patient_uuid, visit_date) VALUES ('".$odk_ptrackerid."', '".$date."','".$odk_facility."', 'New sync record', '".$odk_odk_uuid."', '".$odk_openmrs_person_uuid."', '".$odk_openmrs_patient_uuid."', '".$odk_visit_date."')";

if ($conn->query($sql_create) === TRUE) {
	$sync_log_id = $conn->insert_id;
    echo "<br>".$date." Created sync log record <strong>".$sync_log_id."</strong>.";
    $new_record_created = 1;
} else {
    echo "<br>".$date." Error creating sync log record: " . $sql_create . "<br>" . $conn->error;
    die('<br><span style="color:red"><strong>A critical error occurred.</strong></span>');

}

?>