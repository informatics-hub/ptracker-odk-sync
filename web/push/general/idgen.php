<?php
/*
version: 	1.0
author:		Asen Mwandemele
date:		November 2017

generates openmrs id
___________________________________
change log
___________________________________
date:		author:			comment:

*/ 

// generating new openmrs id to be used for patient generation

$idgen_curL = curl_init(''.$openmrs_url.'/module/idgen/generateIdentifier.form?source=1&username='.$openmrs_username.'&password='.$openmrs_password.'');
curl_setopt($idgen_curL, CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
curl_setopt($idgen_curL, CURLOPT_USERPWD,''.$openmrs_logins.'');
curl_setopt($idgen_curL, CURLOPT_RETURNTRANSFER,true);
curl_setopt($idgen_curL, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); 
curl_setopt($idgen_curL, CURLOPT_POST,1);
curl_setopt($idgen_curL, CURLOPT_POSTFIELDS,$idgen_data);
$idgenoutput  = curl_exec($idgen_curL);
$idgenstatus  = curl_getinfo($idgen_curL,CURLINFO_HTTP_CODE);
curl_close($idgen_curL);

$idgen_response = json_decode($idgenoutput);

$generated_openmrs_id = $idgen_response->identifiers[0];

echo "<br>".$date." Created OpenMRS ID: <strong>".$generated_openmrs_id."</strong>";

$idgen_create_error = $idgenoutput;

if (isset($generated_openmrs_id)) {
$sql_create= "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', generated_openmrs_id = '".$generated_openmrs_id."', openmrs_id = '".$generated_openmrs_id."', status = 'Generated OpenMRS ID: ".$generated_openmrs_id."', http_status_code = '".$idgenstatus."', error_message = '".$idgen_create_error."' WHERE id = '".$sync_log_id."'";

if ($conn->query($sql_create) === TRUE) {
	//echo "<br>".$date." Updated Sync Log Record with OpenMRS ID ".$generated_openmrs_id."";
} else {
	echo "<br>".$date." Error updating record: " . $conn->error;
}
} else { echo "<br>".$date." Error Generating OpenMRS ID: ".$idgen_create_error."";  }






?>