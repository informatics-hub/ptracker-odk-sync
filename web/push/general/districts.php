<?php
/*
version: 	1.0
author:		Asen Mwandemele
date:		November 2017

districts
___________________________________
change log
___________________________________
date:		author:			comment:	

*/ 		

switch ($row["district"]) {
    case "1":
    	//	Andara District
        $odk_district = "M4CCRjpqMoJ"; 
        $odk_country_district = 'Andara District';
        break;
    case "2":
    	//	Aranos District
        $odk_country_district = 'Aranos District';
        $odk_district = "zunsIBRYV3h"; 
        break;
	case "3":
    	// Eenhana District
        $odk_country_district = 'Eenhana District';
    	$odk_district = "GGTLdoDHbrR"; 
        break;
    case "4":
    	// Engela District	
        $odk_country_district = 'Engela District';
    	$odk_district = "qAWnilOcDa8"; 
        break;
    case "5":
    	// Gobabis District	
        $odk_country_district = 'Gobabis District';
    	$odk_district = "pCHdG1g9imN"; 
        break;
    case "6":
    	// Grootfontein District	
        $odk_country_district = 'Grootfontein District';
    	$odk_district = "HBNyx54xyKg"; 
        break;
    case "7":
    	// 	Karasburg District	
        $odk_country_district = 'Karasburg District';
    	$odk_district = "h9lJDhyWO9x"; 
        break;
    case "8":
    	// 	Katima Mulilo District	
        $odk_country_district = '	Katima Mulilo District';
    	$odk_district = "KSe05Xbqo0U"; 
        break;
    case "9":
    	// 	Keetmanshoop District	
        $odk_country_district = 'Keetmanshoop District';
    	$odk_district = "F7k9I7WePE7"; 
        break;
    case "10":
    	// Khorixas District	
        $odk_country_district = 'Khorixas District';
    	$odk_district = "ut0VY2RkP4n"; 
        break;
    case "11":
    	// 	Luderitz District	
        $odk_country_district = 'Luderitz District';
    	$odk_district = "dnVyDFlf55D"; 
        break;    
    case "12":
    	// 	Mariental District	
        $odk_country_district = 'Mariental District';
    	$odk_district = "bFCHIa6CuzW"; 
        break;	
    case "13":
    	// 	Nankudu District	
        $odk_country_district = 'Nankudu District';
    	$odk_district = "wSHVyf2XMH4"; 
        break;
    case "14":
    	// 	Nyangana District	
        $odk_country_district = 'Nyangana District';
    	$odk_district = "FtxUgraJFFH"; 
        break;
    case "15":
    	// 	Okahandja District	
        $odk_country_district = 'Okahandja District';
    	$odk_district = "UQTgGLGzVJR"; 
        break;
    case "16":
    	// 	Okahao District	
        $odk_country_district = 'Okahao District';
    	$odk_district = "EcsNePeiu8B"; 
        break;
    case "17":
    	// 	Okakarara District	
    	$odk_district = "cwdQdlsupXZ"; 
        $odk_country_district = 'Okakarara District';
        break;
    case "18":
    	// 	Okongo District	
        $odk_country_district = 'Okongo District';
    	$odk_district = "FKhUQqO7kgp"; 
        break;
    case "19":
    	// 	Omaruru District	
        $odk_country_district = 'Omaruru District';
    	$odk_district = "JdZWxlDcuct"; 
        break;
    case "20":
    	// 	Omuthiya District	
    	$odk_district = "A3TNsiR1dh5"; 
        $odk_country_district = 'Omuthiya District';
        break;
    case "21":
    	// 	Onandjokwe District	
    	$odk_district = "RTeFLiIQbw6"; 
        $odk_country_district = 'Outjo District';
        break;
    case "22":
    	// 	Opuwo District	
        $odk_country_district = 'Opuwo District';
    	$odk_district = "dOdJP21SD9O"; 
        break;
    case "23":
    	// 	Oshakati District	
        $odk_country_district = 'Oshakati District';
    	$odk_district = "fGEgF3PkXvq"; 
        break;
    case "24":
    	// 	Oshikuku District	
        $odk_country_district = 'Oshikuku District';
    	$odk_district = "qUvunSjZohO"; 
        break;
    case "25":
    	// 	Otjiwarongo District	
    	$odk_district = "OZpQSwHeL07"; 
        $odk_country_district = 'Otjiwarongo District';
        break;
    case "26":
    	// 	Outapi District	
    	$odk_district = "kvoY0kknTC4"; 
        $odk_country_district = 'Outapi District';
        break;
    case "27":
    	// 	Outjo District	
    	$odk_district = "nBGDrsjRfLw"; 
    	$odk_country_district = 'Outjo District';
        break;
    case "28":
    	// Rehoboth District	
        $odk_country_district = 'Rehoboth District';
    	$odk_district = "pTuejLrN4sm"; 
        break;
    case "29":
    	// Rundu District	
        $odk_country_district = 'Rundu District';
    	$odk_district = "wMIC5fKYst9"; 
        break;
    case "30":
    	// Swakopmund District	
        $odk_country_district = 'Swakopmund District';
    	$odk_district = "Ep5roMtNesc"; 
        break;
    case "31":
    	// Tsandi District	
        $odk_country_district = 'Tsandi District';
    	$odk_district = "QH5y5NkOxfc"; 
        break;
    case "32":
    	// Tsumeb District	
        $odk_country_district = 'Tsumeb District';
    	$odk_district = "PvX6HjANIMR"; 
        break;
    case "33":
    	// Usakos District	
        $odk_country_district = 'Usakos District';
    	$odk_district = "tbFjsqAACE8"; 
        break;
    case "34":
    	// Walvis Bay District	
    	$odk_district = "E9sSue8msED"; 
        $odk_country_district = 'Walvis Bay District';
        break;
    case "35":
    	// Windhoek District	
        $odk_country_district = 'Windhoek District';
    	$odk_district = "OPZrTAb9fdW"; 
        break;
    default:
        $odk_country_district = "Unknown Location";
        $odk_district = "8d6c993e-c2cc-11de-8d13-0010c6dffd0f";
        echo "<br>".$date." Unknown district code, setting district to ".$odk_country_district."";
}


?>