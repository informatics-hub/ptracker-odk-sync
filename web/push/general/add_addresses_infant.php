<?php
/*
version: 	1
author:		Asen Mwandemele
date:		January 2018

add infant/patient addresss
___________________________________
change log
___________________________________
date:		author:			comment:
24/01/2018	Asen			Fixes to typos in variable names
*/ 			    
$address_infant_to_add = " info";
$address_infant_data = array(); 

if ($odk_address != '') { $address_infant_data['address1']    = $odk_address; }   // address
if ($odk_address_locaiton != '') { $address_infant_data['address2']    = $odk_address_locaiton; }   // location
if ($odk_country != null) { $address_infant_data['country'] = $odk_country; }
if ($odk_country_district != null) { $address_infant_data['countyDistrict'] = $odk_country_district; }
     
$address_infant_data = json_encode($address_infant_data);
  
$address_infant_curL = curl_init(''.$openmrs_url.'/ws/rest/v1/person/'.$created_openmrs_infant_uuid.'/address');
curl_setopt($address_infant_curL, CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
curl_setopt($address_infant_curL, CURLOPT_USERPWD,''.$openmrs_logins.'');
curl_setopt($address_infant_curL, CURLOPT_RETURNTRANSFER,true);
curl_setopt($address_infant_curL, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); 
curl_setopt($address_infant_curL, CURLOPT_POST,1);
curl_setopt($address_infant_curL, CURLOPT_POSTFIELDS,$address_infant_data);
$address_infant_output   = curl_exec($address_infant_curL);
$address_infant_status   = curl_getinfo($address_infant_curL,CURLINFO_HTTP_CODE);
curl_close($address_infant_curL);

// get json response
$address_infant_response = json_decode($address_infant_output);

//echo $address_infant_output;

// display error message or output uuid of person created
$address_infant_create_error = $address_infant_response->error->message;

if (isset($address_infant_response->error->message)) { 

	echo "<br>".$date." Error: ".$address_infant_response->error->message.""; 
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Error adding address ".$address_infant_to_add."', openmrs_address_infant_added = 'FALSE', http_status_code = '".$address_infant_status."', error_message = '".$address_infant_create_error."' WHERE id = '".$sync_log_id."'";
	echo "<br>".$date." Error adding address <em>".$address_infant_to_add."</em>";
	
} else {
	$created_openmrs_address_infant_uuid = $address_infant_response->uuid;
	echo "<br>".$date." Added address <em>".$address_infant_to_add."</em> - '".$created_openmrs_address_infant_uuid."'";
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Added address', openmrs_address_added = 'TRUE', http_status_code = '".$address_infant_status."', error_message = '".$address_infant_create_error."' WHERE id = '".$sync_log_id."'";

}
 
if ($conn->query($sql_update) === TRUE) {
    //echo "<br>".$date." Updated Sync Log Record ".$created_openmrs_address_infant_uuid."";
} else {
    echo "Error: " . $sql_update . "<br>" . $conn->error;
}




?>