<?php
/*
version: 	1.0
author:		Asen Mwandemele
date:		January 2018

districts
___________________________________
change log
___________________________________
date:		author:			comment:	
20/02/2018	Asen			Fixed issue where country was spelt wrong and other was not added.

*/ 		
switch ($row["country"]) {
    case "1":
        $odk_country = 'Namibia';
        break;
    case "2":
        $odk_country = "Angola"; 
        break;
	case "3":
    	$odk_country = "Zambia"; 
        break;
    case "4":
    	$odk_country = "Zimbabwe"; 
        break;
    case "5":
    	$odk_country = "Botswana"; 
        break;
    case "6":
    	$odk_country = "South Africa"; 
        break;
    case "66":
    	$odk_country = null; 
        break;
    case "99":
    	$odk_country = $row["country_other"];
        break;
    default:
        $odk_country = $row["country_other"];
        echo "<br>".$date." Unknown country code, setting country to ".$odk_country."";
}

?>