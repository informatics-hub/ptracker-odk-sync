<?php
/*
version: 	1.0
author:		Asen Mwandemele
date:		July 2020

FIND PATIENT USING PTRACKER ID - Infant PNC
___________________________________
change log
___________________________________
date:		author:			comment:

*/

$identifier_to_check = "Mother PTracker ID";
echo "<br>".$date." Checking identifier <em>".$identifier_to_check."</em> (".$parent_ptracker_id.")";

$sql_find_m_patient_uuid = "SELECT * FROM person WHERE person_id IN (SELECT patient_id FROM patient_identifier WHERE identifier = '".$parent_ptracker_id."') ";
$result_find_m_patient_uuid = $conn3->query($sql_find_m_patient_uuid);
$row_find_m_patient_uuid = $result_find_m_patient_uuid->fetch_assoc();

if ($row_find_m_patient_uuid['uuid'] != '') {
    
    echo "<br>".$date." Patient Found : <em>".$row_find_m_patient_uuid['uuid']."</em>";
    
    $found_openmrs_m_patient_identifier_uuid = $row_find_m_patient_uuid['uuid'];
    $found_openmrs_m_person_identifier_uuid = $row_find_m_patient_uuid['uuid'];
        
    $sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Found identifier ".$identifier_to_check."', openmrs_patient_uuid = '".$found_openmrs_patient_identifier_uuid."', ptracker_id_found = 'TRUE', http_status_code = '".$identifier_status."', error_message = '".$identifier_ptracker_create_error."' WHERE id = '".$sync_log_id."'";
    
} else {
    
    $found_openmrs_patient_identifier_uuid = null;
	echo "<br>".$date." Patient not found";
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'No patient with identifier ".$identifier_to_check." found', ptracker_id_found = 'FALSE', http_status_code = '".$identifier_status."', error_message = '".$identifier_ptracker_create_error."' WHERE id = '".$sync_log_id."'";

}
 
if ($conn->query($sql_update) === TRUE) {


} else {
    
    echo "Error: " . $sql_update . "<br>" . $conn->error;
}

?>