<?php
/*
version: 	1.0
author:		Asen Mwandemele
date:		July 2020

new relationship at pnc
___________________________________
change log
___________________________________
date:		author:			comment:

*/      		
			   
$person_rel_data = array(); 

$person_rel_data['relationshipType'] = '8d91a210-c2cc-11de-8d13-0010c6dffd0f';   // Relationship from a mother/father to a child
$person_rel_data['personA'] = $current_openmrs_parent_uuid; // parent
$person_rel_data['personB'] = $current_openmrs_child_uuid; // child
     
$person_rel_data = json_encode($person_rel_data);
 
$person_rel_curL = curl_init(''.$openmrs_url.'/ws/rest/v1/relationship');
curl_setopt($person_rel_curL, CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
curl_setopt($person_rel_curL, CURLOPT_USERPWD,''.$openmrs_logins.'');
curl_setopt($person_rel_curL, CURLOPT_RETURNTRANSFER,true);
curl_setopt($person_rel_curL, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); 
curl_setopt($person_rel_curL, CURLOPT_POST,1);
curl_setopt($person_rel_curL, CURLOPT_POSTFIELDS,$person_rel_data);
$person_rel_output   = curl_exec($person_rel_curL);
$person_rel_status   = curl_getinfo($person_rel_curL,CURLINFO_HTTP_CODE);
curl_close($person_rel_curL);

// get json response
$person_rel_response = json_decode($person_rel_output);

echo " <br><br><br>";

echo $person_rel_data;

echo " <br><br><br>";

// display error message or output uuid of person created
$person_rel_create_error = $person_rel_response->error->message;

if (isset($person_rel_response->error->message)) { 

	$created_openmrs_person_rel_uuid = null;
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Error creating pnc relationship', http_status_code = '".$person_rel_status."', error_message = '".$person_rel_create_error."' WHERE id = '".$sync_log_id."'";
	echo "<br>".$date." Error Creating Relationship: <br>".$person_rel_create_error."<br> ";
	
} else {
	$created_openmrs_person_rel_uuid = $person_rel_response->uuid;
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'PNC relationship created successfully', http_status_code = '".$person_rel_status."', openmrs_person_uuid = '".$created_openmrs_person_rel_uuid."', error_message = '".$person_rel_create_error."' WHERE id = '".$sync_log_id."'";
	echo "<br>".$date." Created relationship with UUID: ".$created_openmrs_person_rel_uuid."";
	
} 

if ($conn->query($sql_update) === TRUE) {

} else {
    echo "Error: " . $sql_create . "<br>" . $conn->error;
}

	

?>