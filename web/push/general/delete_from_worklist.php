<?php
/*
version: 	1.0
author:		Asen Mwandemele
date:		January 2020

delete record in sync log table
___________________________________
change log
___________________________________
date:		author:			comment:

*/      		

if ($created_openmrs_encounter_uuid != null) { 

    $sql_create= "DELETE FROM stag_ptracker_worklist WHERE _URI = '".$odk_odk_uuid."'";

    if ($conn->query($sql_create) === TRUE) {
        echo "<br>".$date." Worklist record <strong>".$odk_odk_uuid."</strong> removed from queue.";
        $new_record_created = -1;
    } else {
        echo "<br>".$date." Error removing worklist record: " . $odk_odk_uuid . "<br>" . $conn->error;
        die('<br><span style="color:red"><strong>A critical error occurred.</strong></span>');

    }
} else {
    die('<br><span style="color:orange"><strong>No encounter created, not removing from worklist</strong></span>');
}

?>