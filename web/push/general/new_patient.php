<?php
/*
version: 	1.0
author:		Asen Mwandemele
date:		November 2017

new patient
___________________________________
change log
___________________________________
date:		author:			comment:

*/ 			    
$identifiers           = array();
$patient_data           = array();
$person_identification = array();

//$identifiers['identifier']     = $odk_ptrackerid; // PTracker ID
//$identifiers['identifierType'] = "4da0a3fe-e546-463f-81fa-084f098ff06c"; // uuid of PTracker ID
$identifiers['identifier']     = $generated_openmrs_id; // Generated OpenMRS ID
$identifiers['identifierType'] = "05a29f94-c0ed-11e2-94be-8c13b969e334"; // uuid of OpenMRS ID
$identifiers['location']       = $odk_location_facility_encounter; 
//$identifiers['preferred']     = "true"; 
 
// combine 
array_push($person_identification,$identifiers);

$patient_data['person']      = $created_openmrs_person_uuid; // Newly created person's uuid  
$patient_data['identifiers'] = $person_identification;


$patient_data = json_encode($patient_data);
  
$patient_curL = curl_init(''.$openmrs_url.'/ws/rest/v1/patient');
curl_setopt($patient_curL, CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
curl_setopt($patient_curL, CURLOPT_USERPWD,''.$openmrs_logins.'');
curl_setopt($patient_curL, CURLOPT_RETURNTRANSFER,true);
curl_setopt($patient_curL, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); 
curl_setopt($patient_curL, CURLOPT_POST,1);
curl_setopt($patient_curL, CURLOPT_POSTFIELDS,$patient_data);
$patient_output   = curl_exec($patient_curL);
$patient_status   = curl_getinfo($patient_curL,CURLINFO_HTTP_CODE);
curl_close($patient_curL);

// get json response
$patient_response = json_decode($patient_output);

//echo $patient_data;

// display error message or output uuid of person created
$patient_create_error = $patient_response->error->message;

if (isset($patient_response->error->message)) { 

	echo "<br>".$date." Error: ".$patient_response->error->message."";  
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Error Creating Patient', http_status_code = '".$patient_status."', error_message = '".$patient_create_error."' WHERE id = '".$sync_log_id."'";
	echo "<br>".$date." Error Creating Patient";
	
} else {
	$created_openmrs_patient_uuid = $patient_response->uuid;
	echo "<br>".$date." Created Patient with UUID: ".$created_openmrs_patient_uuid."";
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', openmrs_patient_uuid = '".$created_openmrs_patient_uuid."', status = 'Created Patient with UUID: ".$created_openmrs_patient_uuid."', http_status_code = '".$patient_status."', error_message = '".$patient_create_error."' WHERE id = '".$sync_log_id."'";

}
 
if ($conn->query($sql_update) === TRUE) {
    //echo "<br>".$date." Updated Sync Log Record '".$created_openmrs_patient_uuid."'.";
} else {
    echo "Error: " . $sql_update . "<br>" . $conn->error;
}



?>