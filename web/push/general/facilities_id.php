<?php
/*
version: 	1.2
author:		Asen Mwandemele
date:		August 2018

facilities (returns ID instead of UUID)
___________________________________
change log
___________________________________
date:		author:			comment:
10/02/2020  Asen            Modified script to retireve openmrs facility id from updated facility_codes table

*/
    //get openmrs facility uuid by using mfl code provided by odk
	$sql_facility_match = "SELECT * FROM facility_codes WHERE mfl_code = '".$odk_facility_check."' ";
	$result_sql_facility_match = $conn->query($sql_facility_match);
	$row_facility_match = $result_sql_facility_match->fetch_assoc();

    if (isset($row_facility_match['uuid'])) {

        echo "<br>".$date." Facility location ID found : <em>".$row_facility_match['name']."</em>";
        $odk_location_facility_id = $row_facility_match['openmrs_location_id'];         
          
    } else {
        $odk_location_facility_id = "1";
        echo "<br>".$date." Unknown facility code, switching to default UNKNOWN LOCATION";
        
    }

?>