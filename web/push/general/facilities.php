<?php
/*
version: 	1.2
author:		Asen Mwandemele
date:		November 2017

facilities
___________________________________
change log
___________________________________
date:		author:			comment:
16/01/2017	Asen			Added other facility code in off chance that is used instead of the facility code from drop down in ODK
14/08/2018	Asen			Added link to database for finding full list of openmrs uuid / mfl code districts

*/

// locations have different IDs in the OpenMRS database :-( should consider doing this in the view query or something along those lines
if ($row['facility_code'] != null) { $odk_facility_check = $row['facility_code']; } else { $odk_facility_check = $row['facility_code_other']; }

    //get openmrs facility uuid by using mfl code provided by odk
	$sql_facility_match = "SELECT * FROM facility_codes WHERE mfl_code = '".$odk_facility_check."' ";
	$result_sql_facility_match = $conn->query($sql_facility_match);
	$row_facility_match = $result_sql_facility_match->fetch_assoc();

    if (isset($row_facility_match['uuid'])) {
        
        $odk_location_facility = $row_facility_match['uuid']; 
        echo "<br>".$date." Found facility code for ".$row_facility_match['name']."";
        
    } else {
      
        $odk_location_facility = "8d6c993e-c2cc-11de-8d13-0010c6dffd0f";
        echo "<br>".$date." Unknown facility code, switching to default UNKNOWN LOCATION";
        
    }

?>