<?php
/*
version: 	1.0
author:		Asen Mwandemele
date:		January 2017

gets provider id when given person id
___________________________________
change log
___________________________________
date:		author:			comment:

*/ 

// create connection
$conn2 = new mysqli($servername2, $username2, $password2, $dbname2);

// check connection
if ($conn2->connect_error) {
    die("Connection failed: " . $conn2->connect_error);
}

if ($odk_provider_username != null) {
	//get system id by using username
	$sql_user = "SELECT * FROM users WHERE username = '".$odk_provider_username."' ";
	$result_sql_user = $conn2->query($sql_user);
	$row_user = $result_sql_user->fetch_assoc();
		
	$sql_proider_get = "SELECT * FROM provider WHERE person_id = '".$row_user["person_id"]."' ";
	$result_sql_provider_get = $conn2->query($sql_proider_get);
	$row_provider = $result_sql_provider_get->fetch_assoc();
	
	
	if ($row_provider["uuid"] != null) {
	
		$odk_provider_id = $row_provider["uuid"];
	    	    
		echo "<br>".$date." Obtained provider ID  for <strong>".$odk_provider_username."</strong>";
	
	} else {
		
		echo "<br>".$date." Unable to determine provider ID for <strong>".$odk_provider_username."</strong>";
		die('an error occurred');
	
	}
	
} else {
		
		echo "<br>".$date." No username given.";
		die('an error occurred');

}

$conn2->close();

?>