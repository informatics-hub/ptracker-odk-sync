<?php
/*
version: 	1.0
author:		Asen Mwandemele
date:		January 2018

delete record in sync log table
___________________________________
change log
___________________________________
date:		author:			comment:
2018/12/04  Asen            Modified script to avoid an endless loop and place burden on developer to resolve issue.
*/      		
			   
//$sql_create= "DELETE FROM stag_ptracker_synclog WHERE odk_uuid = '".$odk_odk_uuid."'";
$sql_create= "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'FAILED', error_message = 'Unable to complete submission, delete this from sync log and try again to see what hppened.' WHERE odk_uuid = '".$odk_odk_uuid."'";

if ($conn->query($sql_create) === TRUE) {
    echo "<br>".$date." Sync log record <strong>".$sync_log_id."</strong> has failed to submit.";
    $new_record_created = -1;
} else {
    echo "<br>".$date." Error removing sync log record: " . $sql_create . "<br>" . $conn->error;
    die('<br><span style="color:red"><strong>A critical error occurred.</strong></span>');

}

?>