<?php
/*
version: 	1
author:		Asen Mwandemele
date:		January 2018

new person
___________________________________
change log
___________________________________
date:		author:			comment:

*/      		
			   
$name_infant        = array();
$names_infant       = array();
$infant_data 		= array(); 

$name_infant['givenName']  = $odk_infant_given;
$name_infant['middleName'] = $odk_infant_middle;
$name_infant['familyName'] = $odk_infant_family;

array_push($names_infant,$name_infant); 

$infant_data['birthdate'] 	= $odk_infant_dob;
$infant_data['gender']   	= $odk_infant_sex;   
$infant_data['names'] 		= $names_infant;
     
$infant_data = json_encode($infant_data);
 
$infant_curL = curl_init(''.$openmrs_url.'/ws/rest/v1/person');
curl_setopt($infant_curL, CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
curl_setopt($infant_curL, CURLOPT_USERPWD,''.$openmrs_logins.'');
curl_setopt($infant_curL, CURLOPT_RETURNTRANSFER,true);
curl_setopt($infant_curL, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); 
curl_setopt($infant_curL, CURLOPT_POST,1);
curl_setopt($infant_curL, CURLOPT_POSTFIELDS,$infant_data);
$infant_output   = curl_exec($infant_curL);
$infant_status   = curl_getinfo($infant_curL,CURLINFO_HTTP_CODE);
curl_close($infant_curL);

// get json response
$infant_response = json_decode($infant_output);

//echo $infant_data;

// display error message or output uuid of person created
$infant_create_error = $infant_response->error->message;

//echo $infant_create_error;

if (isset($infant_response->error->message)) { 

	$created_openmrs_infant_uuid = null;
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Error creating infant', http_status_code = '".$infant_status."', error_message = '".$infant_create_error."' WHERE id = '".$sync_log_id."'";
	echo "<br>".$date." Error Creating Infant: <br>".$infant_create_error."<br> ";
	
} else {
	$created_openmrs_infant_uuid = $infant_response->uuid;
	$sql_update = "UPDATE stag_ptracker_synclog SET date_updated = '".$date."', status = 'Infant created successfully', http_status_code = '".$infant_status."', openmrs_person_uuid = '".$created_openmrs_infant_uuid."', error_message = '".$infant_create_error."' WHERE id = '".$sync_log_id."'";
	echo "<br>".$date." Created infant with UUID: ".$created_openmrs_infant_uuid."";
	
} 

if ($conn->query($sql_update) === TRUE) {

} else {
    echo "Error: " . $sql_create . "<br>" . $conn->error;
}

?>