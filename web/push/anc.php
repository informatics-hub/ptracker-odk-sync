<?php
/*
version: 	1.6
author:		Asen Mwandemele
date:		November 2017

anc visit
___________________________________
change log
___________________________________
date:		author:			comment:
12/12/2017	Asen			Changes to the HIV Status section
13/12/2017	Asen			Added the count of how many records found
09/01/2018	Asen			Added an if statement that checks if encounter id has been created before creating observations etc.
10/01/2018	Asen			Added section to check openmrs if patient with ptracker id exists
							Added section to check if sync log record is created first
11/01/2018	Asen			Changes to country field and if statement for when fields are empty
12/01/2018	Asen			Added section to find provider id based on given username
16/01/2018	Asen			Changed SQL Query to reflect _URI rather than ptracker id and visit date
18/01/2018	Asen			Added code to check if person has been created before attempting to generate openmrs id and creating a patient etc.
22/01/2018	Asen			Resolved issue where sync log is created even though openmrs server is down. 
24/01/2018	Asen			Added transfer out
21/08/2018  Asen            Fixed to retreive location_id from openmrs database as facility id
16/04/2019  Asen            E109-4. Update the ODK sync to capture the added Partner testing quesiton
20/01/2020	Asen			Added remove from worklist file to remove completed record from the worklist staging table
							Alter the MySQL query to reflect new worklist and encounter staging table

*/ 

// Report all errors except E_NOTICE
error_reporting(E_ALL & ~E_NOTICE);

// load boostrap styles
echo '
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
';

// specify date & timezone 
date_default_timezone_set('Africa/Windhoek');
$date 		 = date('Y/m/d H:i:s');

// connect to database & get openmrs logins
require_once('database/config.php'); 

//echo '<h4>PTracker ODK OpenMRS Sync</h4>';
echo ''.$date.' Start of <strong>ANC</strong> run';

// sql query to fetch data from odk view
$sql = "SELECT * FROM stag_odk_anc WHERE _URI IN (SELECT _URI FROM stag_ptracker_worklist) LIMIT 30";  // loads all records present in the work list view
$result = $conn->query($sql);

// start to loop through each record returned in query
if ($result->num_rows > 0) {
    $rows_count=mysqli_num_rows($result); // count number of rows
	echo '<br>'.$date.' Found <strong>'.$rows_count.'</strong> records.';

    while($row = $result->fetch_assoc()) {
        echo '<br>'.$date.' Processing ID <strong>' . $row['ptracker_id']. '</strong>';

        		$odk_ptrackerid = $row['ptracker_id'];
        		$odk_visit_date = $row['visit_date']; // Format should YYYY-MM-DD HH:MM:SS to avoid mixed updates and errors
        		$odk_visit_date_odk_recorded = $row['visit_date_recorded_odk']; // Required specifically for visit date/time for openmrs
        		$odk_openmrs_id= $row['openmrs_id'];
        		$generated_openmrs_id = '';
        		$odk_openmrs_person_uuid = $row['openmrs_person_uuid'];
        		$odk_openmrs_patient_uuid = $row['openmrs_patient_uuid'];
        		$odk_odk_uuid = $row['odk_uuid'];
        		$odk_given = $row['given'];
        		$odk_middle = $row['middle'];
        		$odk_family = $row['family'];
        		$odk_sex = $row['sex'];
        		$odk_dob = $row['dob'];
				$odk_age = $row['age'];
				$odk_phone_number = $row['phone_number'];
				$odk_kin_name = $row['kin_name'];
				$odk_kin_contact = $row['kin_contact'];
				//anc
				$odk_anc_fist_visit = $row['anc_first_visit'];
				$odk_anc_gravida = $row['anc_gravida'];
				$odk_anc_para = $row['anc_para'];
				$odk_anc_edd = $row['anc_edd'];
				$odk_anc_edd_calculated = $row['anc_edd_calculated'];
				$odk_anc_lnmp = $row['anc_lnmp'];
				$odk_next_visit_date = $row['next_visit_date'];
				$odk_next_visit_date_missing = $row['next_visit_date_missing'];
				$odk_next_facility_to_visit = $row['next_facility_to_visit'];
				$odk_next_facility_to_visit_transfered = $row['next_facility_to_visit_transfered'];
				$odk_next_facility_to_visit_transfered_other = $row['next_facility_to_visit_transfered_other'];
				//hiv status
				$odk_hiv_test_status = $row['hiv_test_status'];
				$odk_hiv_test_result = $row['hiv_test_result'];
				$odk_art_int_status = $row['art_int_status'];
				$odk_art_int_refused_reason = $row['art_int_status_refused_reason'];
				$odk_art_int_refused_reason_missing = $row['art_int_status_refused_reason_missing'];
				$odk_art_number = $row['art_number'];
				$odk_odk_art_number_missing = $row["art_number_missing"];
				$odk_art_start_date = $row['art_start_date'];
				$odk_vl_test_done = $row['vl_test_done'];
				$odk_vl_test_date = $row['vl_test_date'];
				$odk_vl_test_result = $row['vl_test_result'];
				$odk_vl_test_result_value = $row['vl_test_result_value'];
				//partner testing
				$odk_partnertest_done = $row['partner_hivtest_done'];
				$odk_partnertest_date = $row['partner_hivtest_date'];
				$odk_partnertest_date_missing = $row['partner_hivtest_date_missing'];
				$odk_partnertest_result = $row['partner_hivtest_result'];				
				//misc
				$odk_provider_person_uuid = $row['provider_uuid'];
				$odk_provider_username = $row['username'];
				$odk_address = $row['address'];
				$odk_address_locaiton = $row['location'];
				$odk_country_district = '';
				$facility_uuid = $row['facility_uuid'];
				if ($row['facility_code'] != null) { $odk_facility = $row['facility_code']; } else { $odk_facility = $row['facility_code_other']; } // used to record facility code in staging table only

				//gets provider ID from openmrs database
				include 'general/provider_id.php';	

				// match locations with that on openmrs
				// facilities
				include 'general/facilities_encounter.php';				

				// countries
				include 'general/countries.php';				

				// districts
				include 'general/districts.php';				

				//creates record in log table
				include 'general/new_record.php';	
				
				if ($new_record_created != 1) {
				
				echo '<br>'.$date.' Sync record not created.'; 
				
				} else  {
				 
					    // check openmrs if patient with ptracker id exists
					    include 'general/check_ptrackerid.php';				    
					    
					    // if patient with ptracker id exists, it should create
					    if ($found_openmrs_patient_identifier_uuid == null) { 
					    
						   	 	// create new person
						   	 	include 'general/new_person.php';
			
						   	 	if ($created_openmrs_person_uuid != null) { 
								   	 	
								   	 	//generate openmrs id
								   	 	include 'general/idgen.php';
								   	 	
								   	 	// create patient
										include 'general/new_patient.php';
										
										// assign PTracker identifier to new person
										include 'general/assign_ptrackerid.php';
										
										//  add attributes to person
										// 	phone number
										if ($odk_phone_number != null) { include 'general/add_attribute_phone.php'; }
							
										// 	next of kin
										if ($odk_kin_name != null) { include 'general/add_attribute_kin.php'; }
											
										// 	next of kin phone number
										if ($odk_kin_contact != null) { include 'general/add_attribute_kin_phone.php'; }
											
										// 	addresses
										include 'general/add_addresses.php'; 
								}
					    } else {
					    		// Set the patient uuid found in openmrs to be used for encounter
						    	$created_openmrs_patient_uuid = $found_openmrs_patient_identifier_uuid;	
						    	
						    	// Set the person uuid found in openmrs to be used for encounter
						    	$created_openmrs_person_uuid = $found_openmrs_person_identifier_uuid;
						    	
						    	echo '<br>'.$date.' <strong>Jump to</strong> Add Encounter';
					    }

						if ($created_openmrs_patient_uuid != null) { 
							    
									// create encounter
									include 'anc/new_encounter.php'; 
															
									// checks if encounter was created first.
									if ($created_openmrs_encounter_uuid != null) { 
											
											// Record observation - anc - first visit
											if ($odk_anc_fist_visit != null) { include 'anc/new_obs_first_visit.php'; }
											
											// Record observation - anc - gravida
											if ($odk_anc_gravida != null) { include 'anc/new_obs_gravida.php'; }
											
                                            // Record observation - PTracker ID
											if ($odk_ptrackerid != null) { include 'general/add_ptracker_id.php'; }
											
											// Record observation - anc - para
											if ($odk_anc_para != null) { include 'anc/new_obs_para.php'; }
											
											// Record observation - anc - lnmp
											if ($odk_anc_lnmp != null)  { include 'anc/new_obs_lnmp.php'; }
											
											// Record observation - anc - edd
											if($odk_anc_edd != null ) { 
														$edd_value = $odk_anc_edd; 
														include 'anc/new_obs_edd.php'; 
											} else { 
														if($odk_anc_edd_calculated != null ) {  
                                                            $edd_value = $odk_anc_edd_calculated; 
                                                            include 'anc/new_obs_edd.php'; 
                                                        }
											}
                                        
											// Record observation - hiv - test status
											if ($odk_hiv_test_status != null) { include 'hiv_status/new_obs_hiv_test_status.php'; }
											
											// Record observation - hiv - test result
											if ($odk_hiv_test_result != null) { include 'hiv_status/new_obs_hiv_test_result.php'; }
											
											// Record observation - hiv - art initiation status
											if ($odk_art_int_status != null) { include 'hiv_status/new_obs_art_int_status.php'; }
                                        
                                            // Record observation - hiv - art refusal reason
								            if ($odk_art_int_refused_reason != null) { include 'hiv_status/new_obs_art_int_refused_reason.php'; }
                                        
                                            // Record observation - hiv - missnig art refusal reason
                                            if ($odk_art_int_refused_reason_missing != null) { include 'hiv_status/new_obs_art_int_refused_reason_missing.php'; }	
											
											// Assign identifier - hiv - art number
											if ($odk_odk_art_number_missing != null) { include 'hiv_status/new_obs_art_number_missing.php'; } else { include 'hiv_status/assign_artnumber.php'; }	
											
											// Record observation - hiv - art start date 
											if ($odk_art_start_date != null) { include 'hiv_status/new_obs_art_start_date.php'; }
											
											// Record observation - hiv - vl test done
											if ($odk_vl_test_done != null)  { include 'hiv_status/new_obs_vl_test_done.php'; }				
											
											// Record observation - hiv - vl test date
											if ($odk_vl_test_date != null)  { include 'hiv_status/new_obs_vl_test_date.php'; }
											
											// Record observation - hiv - vl test result
											if ($odk_vl_test_result != null) { include 'hiv_status/new_obs_vl_test_result.php'; }
																						
											// Record observation - hiv - vl copies
											if ($odk_vl_test_result_value != null) { include 'hiv_status/new_obs_vl_copies.php'; }
											
											// Record observation - partner testing - done
											if ($odk_partnertest_done != null) { include 'anc/new_obs_partnertest_done.php'; }
											
											// Record observation - partner testing - date
											if ($odk_partnertest_date != null)  { include 'anc/new_obs_partnertest_date.php'; }				
											
											// Record observation - partner testing - date missing
											if ($odk_partnertest_date_missing != null)  { include 'anc/new_obs_partnertest_date_missing.php'; }
											
											// Record observation - partner testing - result
											if ($odk_partnertest_result != null) { include 'anc/new_obs_partnertest_result.php'; }
																					
											// Record observation - anc - next visit date
											if ($odk_next_visit_date != null) { include 'anc/new_obs_next_visit_date.php'; } 

                                            // Record observation - anc - next visit date
											if ($odk_next_visit_date_missing != null) { include 'anc/new_obs_next_visit_date_missing.php'; } 
                                        
											// Record observation - anc - facility of next appointment 
											if ($odk_next_facility_to_visit != null) { include 'anc/new_obs_facility_of_next_appointment.php'; }

											// Record observation - anc - transfer to
											if ($odk_next_facility_to_visit_transfered != null) { include 'anc/new_obs_facility_of_next_appointment_transfer.php'; }
								
								} else { echo '<br>'.$date.' Encounter not created.'; 
								
								//deletes record in log table
								include 'general/delete_record.php';

								}	

						//deletes record in worklist table
						include 'general/delete_from_worklist.php';	
				} else { 
						echo '<br>'.$date.' Unable to complete submission.'; 
						//deletes record in log table
						include 'general/delete_record.php';										
						die('<br>'.$date.' Stopping sync process until next attempt.') ; 
				}
		}	
    }
} else {
    
    echo '<br>'.$date.' <strong>No</strong> new records to process.';
}

echo '<br>'.$date.' End of run.';
// close connection
$conn->close();
?>